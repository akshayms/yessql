#include <stdlib.h>
#include "ix.h"
#include <sys/stat.h>

#include "../rbf/rbfm.h"
#include <limits>
#include <cstddef>
#include <iostream>


IndexManager* IndexManager::_index_manager = 0;

//done nc
IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

//done nc
IndexManager::IndexManager()
{
}

//done nc
IndexManager::~IndexManager()
{
}

//done nc
RC IndexManager::createFile(const string &fileName)
{
//	cout<<"IX: Creating file"<<endl;
	/**
	 * While creating a new file, we also initialize the root page with the metadata. in sequence we -
	 * - create file
	 * - open file
	 * - initalize metadata
	 * - append initial leaf-root page
	 * - close file
	 */

	RC rc = PagedFileManager::instance()->createFile(fileName);
	IXFileHandle ixFileHandle;
	if(rc != -1){
		void *pageData = malloc(PAGE_SIZE);
		memset(pageData, 0, PAGE_SIZE);
		initializeRootPageMetadata(pageData);
		updateNextLeafPage(-1, pageData);
		openFile(fileName, ixFileHandle);
         // override initially.
        updatePageType(LEAF_PAGE, pageData);
        ixFileHandle.appendPage(pageData);
	}
	return rc;
}

//done nc
RC IndexManager::destroyFile(const string &fileName)
{
	return PagedFileManager::instance()->destroyFile(fileName);

}

//done nc
RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle)
{

	RC rc = PagedFileManager::instance()->openFile(fileName, ixfileHandle.fileHandle);
	return rc;
}

//done nc
RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
	return PagedFileManager::instance()->closeFile(ixfileHandle.fileHandle);
}

int IndexManager::getKeySize(const void *key, const Attribute &attribute) {
    int size = NUM_RIDS_SIZE + RID_SIZE + sizeof(int);
    if (attribute.type == TypeVarChar) {
        int stringLength;
        memcpy(&stringLength, key, sizeof(int));
        return size + stringLength;
    }
    else {
        return size;
    }
}

int IndexManager::getIntermediateKeySize(const void *key, const Attribute &attribute) {
    int size = (PAGE_ID_SIZE * 2) + sizeof(int);
    if (attribute.type == TypeVarChar) {
        int stringLength;
        memcpy(&stringLength, (char *) key + PAGE_ID_SIZE, sizeof(int));
        return size + stringLength;
    }
    else {
        return size;
    }
}

RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid) {
    void *rootPage = malloc(PAGE_SIZE);
    memset(rootPage, 0, PAGE_SIZE);
    ixfileHandle.readPage(0, rootPage);
    char pageType = getPageType(rootPage);
    bool rootSplit = false;
    void *parentKey = malloc(MAX_KEY_SIZE);
    memset(parentKey, 0, MAX_KEY_SIZE);
    int rootPageId = 0;
    int keySize = getKeySize(key, attribute);
    short start, end;

    int rc = insert(ixfileHandle, attribute, key, rid, keySize, rootPageId, rootPage, rootSplit, parentKey);

    if (rc != 0) {
        free(rootPage);
        free(parentKey);
        return -1;
    }

    if (rootSplit && pageType == LEAF_PAGE) {
        createNewRoot(ixfileHandle, attribute, rootPage, parentKey);
    }

    free(rootPage);
    free(parentKey);
    return 0;

}

void IndexManager::createNewRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, void *rootPage, void *parentKey) {
    void *newRoot = malloc(PAGE_SIZE);
    initializeRootPageMetadata(newRoot);

    int numPages = ixfileHandle.getNumberOfPages();

    ixfileHandle.appendPage(rootPage);
    // previous root has been appeneded to the page as an intermediate node.
    memcpy(parentKey, &numPages, PAGE_ID_SIZE);
    int rootKeySize = getIntermediateKeySize(parentKey, attribute);

    memcpy(newRoot, parentKey, rootKeySize);
    updateFreeSize(INDEX_FILE_FREE_SPACE - rootKeySize, newRoot);
    updateNumberOfKeys(1, newRoot);

    ixfileHandle.writePage(0, newRoot);

    free(newRoot);
}

RC IndexManager::insertAndSplitLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid, int pageId, void *pageData, void *pushUpKey) {
    void *temp = malloc(PAGE_SIZE * 2);
    memset(temp, 0, PAGE_SIZE);
    short oldPageFreeSize = getFreeSize(pageData);
    short oldPageEnd = INDEX_FILE_FREE_SPACE - oldPageFreeSize;
    short keyLength = getKeySize(key, attribute);
    short tempPageEnd = oldPageEnd  + keyLength;
    short start, end, numRids;
    int numPages = ixfileHandle.getNumberOfPages();
    short keySize = getKeySize(key, attribute);
    short newSize;
    int oldNextPage = getNextLeafPage(pageData);
    short oldPageKeyCount = getNumberOfKeys(pageData);
    short newPageKeyCount = 0;
    short newPageFreeSize = 0;
    void *newPage = malloc(PAGE_SIZE);
    memset(newPage, 0, PAGE_SIZE);
    initializeLeafPageMetadata(newPage);

    linearSearch(pageData, attribute, key, start, end);
    if (start == end) {
        tempPageEnd = oldPageEnd + keyLength;
        numRids = 1;
        memcpy(temp, pageData, start);
        memcpy((char *) temp + start, key, keySize);
        memcpy((char *) temp + start + keySize - RID_SIZE - NUM_RIDS_SIZE, &numRids, NUM_RIDS_SIZE);
        memcpy((char *) temp + start + keySize - RID_SIZE, &(rid.pageNum), PAGE_ID_SIZE);
        memcpy((char *) temp + start + keySize - SLOT_ID_SIZE, &(rid.slotNum), SLOT_ID_SIZE);
        memcpy((char *) temp + start + keySize, (char *) pageData + start, oldPageEnd - start);
    }
    else {
        tempPageEnd = oldPageEnd + RID_SIZE;
        newSize = RID_SIZE;
        short currentOffset = 0;
        memcpy(temp, pageData, start);
        currentOffset += start;
        memcpy(&numRids, (char *) pageData + start + keySize - NUM_RIDS_SIZE - RID_SIZE, NUM_RIDS_SIZE);
        numRids++;
        memcpy((char *) pageData + start + keySize - NUM_RIDS_SIZE - RID_SIZE, &numRids, NUM_RIDS_SIZE);

        memcpy((char *) temp + currentOffset, (char *) pageData + start, end - start);
        currentOffset += end - start;
        memcpy((char *)temp + currentOffset, &(rid.pageNum), PAGE_ID_SIZE);
        currentOffset += PAGE_ID_SIZE;
        memcpy((char *)temp + currentOffset, &(rid.slotNum), SLOT_ID_SIZE);
        currentOffset += SLOT_ID_SIZE;
        memcpy((char *)temp + currentOffset, (char *) pageData + end, oldPageEnd - end);
    }

    short offsetToSplit = getLeafMidPoint(attribute, temp, tempPageEnd, oldPageKeyCount, newPageKeyCount, oldPageFreeSize, newPageFreeSize);
    if (start != end) {
        // because its an existing key, only RID is updated
        newPageKeyCount--;
    }
    memcpy(pageData, temp, offsetToSplit);
    memcpy(newPage, (char *) temp + offsetToSplit, tempPageEnd - offsetToSplit);

    updateFreeSize(oldPageFreeSize, pageData);
    updateFreeSize(newPageFreeSize, newPage);
    updateNumberOfKeys(oldPageKeyCount, pageData);
    updateNumberOfKeys(newPageKeyCount, newPage);
    updateNextLeafPage(numPages, pageData);
    updateNextLeafPage(oldNextPage, newPage);
    //cout << " next leaf: " << numPages << " and  " << oldNextPage << endl;
    updatePageType(LEAF_PAGE, pageData);

    ixfileHandle.appendPage(newPage);

    memcpy(pushUpKey, &pageId, PAGE_ID_SIZE);
    int firstKeySize = getKeySize(newPage, attribute) - NUM_RIDS_SIZE - RID_SIZE;
    memcpy((char *) pushUpKey + PAGE_ID_SIZE, (char *) newPage, firstKeySize);
    memcpy((char *) pushUpKey + PAGE_ID_SIZE + firstKeySize, &numPages, PAGE_ID_SIZE);

    free(newPage);
    free(temp);
    return 0;
}

int IndexManager::getLeafMidPoint(const Attribute &attribute, void *temp, int tempPageEnd, short &oldPageKeyCount, short &newPageKeyCount, short &oldPageFreeSize, short &newPageFreeSize) {
    short i;
    short offset = 0;
    offset = getNextKeyOffset(attribute, offset, temp);
    for (i = 0; i < oldPageKeyCount + 1; i++) {
        if (offset > INDEX_FILE_FREE_SPACE / 2) {
            break;
        }
        offset = getNextKeyOffset(attribute, offset, temp);
    }
    newPageKeyCount = oldPageKeyCount - i;
    oldPageKeyCount = i + 1;
    oldPageFreeSize = INDEX_FILE_FREE_SPACE - offset;
    newPageFreeSize  = INDEX_FILE_FREE_SPACE - (tempPageEnd - offset);
    return offset;
}

RC IndexManager::insertAndSplitIntermediate(IXFileHandle &ixfileHandle, const Attribute &attribute, void *overFlowKey, int pageId, void * pageData, void *pushUpKey) {
    short numSlots = getNumberOfKeys(pageData);
    short freeSize = getFreeSize(pageData);
    short oldPageEnd = INDEX_FILE_FREE_SPACE - freeSize;
    void *temp = malloc(PAGE_SIZE * 2);
    memset(temp, 0, (PAGE_SIZE * 2));
    void *newPage = malloc(PAGE_SIZE);
    memset(newPage, 0, PAGE_SIZE);
    initializeIntermediatePageMetadata(newPage);
    int numPages = ixfileHandle.getNumberOfPages();
    short start, end;
    int keySize = 0, i;

    if (attribute.type == TypeVarChar) {
        memcpy(&keySize, (char *) overFlowKey + PAGE_ID_SIZE, sizeof(int));
    }
    keySize += sizeof(int) + (PAGE_ID_SIZE * 2);

    linearSearch(pageData, attribute, (char *)overFlowKey + PAGE_ID_SIZE, start, end);
    short tempPageEnd = oldPageEnd + keySize - PAGE_ID_SIZE;
    short offsetToSplit = 0;
    if (start == oldPageEnd) {
        memcpy(temp, pageData, oldPageEnd);
        memcpy((char *) temp + start - PAGE_ID_SIZE, overFlowKey, keySize);
    }
    else {
        memcpy(temp, pageData, start);
        memcpy((char *) temp + start - (start != 0 ? PAGE_ID_SIZE : 0), overFlowKey, keySize);
        memcpy((char *)temp + start + keySize - (start != 0 ? PAGE_ID_SIZE : 0), (char *) pageData + start, oldPageEnd - start);
    }
    for (i = 0; i < numSlots + 1; i++) {
        offsetToSplit += PAGE_ID_SIZE;
        if (attribute.type == TypeVarChar) {
            offsetToSplit += *((int *) ((char *) temp + offsetToSplit));
        }
        offsetToSplit += sizeof(int);
        if (offsetToSplit > INDEX_FILE_FREE_SPACE / 2) {
            break;
        }
    }

    int firstKeySize = getIntermediateKeySize((char *)temp + offsetToSplit, attribute);
    memcpy(pageData, temp, offsetToSplit + PAGE_ID_SIZE);
    memcpy(newPage, (char *) temp + offsetToSplit + firstKeySize - PAGE_ID_SIZE, tempPageEnd - (offsetToSplit + firstKeySize - PAGE_ID_SIZE));

    updateNumberOfKeys(i + 1, pageData);
    updateNumberOfKeys(numSlots - i - 1, newPage);
    updatePageType(INTERMEDIATE_PAGE, pageData);
    updatePageType(INTERMEDIATE_PAGE, newPage);
    updateFreeSize(INDEX_FILE_FREE_SPACE - (offsetToSplit + PAGE_ID_SIZE), pageData);
    updateFreeSize(INDEX_FILE_FREE_SPACE - (tempPageEnd - (offsetToSplit + firstKeySize - PAGE_ID_SIZE)), newPage);

    ixfileHandle.appendPage(newPage);

    memcpy(pushUpKey, &pageId, PAGE_ID_SIZE);
    memcpy((char *) pushUpKey + PAGE_ID_SIZE, (char *) temp + offsetToSplit + PAGE_ID_SIZE, firstKeySize - (2*PAGE_ID_SIZE));
    memcpy((char *) pushUpKey + firstKeySize - PAGE_ID_SIZE, &numPages, PAGE_ID_SIZE);

    //cout << "after isplit, freeOld: " << getFreeSize(pageData) << " freeNew: " << getFreeSize(newPage) << endl;

    return 0;
}

RC IndexManager::insert(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid, int keySize, int pageId, void *pageData, bool &split, void *pushUpKey) {
    char pageType = getPageType(pageData);
    short start, end, freeSize;
    freeSize = getFreeSize(pageData);
    int rc;
    split = false;
    bool rootSplit = false;
    if (pageType == LEAF_PAGE) {
        linearSearch(pageData, attribute, (char *) key, start, end);
        if (start != end) {
            keySize = RID_SIZE;
            if(isRIDPresent(pageData, start, end, rid, attribute)) {
                return -1;
            }
        }
        if (freeSize - keySize > 0) {

            //cout << " Free size of " << pageId << " is " << freeSize << endl;
            rc = insertToLeaf(attribute, key, rid, start, end, pageData);
            if (rc != 0) {
                return -1;
            }
            ixfileHandle.writePage(pageId, pageData);
        }
        else {
            insertAndSplitLeaf(ixfileHandle, attribute, key, rid, pageId, pageData, pushUpKey);
            split = true;
            ixfileHandle.writePage(pageId, pageData);
        }
    }
    else {
        linearSearch(pageData, attribute, key, start, end);
        int nextPageId;

        if (start == end) {
            memcpy(&nextPageId, (char *) pageData + start - (start != 0 ? PAGE_ID_SIZE : 0), PAGE_ID_SIZE);
        } else {
            memcpy(&nextPageId, (char *) pageData + end  - PAGE_ID_SIZE, PAGE_ID_SIZE);
        }

        void *nextPageData = malloc(PAGE_SIZE);
        memset(nextPageData, 0, MAX_KEY_SIZE);
        void *overFlowKey = malloc(MAX_KEY_SIZE);
        memset(overFlowKey, 0, MAX_KEY_SIZE);
        bool splitCurrentChild = false;
        ixfileHandle.readPage(nextPageId, nextPageData);

        insert(ixfileHandle, attribute, key, rid, keySize, nextPageId, nextPageData, splitCurrentChild, overFlowKey);

        ixfileHandle.writePage(nextPageId, nextPageData);
        // indicates that the child called in the previous call has been split and needs to be added to the current node.

        if (splitCurrentChild) {
            int newKeySize = getIntermediateKeySize((char *) overFlowKey, attribute);
            // because one pageId will be shared between 2 where it belongs.
            if (freeSize - (newKeySize - PAGE_ID_SIZE) > 0) {
                // This call may be unnecessary.
                linearSearch(pageData, attribute, (char *) overFlowKey + PAGE_ID_SIZE, start, end);
                addKeyToIntermediate(pageData, attribute, overFlowKey, start, end);
                ixfileHandle.writePage(pageId, pageData);
            }
            else {
                // this node needs to be split
                if (getPageType(pageData) == ROOT_PAGE) {
                   rootSplit = true;
                }
                insertAndSplitIntermediate(ixfileHandle, attribute, overFlowKey, pageId, pageData, pushUpKey);
                if (rootSplit) {
                    createNewRoot(ixfileHandle, attribute, pageData, pushUpKey);
                    split = false;
                } else {
                    ixfileHandle.writePage(pageId, pageData);
                    split = true;
                }
            }
        }
        free(nextPageData);
        free(overFlowKey);
    }
    return 0;
}

// done
RC IndexManager::addKeyToIntermediate(void *pageData, const Attribute &attribute, const void *key, short start, short end) {
    short freeSize = getFreeSize(pageData);
    short numKeys = getNumberOfKeys(pageData);
    short lastByte = INDEX_FILE_FREE_SPACE - freeSize;
    int keySize = 0;
    //cout << "adding key,  #K: " << numKeys << " free size: " << freeSize;
    if (attribute.type == TypeVarChar) {
        memcpy(&keySize, (char *) key + PAGE_ID_SIZE, sizeof(int));
    }
    keySize += sizeof(int) + (PAGE_ID_SIZE * 2);

    if (start == lastByte) {
        memcpy((char *) pageData + start - (start != 0 ? PAGE_ID_SIZE : 0), key, keySize);

        updateFreeSize(freeSize - keySize + (start != 0 ? PAGE_ID_SIZE : 0), pageData);
        updateNumberOfKeys(numKeys + 1, pageData);
    }
    else {
        memmove((char *) pageData + start + keySize - PAGE_ID_SIZE, (char *) pageData + start, lastByte - start);
        memcpy((char *) pageData + start - (start != 0 ? PAGE_ID_SIZE : 0), key, keySize);

        if (start == 0) {
            updateFreeSize(freeSize - keySize, pageData);
        }
        else {
            updateFreeSize(freeSize - keySize + PAGE_ID_SIZE, pageData);
        }
        updateNumberOfKeys(numKeys + 1, pageData);
    }

    //cout << ", after insert " << getFreeSize(pageData) << endl;

    return 0;

}

// done
RC IndexManager::insertToLeaf(const Attribute &attribute, const void *key, const RID &rid, const short position, const short positionEnd, void *leafPage) {
    void *data = malloc(MAX_KEY_SIZE);
    memset(data, 0, MAX_KEY_SIZE);
    int secondHalfEnd = INDEX_FILE_FREE_SPACE - getFreeSize(leafPage);
    short numRids = 1;
    int keySize = 0;
    int numRecords = getNumberOfKeys(leafPage);
    short freeSize = getFreeSize(leafPage);
    int offset;
    if (numRecords == 0) {
        memcpy(&keySize, key, sizeof(int));
        memcpy(leafPage, key, sizeof(int));
        offset = sizeof(int);
        if (attribute.type == TypeVarChar) {
            memcpy((char *) leafPage + sizeof(int),(char *) key + sizeof(int), keySize);
            offset += keySize;
        }
        memcpy((char *) leafPage + offset, &numRids, NUM_RIDS_SIZE);
        memcpy((char *) leafPage + offset + NUM_RIDS_SIZE, &(rid.pageNum), PAGE_ID_SIZE);
        memcpy((char *) leafPage + offset + NUM_RIDS_SIZE + PAGE_ID_SIZE, &(rid.slotNum), PAGE_ID_SIZE);
        updateFreeSize(freeSize - offset - NUM_RIDS_SIZE - RID_SIZE, leafPage);
        short prevKeyCount = getNumberOfKeys(leafPage);
        updateNumberOfKeys(prevKeyCount + 1, leafPage);
    }
    else if (position == positionEnd) {
        if (attribute.type == TypeVarChar) {
            memcpy(&keySize, key, sizeof(int));
        }
        keySize += sizeof(int);
        memmove((char *) leafPage + position + keySize + RID_SIZE + NUM_RIDS_SIZE, (char *) leafPage + position,
                secondHalfEnd - position);
        memcpy(data, key, keySize);
        memcpy((char *) data + keySize, &numRids, NUM_RIDS_SIZE);
        memcpy((char *) data + keySize + NUM_RIDS_SIZE, (void *) &(rid.pageNum), PAGE_ID_SIZE);
        memcpy((char *) data + keySize + PAGE_ID_SIZE + NUM_RIDS_SIZE, (void *) &(rid.slotNum), PAGE_ID_SIZE);
        memcpy((char *) leafPage + position, data, keySize + RID_SIZE + NUM_RIDS_SIZE);
        updateFreeSize(freeSize - keySize - RID_SIZE - NUM_RIDS_SIZE, leafPage);
        short prevKeyCount = getNumberOfKeys(leafPage);
        updateNumberOfKeys(prevKeyCount + 1, leafPage);
    }
    else {
        if(isRIDPresent(leafPage, position, positionEnd, rid, attribute)) {
            free(data);
            return -1;
        }
        //cout << "Moving to " << positionEnd + RID_SIZE << " from " << positionEnd << " by " << secondHalfEnd - position << " bytes " ;
        //void *temp = malloc(PAGE_SIZE);
        //memset(temp, 0, PAGE_SIZE);
        memmove((char *) leafPage + positionEnd + RID_SIZE, (char *) leafPage + positionEnd, secondHalfEnd - positionEnd);
        //memcpy(temp, (char *) leafPage + positionEnd, secondHalfEnd - position);
        //memcpy((char *) leafPage + positionEnd + RID_SIZE, temp, secondHalfEnd - position);
        //free(temp);
        if (attribute.type == TypeInt || attribute.type == TypeReal) {
            memcpy(&numRids, (char *) leafPage + position + sizeof(int), NUM_RIDS_SIZE);
            numRids++;
            memcpy((char *) leafPage + position + sizeof(int), &numRids, NUM_RIDS_SIZE);
        }
        else {
            int stringLength;
            memcpy(&stringLength, (char *) leafPage + position, sizeof(int));
            memcpy(&numRids, (char *) leafPage + position + sizeof(int) + stringLength, NUM_RIDS_SIZE);
            numRids++;
            //cout << " num rids: " << numRids << endl;
            memcpy((char *) leafPage + position + sizeof(int) + stringLength, &numRids, NUM_RIDS_SIZE);
        }
        memcpy((char *) leafPage + positionEnd, &(rid.pageNum), PAGE_ID_SIZE);
        memcpy((char *) leafPage + positionEnd + PAGE_ID_SIZE, &(rid.slotNum), SLOT_ID_SIZE);
        updateFreeSize(freeSize - RID_SIZE, leafPage);
    }
//    cout << "new free space: " << getFreeSize(leafPage) << endl;
    free(data);
    return 0;
}

// done
bool IndexManager::isRIDPresent(void *leafPage, short position, short positionEnd, const RID &rid, const Attribute &attribute) {
    int stringLength;
    short ridStart = position;
    short numRids;
    memcpy(&stringLength, (char *) leafPage + position, sizeof(int));
    ridStart += sizeof(int);
    if (attribute.type == TypeVarChar) {
        ridStart +=  stringLength;
    }
    memcpy(&numRids, (char *) leafPage + position, NUM_RIDS_SIZE);
    ridStart += NUM_RIDS_SIZE;
    short pageNum, slotNum; // TODO: Change to int
    for (int i = 0; i < numRids; i++) {
        memcpy(&pageNum, (char *) leafPage + ridStart, PAGE_ID_SIZE);
        memcpy(&slotNum, (char *) leafPage + ridStart + PAGE_ID_SIZE, PAGE_ID_SIZE);
        if (rid.pageNum == pageNum && rid.slotNum == slotNum) {
            return true;
        }
        ridStart += RID_SIZE;
    }
    return false;
}

//done
RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{

	void *rootPage = malloc(PAGE_SIZE);
    memset(rootPage, 0, PAGE_SIZE);
	getRootPage(ixfileHandle, rootPage);
	void *childNode = malloc(PAGE_SIZE);
    memset(childNode, 0, PAGE_SIZE);
	memcpy(childNode, rootPage, PAGE_SIZE);

	int pageID=0;
	short start, end, pageFreeSize, numOfSlots, newFreeSize, newNumSlots;

	short actualSizeUsed, numOfRids, numOfRidOffset, ridStart ;

	treeSearch(ixfileHandle, attribute, key, rootPage, childNode, pageID);
	linearSearch(childNode, attribute, key, start, end);

//	cout<<"start = "<<start<<endl;
//	cout<<"end = "<<end<<endl;

	if(start == end){
		// Key was not found, return error
		free(rootPage);
		free(childNode);
		return -1;
	}

	pageFreeSize = RecordBasedFileManager::instance()->getFreeSpaceSize(childNode);
	actualSizeUsed = INDEX_FILE_FREE_SPACE - pageFreeSize;
//	cout<<"pagefreesize = "<<pageFreeSize<<endl;
//	cout<<"actualSizeUsed = "<<actualSizeUsed<<endl;
	numOfSlots = RecordBasedFileManager::instance()->getNumberOfSlots(childNode);

	switch(attribute.type){
	case TypeInt:
	case TypeReal:
		numOfRidOffset = sizeof(int);
		break;
	case TypeVarChar:
		int length;
		memcpy(&length, (char*)childNode+start, sizeof(int));
		numOfRidOffset = length+sizeof(int);
		break;
	}
	int ridPageId, ridSlotId;
	bool found = false;

	memcpy(&numOfRids, (char*)childNode+start+numOfRidOffset, sizeof(short));
//	cout<<"num of rids = "<<numOfRids<<endl;
	if(numOfRids == 1){
		//There is only one RID associated with the key, so we can delete the entire block
		memcpy(&ridPageId, (char*)childNode+start+numOfRidOffset+NUM_RIDS_SIZE, sizeof(int));
		memcpy(&ridSlotId, (char*)childNode+start+numOfRidOffset+NUM_RIDS_SIZE+sizeof(int), sizeof(int));
		if(ridPageId != rid.pageNum || ridSlotId != rid.slotNum){
//			cout<<"RID not found"<<endl;
			free(rootPage);
			free(childNode);
			return -1;
		}
//		cout<<"Deleting data block size:"<<end-start<<endl;
		deleteSmallBlock(childNode, start, end, actualSizeUsed);
		newFreeSize = pageFreeSize+ (end-start);
		newNumSlots = numOfSlots-1;
		updateFreeSize(newFreeSize, childNode);
//		cout<<"new free size: "<<newFreeSize<<endl;
//		cout<<"newNumSlots: "<<newNumSlots<<endl;
		RecordBasedFileManager::instance()->updateNumberOfSlots(newNumSlots, childNode);
	} else {
		ridStart = start+numOfRidOffset+NUM_RIDS_SIZE;
		for(int i=1;i<=numOfRids;i++){
			memcpy(&ridPageId, (char*)childNode+ridStart, sizeof(int));
			memcpy(&ridSlotId, (char*)childNode+ridStart+sizeof(int), sizeof(int));
			if(ridPageId == rid.pageNum && ridSlotId == rid.slotNum){
				found = true;
				break;
			}
			ridStart += (sizeof(int)+sizeof(int));
		}
		if(found){
			deleteSmallBlock(childNode, ridStart, ridStart+sizeof(int)+sizeof(int), actualSizeUsed);
			newFreeSize = pageFreeSize + (sizeof(int)*2);
			numOfRids = numOfRids-1;
//			cout<<"RIDs reduce to "<<numOfRids<<endl;
			memcpy((char*)childNode+start+numOfRidOffset, &numOfRids, sizeof(short));
			updateFreeSize(newFreeSize, childNode);
		} else {
			// RID not found, return error
//			cout<<"RID not found"<<endl;
			free(rootPage);
			free(childNode);
			return -1;
		}
	}

	ixfileHandle.writePage(pageID, childNode);

	free(rootPage);
	free(childNode);
	return 0;
}

//done nc
RC IndexManager::deleteSmallBlock(void *childNode, short start, short end, short actualSizeUsed){
	//Actually not deleting the block, we just shift the block between [end - actualsizeused] to the point determined by start offset
	memmove((char*)childNode+start, (char*)childNode+end, INDEX_FILE_FREE_SPACE-end);
	return 0;
}

//done
RC IndexManager::scan(IXFileHandle &ixfileHandle,
        const Attribute &attribute,
        const void      *lowKey,
        const void      *highKey,
        bool			lowKeyInclusive,
        bool        	highKeyInclusive,
        IX_ScanIterator &ix_ScanIterator)
{
	int keyCompare = -2;
	int pageID;
	short start, end;
	void *tempLowKey;

	struct stat fileInfo;
	if (stat(ixfileHandle.fileHandle.fileName.c_str(), &fileInfo) != 0) {
		return -1;
	}

	void *rootPage = malloc(PAGE_SIZE);
	memset(rootPage, 0, PAGE_SIZE);
	getRootPage(ixfileHandle, rootPage);

	ix_ScanIterator.initialize(ixfileHandle, attribute);

	void *leafNode = malloc(PAGE_SIZE);
	memcpy(leafNode, rootPage, PAGE_SIZE);
	if(lowKey!=NULL && highKey!=NULL){
		keyCompare = compareValues(attribute, lowKey, highKey);
	}
	if(lowKey == NULL){
		tempLowKey = malloc(sizeof(int));
        memset(tempLowKey, 0, sizeof(int));
		switch(attribute.type){
			case TypeVarChar:
			{
				int vcSize=0;
				memcpy((char*)tempLowKey, &vcSize, sizeof(int));
				break;
			}
			case TypeInt:
			{
				int iValue = std::numeric_limits<int>::min();
				memcpy((char*)tempLowKey, &iValue, sizeof(int));
				break;
			}
			case TypeReal:
			{
				float fValue = std::numeric_limits<float>::min();
				memcpy((char*)tempLowKey, &fValue, sizeof(float));
//				cout<<"fValue:"<<fValue<<endl;
				break;
			}
		}
	}
	if(keyCompare == 0){
//		cout<<"iterate type 1"<<endl;
		treeSearch(ixfileHandle, attribute, lowKey, rootPage, leafNode, pageID);
		linearSearch(leafNode, attribute, lowKey, start, end);
		if(start == end){
			// no entries for this key
			free(rootPage);
			free(leafNode);
			return 0;
		}
		convertAndAddDataToVector(leafNode, attribute, start, end, ix_ScanIterator);
	} else {
		if(lowKey == NULL){
//			cout<<"iterate type 2"<<endl;
			iterateThroughLeafNodes(ixfileHandle, attribute, rootPage, tempLowKey, highKey, lowKeyInclusive, highKeyInclusive, ix_ScanIterator);
			free(tempLowKey);
		} else {
//			cout<<"iterate type 3"<<endl;
			iterateThroughLeafNodes(ixfileHandle, attribute, rootPage, lowKey, highKey, lowKeyInclusive, highKeyInclusive, ix_ScanIterator);
		}
	}

	free(rootPage);
	free(leafNode);
    return 0;
}

//done
RC IndexManager::iterateThroughLeafNodes(IXFileHandle &ixfileHandle, const Attribute attribute, void *rootData, const void* lowKey, const void* highKey,
							bool lowKeyInclusive, bool highKeyInclusive, IX_ScanIterator &ix_ScanIterator){

	int pageID=0;

    short start, end;

	void *leafNode = malloc(PAGE_SIZE);
	memcpy(leafNode, rootData, PAGE_SIZE);
	bool keepIterating = true;
	bool isZeroPageLeaf = false;
	void *key = malloc(attribute.length);
    memset(key, 0, attribute.length);

	treeSearch(ixfileHandle, attribute, lowKey, rootData, leafNode, pageID );
	linearSearch(leafNode, attribute, lowKey, start, end);

	if(pageID == 0){
		// This is a special condition where at the starting of the tree creation, 0th page is both our root and a leaf page
		isZeroPageLeaf = true;
	}
//	cout<<"starting search at Page id :"<<pageID<<endl;

	short pageFreeSpace = RecordBasedFileManager::instance()->getFreeSpaceSize(leafNode);
//	short numOfSlots = RecordBasedFileManager::instance()->getNumberOfSlots(leafNode);
//	cout<<"pageusedspace :"<<INDEX_FILE_FREE_SPACE-pageFreeSpace<<endl;
//	cout<<"numOfSlots :"<<numOfSlots<<endl;

	while(keepIterating){

		// Go to next page if last key is read
		if(start >= INDEX_FILE_FREE_SPACE - pageFreeSpace){
			if(isZeroPageLeaf){
				//At this point, we have only one page in the tree, the 0th page. So, the first time we exceed page size limit,
				//we can exit scan because we know that there are no other pages and checking getNextLeafPage in this condition will
				// loop back to page 0 infinitely
				keepIterating = false;
				break;
			}
			int nextLeafPage = getNextLeafPage(leafNode);
			if(nextLeafPage == -1){
				keepIterating = false;
				break;
			}
			ixfileHandle.readPage(nextLeafPage, leafNode);
			pageFreeSpace = RecordBasedFileManager::instance()->getFreeSpaceSize(leafNode);
			start = 0;
		}

		end = getNextKeyOffset(attribute, start, leafNode);
		if(keyGreaterThanHighKey(attribute, leafNode, start, end, highKey, key)){
			keepIterating = false;
			break;
		}

		if(!keyNeedsToBeIgnoredDueToInclusives(attribute, key, lowKey, highKey, lowKeyInclusive, highKeyInclusive) ){
			convertAndAddDataToVector(leafNode, attribute, start, end, ix_ScanIterator);
		}
		start = end;
	}

	free(key);
	free(leafNode);
	return 0;
}

//done nc
bool IndexManager::keyGreaterThanHighKey(const Attribute &attribute, void *leafNode, short start, short end, const void *highKey, void *key){

	switch (attribute.type){
		case TypeVarChar:{
			int length;
			memcpy(&length, (char*)leafNode+start, sizeof(int));
			memcpy((char*)key, &length, sizeof(int));
			memcpy((char*)key+sizeof(int), (char*)leafNode+start+sizeof(int), length);
			break;
		}
		case TypeInt:
		case TypeReal:{
			memcpy((char*)key, (char*)leafNode+start, sizeof(int));
			break;
		}
	}
	// if highkey is null, we keep iterating till the end
	if(highKey == NULL){
		return false;
	}
	if( compareValues(attribute, key, highKey) == 1){
		return true;
	}
	return false;
}

//done nc
bool IndexManager::keyNeedsToBeIgnoredDueToInclusives(const Attribute &attribute,void *key, const void *lowKey, const void *highKey,
														bool lowKeyInclusive, bool highKeyInclusive){

	if(compareValues(attribute, key, lowKey) == 0){
		if(!lowKeyInclusive){
			return true;
		}
	}
	if(highKey==NULL){
		return false;
	}
	if(compareValues(attribute, key, highKey) == 0){
		if(!highKeyInclusive){
			return true;
		}
	}
	return false;
}

//done
RC IndexManager::convertAndAddDataToVector(void *leafNode, const Attribute &attribute, short start, short end, IX_ScanIterator &ix_ScanIterator){
	short numOfRIDS;
	int pageId, slotID, length;
	short offset = start;
	void *ridList = malloc(end-start);
	void *key, *temp;
	string vcKey;
	int iValue;
	float fValue;
	memset(ridList, 0, end-start);
	switch (attribute.type){
	case TypeVarChar:
		memcpy(&length, (char*)leafNode+offset, sizeof(int));
		offset+=sizeof(int);
//		cout<<"Key length = "<<length<<endl;
		temp = malloc(length);
		memcpy((char*)temp, (char*)leafNode+offset, length);
		offset+=length;
		break;
	case TypeInt:
		key=malloc(sizeof(int));
		memcpy(&iValue, (char*)leafNode+offset, sizeof(int));
		memcpy((char*)key, (char*)leafNode+offset, sizeof(int));
		offset+=sizeof(int);

           // cout<<"iValue = "<<iValue<<endl;
		break;
	case TypeReal:
		key=malloc(sizeof(int));
		memcpy(&fValue, (char*)leafNode+offset, sizeof(int));
		memcpy((char*)key, (char*)leafNode+offset, sizeof(int));
		offset+=sizeof(int);
		break;
	}
	memcpy(&numOfRIDS, (char*)leafNode+offset, sizeof(short));
//	cout<<"numOfRIDS "<<numOfRIDS<<endl;
	offset+=sizeof(short);
	for(int i=1; i<=numOfRIDS; i++){
		memcpy(&pageId, (char*)leafNode+offset, sizeof(int));
		offset+=sizeof(int);
		memcpy(&slotID, (char*)leafNode+offset, sizeof(int));
		offset+=sizeof(int);
		RID rid;
        rid.pageNum = pageId;
        rid.slotNum = slotID;
//        cout<<"Adding rid "<<pageId<<" "<<slotID<<endl;
		ix_ScanIterator.addRIDToVector(rid);
		if(attribute.type == TypeVarChar){
			string vcKey(static_cast<const char*>(temp), length);
//			cout<<"Key = "<<vcKey<<endl;
			ix_ScanIterator.addKeyValueToVector(vcKey);
		} else if (attribute.type == TypeInt){
//			cout<<"iValue = "<<iValue<<endl;
			ix_ScanIterator.addIntValueToVector(iValue);
		} else {
//		cout<<"fValue = "<<fValue<<endl;
			ix_ScanIterator.addFloatValueToVector(fValue);
		}

	}
	if(attribute.type == TypeVarChar){
		free(temp);
	} else {
		free(key);
	}
	free(ridList);
	return 0;
}

//done nc
void IndexManager::printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const {
	void *rootPage = malloc(PAGE_SIZE);
	memset(rootPage,0,PAGE_SIZE);
	getRootPage(ixfileHandle, rootPage);
	printBtreeRecursive(ixfileHandle, attribute, rootPage, 0);
	free(rootPage);
}

//done nc
void IndexManager::printTabs(int numOfTabs) const {
	for (int i = 0; i < numOfTabs; i++) {
		cout << "\t";
	}
}

//done nc
void IndexManager::printComma(bool isFirst) const {
	if(!isFirst){
		cout<<",";
	}
}

//done
void IndexManager::printBtreeRecursive(IXFileHandle &ixfileHandle, const Attribute &attribute, void *pageData, int numOfTabs) const {
	vector<int> pageIDVector;
	char pageType = getPageType(pageData);
	short pageFreeSpace = getFreeSize(pageData);
	short offset = 0;
	void *childPage = malloc(PAGE_SIZE);
    memset(childPage, 0, PAGE_SIZE);
	bool firstKey = true, firstChild = true;

	printTabs(numOfTabs);
	if(pageType == LEAF_PAGE){
		short numOfRIDS = 0;
		cout << "{";
		cout<<"\"keys\": [";
		while(offset< INDEX_FILE_FREE_SPACE-pageFreeSpace){
			printComma(firstKey);
			cout<<"\"";
			switch(attribute.type){
				case TypeVarChar:
				{
					int length;
					memcpy(&length, (char*)pageData+offset, sizeof(int));
					offset+=sizeof(int);
					char temp[length];
					memcpy(&temp, (char*)pageData+offset, length);
                    offset += length;

					for(int i=0 ; i<length ; i++)
					{
					  cout << temp[i];
					}
					break;
				}
				case TypeInt:
				{
					int iValue;
					memcpy(&iValue, (char*)pageData+offset, sizeof(int));
					cout<<iValue;
					offset+=sizeof(int);
					break;
				}
				case TypeReal:
				{
					float fValue;
					memcpy(&fValue, (char*)pageData+offset, sizeof(float));
					cout<<fValue;
					offset+=sizeof(float);
					break;
				}
			}
			firstKey=false;
			cout<<":[";
			memcpy(&numOfRIDS, (char*)pageData+offset, sizeof(short));
			offset+=sizeof(short);
			for(int i=0; i<numOfRIDS; i++){
				int pageID,slotID;
				memcpy(&pageID, (char*)pageData+offset, sizeof(int));
				offset+=sizeof(int);
				memcpy(&slotID, (char*)pageData+offset, sizeof(int));
				offset+=sizeof(int);
				cout<<"("<<pageID<<","<<slotID<<")";
				cout<<",";
			}
			cout<<"]\"";
		}
		cout<<"]";
	} else {
		int pageID;
		if(offset<= INDEX_FILE_FREE_SPACE-pageFreeSpace){
			memcpy(&pageID, (char*)pageData+offset, sizeof(int));
			pageIDVector.push_back(pageID);
			offset+=sizeof(int);
		}
		cout << "{"<<endl;
		cout<<"\"keys\": [";
		while(offset< INDEX_FILE_FREE_SPACE-pageFreeSpace){
			printComma(firstKey);
			cout<<"\"";
			switch(attribute.type){
				case TypeVarChar:
				{
					int length;
					memcpy(&length, (char*)pageData+offset, sizeof(int));
					offset+=sizeof(int);
					char temp[length];
					memcpy(&temp, (char*)pageData+offset, length);
                    offset += length;
					for(int i=0 ; i<length ; i++)
					{
					  cout << temp[i];
					}
					break;
				}
				case TypeInt:
				{
					int iValue;
					memcpy(&iValue, (char*)pageData+offset, sizeof(int));
					cout<<iValue;
					offset+=sizeof(int);
					break;
				}
				case TypeReal:
				{
					float fValue;
					memcpy(&fValue, (char*)pageData+offset, sizeof(float));
					cout<<fValue;
					offset+=sizeof(float);
					break;
				}
			}
			memcpy(&pageID, (char*)pageData+offset, sizeof(int));
			pageIDVector.push_back(pageID);
			offset+=sizeof(int);
			cout<<"\"";
			firstKey=false;
		}
		cout<<"],";
		cout<<endl;
		printTabs(numOfTabs);
		cout<<"\"children\":["<<endl;
		for(unsigned int i=0; i<pageIDVector.size(); i++){
			memset(childPage,0,PAGE_SIZE);
			ixfileHandle.readPage(pageIDVector[i], childPage);
			pageFreeSpace = getFreeSize(childPage);
			if(pageFreeSpace == INDEX_FILE_FREE_SPACE){
				continue;
			}
			printComma(firstChild);
			printBtreeRecursive(ixfileHandle, attribute, childPage, numOfTabs+1);
			firstChild = false;
		}
		pageIDVector.clear();
		cout<<"]";
	}
	cout<<"}";
	cout<<endl;
	free(childPage);
}

//done nc
void IndexManager::initializeRootPageMetadata(void *data){
	initializeCommonPageMetadata(data);
	updatePageType(ROOT_PAGE, data);
	updateRootPage(0, data);
}

//done nc
void IndexManager::initializeIntermediatePageMetadata(void *data){
	initializeCommonPageMetadata(data);
	updatePageType(INTERMEDIATE_PAGE, data);
	updateRootPage(0, data);
}

//done nc
void IndexManager::initializeLeafPageMetadata(void *data){
	initializeCommonPageMetadata(data);
	updatePageType(LEAF_PAGE, data);
	updateNextLeafPage(-1, data);
}

//done nc
void IndexManager::initializeCommonPageMetadata(void *data){
	RecordBasedFileManager::instance()->updateFreeSpaceSize(INDEX_FILE_FREE_SPACE, data);
	RecordBasedFileManager::instance()->updateNumberOfSlots(0, data);
	updatePageType(UNKNOWN_PAGE, data);
	updateParentPage(0, data);
}

//done nc
RC IndexManager::updatePageType(char pageType, void *data) {
	memcpy((char*)data + PAGE_TYPE_POSITION, &pageType, sizeof(char));
    return 0;
}

//done
RC IndexManager::updateParentPage(int parentPage, void *data) {
	memcpy((char*)data + PARENT_PAGE_POSITION, &parentPage, sizeof(int));
    return 0;
}

//done
RC IndexManager::updateRootPage(int rootPage, void *data) {
	memcpy((char*)data + ROOT_PAGE_POSITION, &rootPage, sizeof(int));
    return 0;
}

//done
RC IndexManager::updateNextLeafPage(int nextLeafPage, void *data) {
	memcpy((char*)data + NEXT_LEAF_PAGE_POSITION, &nextLeafPage, sizeof(int));
    return 0;
}

//done nc
char IndexManager::getPageType(void *data) const{
	char pageType;
	memcpy(&pageType, (char*)data + PAGE_TYPE_POSITION, sizeof(char));
	return pageType;
}

//done
int IndexManager::getParentPage(void *data){
	int parentPage;
	memcpy(&parentPage, (char*)data + PARENT_PAGE_POSITION, sizeof(int));
	return parentPage;
}

//done
int IndexManager::getRootPageID(void *data) const{
	int rootPage;
	memcpy(&rootPage, (char*)data + ROOT_PAGE_POSITION, sizeof(int));
	return rootPage;
}

//done
int IndexManager::getNextLeafPage(void *data){
	int nextLeafPage;
	memcpy(&nextLeafPage, (char*)data + NEXT_LEAF_PAGE_POSITION, sizeof(int));
	return nextLeafPage;
}

//done
void IndexManager::getRootPage(IXFileHandle &ixFileHandle, void *pageData) const{
	ixFileHandle.readPage(0, pageData);
    /*int rootPageId = getRootPageID(pageData);
    if (rootPageId != 0) {
    	ixFileHandle.readPage(rootPageId, pageData);
    }*/
}

//done nc
IX_ScanIterator::IX_ScanIterator()
{
	currentRIDPosition = 0;
}

//done nc
IX_ScanIterator::~IX_ScanIterator()
{
}

//done nc
void IX_ScanIterator::initialize(IXFileHandle &ixFileHandle, const Attribute &attribute){
	this->ixFileHandle = &ixFileHandle;
	this->attribute = attribute;

}

//done nc
void IX_ScanIterator::addRIDToVector(RID rid){
	ridVector.push_back(rid);
}

//done nc
void IX_ScanIterator::addKeyValueToVector(void *key){
	keyVector.push_back(key);
}

void IX_ScanIterator::addKeyValueToVector(string key){
	vcVector.push_back(key);
}

void IX_ScanIterator::addIntValueToVector(int key){
	intVector.push_back(key);
}

void IX_ScanIterator::addFloatValueToVector(float key){
	floatVector.push_back(key);
}

//done nc
RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
	if(ridVector.size() == 0){
		return IX_EOF;
	}
	if(currentRIDPosition >= ridVector.size()){
		return IX_EOF;
	}

	rid = ridVector[currentRIDPosition];

	switch(attribute.type){
	case TypeInt:
		*(int*)key=intVector[currentRIDPosition];
		break;
	case TypeReal:
//		memcpy(&key, (char*)floatVector[currentRIDPosition], sizeof(int));
		*(float*)key=floatVector[currentRIDPosition];
		break;
	case TypeVarChar:

		string temp = vcVector[currentRIDPosition];
		int length=temp.size();
//		cout<<"Returned key length :"<<length<<endl;
//		cout<<"Returned key: "<<temp<<endl;
		char* cTemp = (char*)temp.c_str();
		memcpy((char*)key, &length, sizeof(int));
		memcpy((char*)key+sizeof(int), cTemp, length);

	}

	currentRIDPosition++;
    return 0;
}

//done nc
RC IX_ScanIterator::close()
{
	currentRIDPosition = 0;
	ridVector.clear();
	keyVector.clear();
	intVector.clear();
	floatVector.clear();
	vcVector.clear();
    return 0;
}

//done nc
IXFileHandle::IXFileHandle()
{
    ixReadPageCounter = 0;
    ixWritePageCounter = 0;
    ixAppendPageCounter = 0;
    fileHandle.fileName = string();
}

//done nc
IXFileHandle::~IXFileHandle()
{
}

//done nc
RC IXFileHandle::readPage(PageNum pageNum, void *data)
{
    if ( fileHandle.readPage(pageNum, data) != -1 )
    {
    	ixReadPageCounter++;
    	return 0;
    }
    return -1;
}

//done nc
RC IXFileHandle::writePage(PageNum pageNum, const void *data)
{
    if ( fileHandle.writePage(pageNum, data) != -1 )
    {
    	ixWritePageCounter++;
    	return 0;
    }
    return -1;
}

//done nc
RC IXFileHandle::appendPage(const void *data)
{
    if ( fileHandle.appendPage(data) != -1 )
    {
        ixAppendPageCounter++;
        return 0;
    }
    return -1;
}

//done nc
unsigned IXFileHandle::getNumberOfPages()
{
    return fileHandle.getNumberOfPages();
}

//done nc
RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    readPageCount = ixReadPageCounter;
    writePageCount = ixWritePageCounter;
    appendPageCount = ixAppendPageCounter;
    return 0;
}

// TODO: move to utils.
int IndexManager::compareValues(const Attribute &attribute, const void *key, const void *value) {
    if (attribute.type == TypeInt) {
        if (*((int *) key) == *((int *) value)) return 0;
        if (*((int *) key) < *((int *) value)) return -1;
        if (*((int *) key) > *((int *) value)) return 1;
    }
    else if (attribute.type == TypeReal) {
        if (*((float *) key) == *((float *) value)) return 0;
        if (*((float *) key) < *((float *) value)) return -1;
        if (*((float *) key) > *((float *) value)) return 1;
    }
    else {
        int keyLen, valueLen;
        memcpy(&keyLen, key, sizeof(int));
        memcpy(&valueLen, value, sizeof(int));
        return compareValues(attribute, key, value, keyLen, valueLen);
    }
}

int IndexManager::compareValues(const Attribute &attribute, const char *key, int keyLen, void *value) {
    int valueLen;
    memcpy(&valueLen, value, sizeof(int));
    string kString(key, keyLen);
    string vString(static_cast<const char *>((char *) value + sizeof(int)), valueLen);
    return kString.compare(vString);
}

// TODO: Move to utils
int IndexManager::compareValues(const Attribute &attribute, const void *key, const void *value, int keyLen, int valueLen) {
    string kString (static_cast<const char *>((char *) key + sizeof(int)), keyLen);
    string vString (static_cast<const char *>((char *) value + sizeof(int)), valueLen);
    int result = kString.compare(vString);
//    cout << "comapring " << kString << " with " << vString << " result: " << result << endl;
    return result;
}

/**
 * Return the start and end pointer where the data belongs.
 * If there is no data, then start == end and that is the position
 * where the new node can be inserted.
 * This method searches both a leaf and an intermediate node.
 *
 * If its an intermediate node, the start pointer is pointing to the first byte of the value
 * and the end pointer is pointing to the first byte of the next value. There will always be
 * a pageID to the left of the start pointer (except the case where the leaf/intermediate node is empty)
 * and a pageID between start and end. (last 2 bytes).
 */
RC IndexManager::linearSearch(void *pageData, const Attribute &attribute, const void *key, short &start, short &end) {
    start = end = 0;
    short numSlots = getNumberOfKeys(pageData);
    char pageType = getPageType(pageData);
    // no slots, the record needs to be inserted into the first position.
    if (numSlots == 0) {
        return 0;
    }
    int prevLength, currentLength, compareResult, comparePrev = 0;
    bool found;

    start = 0; end = 0; found = false;
    if (pageType != LEAF_PAGE) {
        start = PAGE_ID_SIZE;
    }
    for (int i = 0; i < numSlots; i++) {
        if (attribute.type == TypeVarChar) {
            memcpy(&currentLength, (char *) pageData + start, sizeof(int));
        }
        compareResult = compareValues(attribute, key, (char *)pageData + start);
        //cout << "Comparing with " << *(int *)((char *) pageData + start)<< " i " << i << " " << numSlots << endl;
        if (compareResult < 0 && start <= PAGE_ID_SIZE) {
            // key is less than the first value in the node.
            found = true;
            end = start;
            break;
        }
        else if (compareResult == 0) {
            if (pageType != LEAF_PAGE) {
                end = start + PAGE_ID_SIZE + sizeof(int) + (attribute.type == TypeVarChar ? currentLength : 0);
            } else {
                end = getNextKeyOffset(attribute, start, pageData);
            }
            //end = start + sizeof(int) + RID_SIZE + (attribute.type == TypeVarChar ? currentLength : 0);
            found = true;
            break;
        }
        else if (comparePrev > 0 && compareResult < 0) {
            end = start;
            //start = end - RID_SIZE - sizeof(int) - (attribute.type == TypeVarChar ? prevLength : 0);
            found = true;
            break;
        }
        if (pageType != LEAF_PAGE) {
            start += (PAGE_ID_SIZE) + sizeof(int) + (attribute.type == TypeVarChar ? currentLength : 0);
        } else {
            start = getNextKeyOffset(attribute, start, pageData);
        }
        //start += sizeof(int) + RID_SIZE + (attribute.type == TypeVarChar ? currentLength : 0);
        comparePrev = compareResult;
        prevLength = currentLength;
    }

    if (compareResult > 0 && !found) {
        end = start;
        //start = end - RID_SIZE - sizeof(int) - (attribute.type == TypeVarChar ? prevLength : 0);
    }
    return 0;
}

short IndexManager::getNextKeyOffset(const Attribute &attribute, short int currentOffset, void *pageData) {
    short numRids;
    if (attribute.type == TypeInt || attribute.type == TypeReal) {
        memcpy(&numRids, (char *) pageData + currentOffset + sizeof(int), sizeof(short));
        return currentOffset + (RID_SIZE * numRids) + sizeof(int) + NUM_RIDS_SIZE;
    }
    else {
        int strLength;
        memcpy(&strLength, (char *) pageData + currentOffset, sizeof(int));
        memcpy(&numRids, (char *) pageData + currentOffset + sizeof(int) + strLength, sizeof(short));
        return currentOffset + sizeof(int) + strLength + NUM_RIDS_SIZE + (numRids * RID_SIZE);
    }
}

RC IndexManager::treeSearch(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, void *pageData, void *leafNode, int &pageId) {
    char pageType = getPageType(leafNode);
    if (pageType == LEAF_PAGE) {
        //memcpy(leafNode, pageData, PAGE_SIZE);

        return 0;
    }

    int numKeys = getNumberOfKeys(leafNode);
    if (numKeys == 0 && pageType == ROOT_PAGE) {
        ixfileHandle.readPage(2, leafNode);
        pageId = 2;
        return 0;
    }

    short start, end;
    linearSearch(leafNode, attribute, key, start, end);
    short rightPageId, leftPageId;

    if (start == end) {
        memcpy(&pageId, (char *) leafNode + start - PAGE_ID_SIZE, PAGE_ID_SIZE);
        ixfileHandle.readPage(pageId, leafNode);

        treeSearch(ixfileHandle, attribute, key, pageData, leafNode, pageId);
    }
    else {
        memcpy(&pageId, (char *) leafNode + end - PAGE_ID_SIZE, PAGE_ID_SIZE);

            ixfileHandle.readPage(pageId, leafNode);
            treeSearch(ixfileHandle, attribute, key, pageData, leafNode, pageId);

    }

    return 0;
}

RC IndexManager::getSibling() {
    return 0;
}

//done
void IndexManager::calculateLeafNodeSplitPoints(const Attribute &attribute, unsigned short oldPageNumOfSlots, unsigned short & dataCopyStartPoint,
									unsigned short dataCopyEndPoint, void* oldPageData, unsigned short & newPageNumSlots, unsigned short & dataLength) {
	int offset = 0;
	int length = 0;
	short ridlistLength = 0;

	for(unsigned int i=1; i<=oldPageNumOfSlots; i++){
		if(attribute.type ==  TypeVarChar){
			memcpy(&length, (char*) oldPageData + offset, sizeof(int));
            offset = offset + sizeof(int) + length;
            //offset += sizeof(int) + length;
			memcpy(&ridlistLength, (char*) (oldPageData) + offset, sizeof(short));
			offset += sizeof(short) + ridlistLength*(sizeof(int)+sizeof(int));

		} else {
			offset += sizeof(int);
			memcpy(&ridlistLength, (char*) (oldPageData) + offset, sizeof(short));
			offset += sizeof(short) + ridlistLength*(sizeof(int)+sizeof(int));

		}
		if(offset >= INDEX_FILE_FREE_SPACE /2 && dataCopyStartPoint == 0){
			newPageNumSlots = oldPageNumOfSlots-i;
			dataCopyStartPoint=offset;
		}
	}
	dataCopyEndPoint=offset;
	dataLength=dataCopyEndPoint-dataCopyStartPoint;
}

RC IndexManager::splitLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, void *oldPageData, void *newPageData, int &newPageID ){

	unsigned short oldPageNumOfSlots = RecordBasedFileManager::instance()->getNumberOfSlots(oldPageData);
	unsigned short oldPageFreeSize = RecordBasedFileManager::instance()->getFreeSpaceSize(oldPageData);
//	cout<<"old page data: numslots: "<<oldPageNumOfSlots<<endl;
//	cout<<"old page data: free size: "<<oldPageFreeSize<<endl;
//	cout<<"old page data: used size: "<<INDEX_FILE_FREE_SPACE-oldPageFreeSize<<endl;
	int currentNoOfPages = ixfileHandle.getNumberOfPages();
	int oldPageNextPagePointer = getNextLeafPage(oldPageData);
	int oldPageParentPagePointer = getParentPage(oldPageData);
	unsigned short newPageFreeSize;
	unsigned short dataLength, newPageNumSlots;
	unsigned short dataCopyStartPoint=0, dataCopyEndPoint=0;

	memset(newPageData, 0 , PAGE_SIZE);
	initializeLeafPageMetadata(newPageData);
	newPageFreeSize = RecordBasedFileManager::instance()->getFreeSpaceSize(newPageData);

	calculateLeafNodeSplitPoints(attribute, oldPageNumOfSlots, dataCopyStartPoint, dataCopyEndPoint, oldPageData, newPageNumSlots, dataLength);

	memcpy((char*)newPageData, (char*)oldPageData+dataCopyStartPoint, dataLength);
	void *zeroData = malloc(dataLength);
	memset(zeroData, 0, dataLength);
	memcpy((char*)oldPageData+dataCopyStartPoint, (char*)zeroData, dataLength);

//	cout<<"After split****"<<endl;
	RecordBasedFileManager::instance()->updateNumberOfSlots(oldPageNumOfSlots - newPageNumSlots, oldPageData);
//	cout<<"old page data: number of slots: "<<oldPageNumOfSlots - newPageNumSlots<<endl;
	RecordBasedFileManager::instance()->updateFreeSpaceSize(oldPageFreeSize+dataLength ,oldPageData);
//	cout<<"old page data: free size: "<<oldPageFreeSize+dataLength<<endl;
//	cout<<"old page data: used size: "<<INDEX_FILE_FREE_SPACE-(oldPageFreeSize+dataLength)<<endl;
	updateNextLeafPage(currentNoOfPages, oldPageData);

	RecordBasedFileManager::instance()->updateNumberOfSlots(newPageNumSlots, newPageData);
//	cout<<"new page data: number of slots: "<<newPageNumSlots<<endl;
	RecordBasedFileManager::instance()->updateFreeSpaceSize(newPageFreeSize-dataLength ,newPageData);
//	cout<<"new page data: free size: "<<newPageFreeSize-dataLength<<endl;
//	cout<<"new page data: used size: "<<INDEX_FILE_FREE_SPACE-(newPageFreeSize-dataLength)<<endl;
	updateNextLeafPage(oldPageNextPagePointer, newPageData);
	updateParentPage(oldPageParentPagePointer, newPageData);

	newPageID = currentNoOfPages;

	free(zeroData);
	return 0;
}


void IndexManager::calculateIntermediateSplitPointsIntReal(unsigned short oldPageNumOfSlots, unsigned short &newPageNumSlots, unsigned short &newPageFreeSpaceDecrement,
											unsigned short &dataCopyStartPoint, unsigned short &dataCopyEndPoint, unsigned short &dataLength) {
	if (oldPageNumOfSlots % 2 == 0) {
		newPageNumSlots = oldPageNumOfSlots / 2;
	} else {
		newPageNumSlots = oldPageNumOfSlots / 2 + 1;
	}
	newPageFreeSpaceDecrement = (newPageNumSlots*4) + (newPageNumSlots+1)*2;
	dataCopyStartPoint = (oldPageNumOfSlots/2)*4 + (oldPageNumOfSlots/2 + 1)*2;
	dataCopyEndPoint = oldPageNumOfSlots*4 + (oldPageNumOfSlots+1)*2;
	dataLength = dataCopyEndPoint-dataCopyStartPoint;
}

void IndexManager::calculateIntermediateSplitPointsVarchar(unsigned short oldPageNumOfSlots, unsigned short & dataCopyStartPoint,
									unsigned short dataCopyEndPoint, void* oldPageData, unsigned short & newPageNumSlots, unsigned short & dataLength,
									unsigned short & newPageFreeSpaceDecrement) {
	short length;
	unsigned short varcharOffset=2;
	for (unsigned int i = 1; i < oldPageNumOfSlots; i++) {
		memcpy(&length, (char*) (oldPageData) + varcharOffset, sizeof(int));
		varcharOffset += sizeof(int) + length + 2;
		if (varcharOffset >= PAGE_SIZE / 2 && dataCopyStartPoint == 0) {
			dataCopyStartPoint = varcharOffset;
			newPageNumSlots = oldPageNumOfSlots - i;

		}
	}
	dataCopyEndPoint = varcharOffset;
	dataLength = dataCopyEndPoint - dataCopyStartPoint;
	newPageFreeSpaceDecrement = dataLength;
}
