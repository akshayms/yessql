#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <string>

#include "../rbf/rbfm.h"

# define IX_EOF (-1)  // end of the index scan


#define INDEX_FILE_FREE_SPACE 4083
#define ROOT_PAGE '0'
#define INTERMEDIATE_PAGE '1'
#define LEAF_PAGE '2'
#define UNKNOWN_PAGE '3'
#define PAGE_TYPE_POSITION 4091
#define PARENT_PAGE_POSITION 4087
#define NEXT_LEAF_PAGE_POSITION 4083
#define ROOT_PAGE_POSITION 4083
//#define FIRST_KEY_OFFSET 2
#define MAX_KEY_SIZE 3000 // Maximum size of a key
#define MIN_ENTRY_SIZE 12 // PageNum on either side of 4 bytes of data
#define RID_SIZE 8
#define PAGE_ID_SIZE 4
#define NUM_RIDS_SIZE 2
#define SLOT_ID_SIZE 4


class IX_ScanIterator;
class IXFileHandle;

class IndexManager {

    public:
        static IndexManager* instance();

        // Create an index file.
        RC createFile(const string &fileName);

        // Delete an index file.
        RC destroyFile(const string &fileName);

        // Open an index and return an ixfileHandle.
        RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

        // Close an ixfileHandle for an index.
        RC closeFile(IXFileHandle &ixfileHandle);

        // Insert an entry into the given index that is indicated by the given ixfileHandle.
        RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Delete an entry from the given index that is indicated by the given ixfileHandle.
        RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Initialize and IX_ScanIterator to support a range search
        RC scan(IXFileHandle &ixfileHandle,
                const Attribute &attribute,
                const void *lowKey,
                const void *highKey,
                bool lowKeyInclusive,
                bool highKeyInclusive,
                IX_ScanIterator &ix_ScanIterator);

        // Print the B+ tree in pre-order (in a JSON record format)
        void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const;

    inline short getFreeSize(void *pageData) const { return RecordBasedFileManager::instance()->getFreeSpaceSize(pageData); }

    int compareValues(const Attribute &attribute, const void *key, const void *value, int keyLen, int valueLen);
    int compareValues(const Attribute &attribute, const void *key, const void *value);
    int compareValues(const Attribute &attribute, const char *key, int keyLen, void *value);

    protected:
        IndexManager();
        ~IndexManager();

    private:
        static IndexManager *_index_manager;

        void initializeRootPageMetadata(void *data);
        void initializeIntermediatePageMetadata(void *data);
        void initializeLeafPageMetadata(void *data);
        void initializeCommonPageMetadata(void *data);

        RC updatePageType(char pageType, void *data);
        RC updateParentPage(int parentPage, void *data);
        RC updateRootPage(int rootPage, void *data);
        RC updateNextLeafPage(int leafPage, void *data);

        char getPageType(void *data) const;
        int getParentPage(void *data);
        int getRootPageID(void *data) const;

        void getRootPage(IXFileHandle &ixFileHandle, void *data) const;

        int getNextLeafPage(void *data);
        inline short getNumberOfKeys(void *pageData) { return RecordBasedFileManager::instance()->getNumberOfSlots(pageData); }

        inline RC updateFreeSize(short freeSize, void *pageData) { return RecordBasedFileManager::instance()->updateFreeSpaceSize(freeSize, pageData); }
        inline RC updateNumberOfKeys(short keys, void *pageData) { return RecordBasedFileManager::instance()->updateNumberOfSlots(keys, pageData); }


        RC linearSearch(void *pageData, const Attribute &attribute, const void *key, short &start, short &end);
        RC treeSearch(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, void *pageData, void *leafNode, int &pageId);

        RC findLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, void *key, void *leafNode, int &leafPageId);

        RC getSibling();

        RC splitLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, void *oldPageData, void *newPageData, int &newPageID);

        RC insertToLeaf(const Attribute &attribute, const void *key, const RID &rid, const short position, const short positionEnd, void *leafPage);

        void calculateLeafNodeSplitPoints(const Attribute &attribute, unsigned short oldPageNumOfSlots, unsigned short & dataCopyStartPoint,
										unsigned short dataCopyEndPoint, void* oldPageData, unsigned short & newPageNumSlots, unsigned short & dataLength);

        void calculateIntermediateSplitPointsIntReal(unsigned short oldPageNumOfSlots, unsigned short &newPageNumSlots, unsigned short &newPageFreeSpaceDecrement,
        											unsigned short &dataCopyStartPoint, unsigned short &dataCopyEndPoint, unsigned short &dataLength);
        void calculateIntermediateSplitPointsVarchar(unsigned short oldPageNumOfSlots, unsigned short & dataCopyStartPoint,
        									unsigned short dataCopyEndPoint, void* oldPageData, unsigned short & newPageNumSlots, unsigned short & dataLength,
        									unsigned short & newPageFreeSpaceDecrement);
        //RC splitIntermediateNode(IXFileHandle &ixfileHandle, const Attribute &attribute, void *oldPageData, void *newPageData, short newChildPage, short &newPageID);

    // pageID of key is the return value from the function with the page id where copyUpKey is inserted.
        int insertToIntermediateNode(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, int parent, int &copyKeyPage);

        short getNextKeyOffset(const Attribute &attribute, short int currentOffset, void *pageData);

        RC addKeyToIntermediate(void *pageData, const Attribute &attribute, const void *key, short start, short end);
    RC splitIntermediateNode(const Attribute &attribute, void *pageData, void *newPageData, const void *key, short start, short end, bool &keyInNewPage);


        RC convertAndAddDataToVector(void *leafNode, const Attribute &attribute, short start, short end, IX_ScanIterator &ix_ScanIterator);
        RC iterateThroughLeafNodes(IXFileHandle &ixfileHandle, const Attribute attribute, void *rootData, const void* lowKey, const void* highKey,
        							bool lowKeyInclusive, bool highKeyInclusive, IX_ScanIterator &ix_ScanIterator);

        bool keyGreaterThanHighKey(const Attribute &attribute, void *leafNode, short start, short end, const void *highKey, void *key);
        bool keyNeedsToBeIgnoredDueToInclusives(const Attribute &attribute, void *key, const void *lowKey, const void *highKey,
        										bool lowKeyInclusive, bool highKeyInclusive);

        void printBtreeRecursive(IXFileHandle &ixfileHandle, const Attribute &attribute, void *pageData, int numOfTabs) const;
        void printTabs(int numOfTabs) const;
        void printComma(bool isFirst) const;

        RC deleteSmallBlock(void *childNode, short start, short end, short actualSizeUsed);

    bool isRIDPresent(void *leafPage, short position, short positionEnd, const RID &rid, const Attribute &attribute);

    RC insert(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid, int keySize, int pageId, void *pageData, bool &split, void *insertToParent);

    RC insertAndSplitLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid, int pageId, void *pageData, void *pushUpKey);

    int getKeySize(const void *key, const Attribute &attribute);

    int getIntermediateKeySize(const void *key, const Attribute &attribute);

    int getLeafMidPoint(const Attribute &attribute, void *temp, int tempPageEnd, short &oldPageKeyCount, short &newPageKeyCount, short &oldPageFreeSize, short &newPageFreeSize);

    RC insertAndSplitIntermediate(IXFileHandle &ixfileHandle, const Attribute &attribute, void *overFlowKey, int pageId, void * pageData, void *pushUpKey);


    void createNewRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, void *rootPage, void *parentKey);
};


class IX_ScanIterator {
	private:
		unsigned int currentRIDPosition;
	    IXFileHandle *ixFileHandle;
	    Attribute attribute;
    public:
		vector<RID> ridVector;
		vector<void *> keyVector;
		vector<int> intVector;
		vector<int> floatVector;
		vector<string> vcVector;
		// Constructor
        IX_ScanIterator();

        // Destructor
        ~IX_ScanIterator();

        // Get next matching entry
        RC getNextEntry(RID &rid, void *key);

        // Terminate index scan
        RC close();

        void initialize(IXFileHandle &ixfileHandle, const Attribute &attribute);
        void addRIDToVector(RID rid);
        void addKeyValueToVector(void *key);
        void addKeyValueToVector(string key);
        void addIntValueToVector(int key);
        void addFloatValueToVector(float key);
};



class IXFileHandle {
    public:

    // variables to keep counter for each operation
    unsigned ixReadPageCounter;
    unsigned ixWritePageCounter;
    unsigned ixAppendPageCounter;
    FileHandle fileHandle;

    // Constructor
    IXFileHandle();

    // Destructor
    ~IXFileHandle();

    RC readPage(PageNum pageNum, void *data);                           // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                    // Write a specific page
    RC appendPage(const void *data);                                    // Append a specific page
    unsigned getNumberOfPages();                                        // Get the number of pages in the file
	// Put the current counter values of associated PF FileHandles into variables
	RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);




};

#endif
