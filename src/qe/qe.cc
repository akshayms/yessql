#include <sstream>
#include <stdlib.h>
#include <time.h>
#include "qe.h"
#include <sstream>


/*
 * iterAttribName will be of the format tableName.AttribName
 * this helper function will split the two names in separate strings
 * relName will store the table name
 * attribName will store the attribute name
 */
void splitIteratorAttribName(string iterAttribName, string &relName, string &attribName){
	for (unsigned int i=0; i<iterAttribName.length(); i++)
	{
	    if (iterAttribName[i] == '.')
	    	iterAttribName[i] = ' ';
	}

	vector<string> array;
	stringstream ss(iterAttribName);
	string temp;
	while (ss >> temp){
		array.push_back(temp);
	}
	relName = array[0];
	attribName = array[1];
}

void fillVoidContainer(unsigned int index, bool& recValueNull, short offset, unsigned int length, void* readData, void*& dataContainer, AttrType type) {
	if (RecordBasedFileManager::instance()->isFieldNull(readData, index)) {
		recValueNull = true;
	} else {
		switch (type) {
		case TypeInt:
			dataContainer = malloc(sizeof(int));
			int itemp;
			memcpy(&itemp, (char*) (readData) + offset, sizeof(int));
			memcpy((char*) (dataContainer), (char*) (readData) + offset, sizeof(int));
//			cout<<" "<<itemp;
			break;
		case TypeReal:
			dataContainer = malloc(sizeof(float));
			float ftemp;
			memcpy(&ftemp, (char*) (readData) + offset, sizeof(float));
			memcpy((char*) (dataContainer), (char*) (readData) + offset, sizeof(float));
//			cout<<" "<<ftemp;
			break;
		case TypeVarChar:
			memcpy(&length, (char*) (readData) + offset, sizeof(int));
//			cout<<" length "<<length;
			dataContainer = malloc(length + sizeof(int));
			memcpy((char*) (dataContainer), (char*) (readData) + offset, sizeof(int) + length);
			break;
		}
	}
}

bool foundValidNextTuple(int compareValue, CompOp op) {
	switch (op) {
	case EQ_OP:
		if (compareValue == 0) {
			return true;
		}
		break;
	case LT_OP:
		if (compareValue < 0) {
			return true;
		}
		break;
	case LE_OP:
		if (compareValue <= 0) {
			return true;
		}
		break;
	case GT_OP:
		if (compareValue > 0) {
			return true;
		}
		break;
	case GE_OP:
		if (compareValue >= 0) {
			return true;
		}
		break;
	case NE_OP:
		if (compareValue != 0) {
			return true;
		}
		break;
	case NO_OP:
		return true;
		break;
	}
	return false;
}

void concatenateEntry(vector<Attribute> leftInAttributes, vector<Attribute> rightInAttributes, void* leftData, void* rightData, void* joinData) {
	short joinOffset, leftOffset, rightOffset, joinAttribIndex = 0;
	int leftNullSize = RecordBasedFileManager::instance()->getNullBytesLength(leftInAttributes.size());
	int rightNullSize = RecordBasedFileManager::instance()->getNullBytesLength(rightInAttributes.size());
	int joinNullSize = RecordBasedFileManager::instance()->getNullBytesLength(leftInAttributes.size() + rightInAttributes.size());
	int length = 0;
//	cout << endl << "leftNullSize " << leftNullSize << " rightNullSize "<< rightNullSize << " joinNullSize " << joinNullSize;
	//loop thru left record
	leftOffset = leftNullSize;
	joinOffset = joinNullSize;
	for (unsigned i = 0; i < leftInAttributes.size(); i++) {
//		cout << " " << leftInAttributes[i].name << " ";
		if (!RecordBasedFileManager::instance()->isFieldNull(leftData, i)) {
			switch (leftInAttributes[i].type) {
			case TypeInt:
				int itemp;
				memcpy(&itemp, (char*) (leftData) + leftOffset, sizeof(int));
//				cout << itemp<<endl;
				memcpy((char*) (joinData) + joinOffset, &itemp, sizeof(int));
				leftOffset += sizeof(int);
				joinOffset += sizeof(int);
				break;
			case TypeReal:
				float ftemp;
				memcpy(&ftemp, (char*) (leftData) + leftOffset, sizeof(float));
//				cout << ftemp;
				memcpy((char*) (joinData) + joinOffset, &ftemp, sizeof(float));
				leftOffset += sizeof(float);
				joinOffset += sizeof(float);
				break;
			case TypeVarChar:
				memcpy(&length, (char*) (leftData) + leftOffset, sizeof(int));
				memcpy((char*) (joinData) + joinOffset, &length, sizeof(int));
				char* ctemp = (char*) (malloc(length));
				leftOffset += sizeof(int);
				joinOffset += sizeof(int);
				memcpy(ctemp, (char*) (leftData) + leftOffset, length);
//				for (int j = 0; j < length; j++) {
//					cout << ctemp[j];
//				}
				memcpy((char*) (joinData) + joinOffset, ctemp, length);
				free(ctemp);
				leftOffset += length;
				joinOffset += length;
				break;
			}
		} else {
			RecordBasedFileManager::instance()->updateNullBytes(joinData,joinAttribIndex);
		}
		joinAttribIndex++;
	}
//	cout << " left offset " << leftOffset;
//	cout << " join offset " << joinOffset;
	//loop thru right record
	rightOffset = rightNullSize;
	for (unsigned i = 0; i < rightInAttributes.size(); i++) {
//		cout << " " << rightInAttributes[i].name << " ";
		if (!RecordBasedFileManager::instance()->isFieldNull(rightData, i)) {
			switch (rightInAttributes[i].type) {
			case TypeInt:
				int itemp;
				memcpy(&itemp, (char*) (rightData) + rightOffset, sizeof(int));
//				cout << itemp<<endl;;
				memcpy((char*) (joinData) + joinOffset, &itemp, sizeof(int));
				rightOffset += sizeof(int);
				joinOffset += sizeof(int);
				break;
			case TypeReal:
				float ftemp;
				memcpy(&ftemp, (char*) (rightData) + rightOffset,sizeof(float));
//				cout << ftemp;
				memcpy((char*) (joinData) + joinOffset, &ftemp, sizeof(float));
				rightOffset += sizeof(float);
				joinOffset += sizeof(float);
				break;
			case TypeVarChar:
				memcpy(&length, (char*) (rightData) + rightOffset, sizeof(int));
				memcpy((char*) (joinData) + joinOffset, &length, sizeof(int));
				char* ctemp = (char*) (malloc(length));
				rightOffset += sizeof(int);
				joinOffset += sizeof(int);
				memcpy(ctemp, (char*) (rightData) + rightOffset, length);
//				for (int j = 0; j < length; j++) {
//					cout << ctemp[j];
//				}
				memcpy((char*) (joinData) + joinOffset, ctemp, length);
				free(ctemp);
				rightOffset += length;
				joinOffset += length;
				break;
			}
		} else {
			RecordBasedFileManager::instance()->updateNullBytes(joinData, joinAttribIndex);
		}
		joinAttribIndex++;
	}
//	cout << " right offset " << rightOffset;
//	cout << " join offset " << joinOffset << endl;
}

Filter::Filter(Iterator* input, const Condition &condition) {
	input->getAttributes(iteratorAttrs);
	this->condition = condition;
	this->input = input;
}

Filter::~Filter(){

};

RC Filter::getNextTuple(void *data) {
	void *readData = malloc(BUFFER_SIZE), *rhsRecordValue;
	AttrType lhsType,rhsType;
	memset(readData,0,BUFFER_SIZE);
	bool isFoundNextValidTuple = false;
	bool rhsFound = false, isRHSRequired;
	isRHSRequired = condition.bRhsIsAttr;
	if(!isRHSRequired){
		// if the RHS to be compared with is a constant value, we copy it to a container, which would otherwise be used to copy the actual
		// RHS attribute value into. This reduces the complexity of the following loop
		int rhsValueSize;
		switch(condition.rhsValue.type){
		case TypeInt:
		case TypeReal:
			rhsValueSize = sizeof(int);
			break;
		case TypeVarChar:
			int rhsLength=0;
			memcpy(&rhsLength, (char*)condition.rhsValue.data, sizeof(int));
			rhsValueSize = sizeof(int)+rhsLength;
			break;

		}
		rhsRecordValue = malloc(rhsValueSize);
		memset(rhsRecordValue,0,rhsValueSize);
		memcpy((char*)rhsRecordValue, (char*)condition.rhsValue.data, rhsValueSize);
		rhsFound = true;
		condition.rhsAttr="";
	}

	while(!isFoundNextValidTuple){

		if(input->getNextTuple(readData) == QE_EOF){
			return QE_EOF;
		}

		string lhsAttrName = condition.lhsAttr;

	    unsigned short nullSize = RecordBasedFileManager::instance()->getNullBytesLength(iteratorAttrs.size());
	    bool lhsFound = false;
	    bool recValueNull = false;
	    void *lhsRecordValue;

		unsigned int index = 0,length;
		short offset=nullSize;
		for (index=0; index<iteratorAttrs.size(); index++) {
			if (lhsAttrName.compare(iteratorAttrs[index].name) == 0) {
				// found LHS attribute, fill LHS container
				lhsFound = true;
				lhsType = iteratorAttrs[index].type;
//				cout<<" found lhs "<<iteratorAttrs[index].name;
				fillVoidContainer(index, recValueNull, offset, length, readData, lhsRecordValue, iteratorAttrs[index].type);
			} else if(isRHSRequired){
				if(condition.rhsAttr.compare(iteratorAttrs[index].name) == 0){
					// RHS is an attribute, found RHS attribute, fill RHS container
					rhsFound = true;
					rhsType = iteratorAttrs[index].type;
//					cout<<" found rhs "<<iteratorAttrs[index].name;
					fillVoidContainer(index, recValueNull, offset, length, readData, rhsRecordValue, iteratorAttrs[index].type);
				}
			} else {
				if (!RecordBasedFileManager::instance()->isFieldNull(readData, index)){
					switch(iteratorAttrs[index].type){
					case TypeInt:
						offset+=sizeof(int);
						break;
					case TypeReal:
						offset+=sizeof(float);
						break;
					case TypeVarChar:
						memcpy(&length, (char*)readData+offset, sizeof(int));
						offset+=length+sizeof(int);
						break;
					}
				}
			}
			if(lhsFound && rhsFound){
				if(isRHSRequired){
					if(lhsType!=rhsType){
//						cout<<"LHS and RHS types dont match"<<endl;
						free(readData);
						return -1;
					}
				}
				break;
			}
		}

		if(!lhsFound || !rhsFound){
//			cout<<"Incorrect attribute "<<endl;
			free(readData);
			return -1;
		}

		int compareValue = IndexManager::instance()->compareValues(iteratorAttrs[index], lhsRecordValue, rhsRecordValue);
		if(!recValueNull){
			isFoundNextValidTuple = foundValidNextTuple(compareValue, condition.op);
//			if(isFoundNextValidTuple){
//				cout<<" returning"<<endl;
//			} else {
//				cout<<" ignoring"<<endl;
//			}

			free(lhsRecordValue);
			if(isRHSRequired){
				free(rhsRecordValue);
			}
		}
	}

	if(!isRHSRequired){
		free(rhsRecordValue);
	}
	memcpy(data,readData, BUFFER_SIZE);
	free(readData);
	return 0;
}

// For attribute in vector<Attribute>, name it as rel.attr
void Filter::getAttributes(vector<Attribute> &attrs) const{
	attrs = iteratorAttrs;
}


Project::Project(Iterator *input, const vector<string> &attrNames){
	this->input = input;
	invalidAttributes=false;
	this->projectedAttributeNames = attrNames;
	input->getAttributes(this->completeRecordAttributes);
	initializeProjectedAttributes();
}

Project::~Project(){

}

RC Project::getNextTuple(void *data){

	void *readData = malloc(BUFFER_SIZE);
	memset(readData,0,BUFFER_SIZE);
	memset(data,0,BUFFER_SIZE);
	map<string,AttributeAndOffset> projAttributesMap;

	if(invalidAttributes){
		free(readData);
		return QE_EOF;
	}
	if(input->getNextTuple(readData) == QE_EOF){
		free(readData);
		return QE_EOF;
	}
	unsigned short nullSize = RecordBasedFileManager::instance()->getNullBytesLength(completeRecordAttributes.size());
	unsigned int index = 0, projIndex=0, length;
	short offset = nullSize, currentOffset;
	for (index=0; index<completeRecordAttributes.size(); index++) {
		currentOffset = offset;
		if (RecordBasedFileManager::instance()->isFieldNull(readData, index)){
			// to identify whether or not a variable is null in the 2nd run, we set this offset to -1
			// this way, we can set corresponding bit in the return data and skip unnecessary copying of data
			currentOffset = -1;
		} else {
			if (!RecordBasedFileManager::instance()->isFieldNull(readData, index)){
				switch(completeRecordAttributes[index].type){
				case TypeInt:
					offset+=sizeof(int);
					break;
				case TypeReal:
					offset+=sizeof(float);
					break;
				case TypeVarChar:
					memcpy(&length, (char*)readData+offset, sizeof(int));
					offset+=length+sizeof(int);
					break;
				}
			}
		}
		AttributeAndOffset temp = {completeRecordAttributes[index], currentOffset};
		projAttributesMap.insert(std::pair<string,AttributeAndOffset>(completeRecordAttributes[index].name, temp));
	}


	unsigned short projNullSize = RecordBasedFileManager::instance()->getNullBytesLength(projectedAttributeNames.size());
	short projOffset = projNullSize;
	for (projIndex=0; projIndex<projectedAttributeNames.size(); projIndex++){
		auto search=projAttributesMap.find(projectedAttributeNames[projIndex]);
		Attribute attr = search->second.attr;
		short startOff = search->second.startOffset;
		if(startOff == -1){
			RecordBasedFileManager::instance()->updateNullBytes(data,projIndex);
		} else {
			switch(attr.type){
			case TypeInt:
			case TypeReal:
				memcpy((char*)data+projOffset, (char*)readData+startOff, sizeof(int));
				projOffset+=sizeof(int);
				break;
			case TypeVarChar:
				memcpy(&length, (char*)readData+startOff, sizeof(int));
				memcpy((char*)data+projOffset, (char*)readData+startOff, length+sizeof(int));
				projOffset+=length+sizeof(int);
				break;
			}
		}
	}

	free(readData);
	return 0;
}

void Project::initializeProjectedAttributes(){

	bool foundAttrib;
	for(unsigned int i=0;i<projectedAttributeNames.size();i++){
		foundAttrib = false;
		for(unsigned int j=0;j<completeRecordAttributes.size();j++){
			if(projectedAttributeNames[i].compare(completeRecordAttributes[j].name) == 0){
				foundAttrib=true;
				Attribute tempAttr;
				tempAttr.name=completeRecordAttributes[j].name;
				tempAttr.type=completeRecordAttributes[j].type;
				tempAttr.length=completeRecordAttributes[j].length;
				projectedAttributes.push_back(tempAttr);
			}
		}
		if(!foundAttrib){
			invalidAttributes = true;
		}
	}
}

// For attribute in vector<Attribute>, name it as rel.attr
void Project::getAttributes(vector<Attribute> &attrs) const{
	attrs = projectedAttributes;
}

Aggregate::Aggregate(Iterator *input, Attribute aggAttr, AggregateOp op){
	this->input = input;
	this->aggAttr = aggAttr;
	this->op = op;
	input->getAttributes(this->inputAttributes);
	opPerformed = false;
}


Aggregate::~Aggregate(){

}

RC Aggregate::getNextTuple(void *data){
	//Aggregrations only have one tuple. once we send an aggregation output, set the opPerformed switch to true
	if(aggAttr.type == TypeVarChar){
		return QE_EOF;
	}
	if(opPerformed){
		return QE_EOF;
	}

	unsigned short nullSize = RecordBasedFileManager::instance()->getNullBytesLength(inputAttributes.size());
	bool firstRec = true, validAttribute=false;
	float avg, sum=0, min, max, count=0.0, fTemp;
	int iMin, iMax, iSum=0, length=0, iTemp;
	unsigned int index=0;
	short offset=nullSize;
	void *readData = malloc(BUFFER_SIZE);
	memset(readData,0,BUFFER_SIZE);
	memset(data, 0, sizeof(int)+1);

	while(input->getNextTuple(readData)!=QE_EOF){

		offset=nullSize;
		for (index=0; index<inputAttributes.size(); index++) {
			if(inputAttributes[index].name.compare(aggAttr.name) == 0){
				if(inputAttributes[index].type!=aggAttr.type){
					free(readData);
					return -1;
				}
				validAttribute=true;
				if (!RecordBasedFileManager::instance()->isFieldNull(readData, index)){
					switch(inputAttributes[index].type){
					case TypeInt:
						memcpy(&iTemp, (char*)readData+offset, sizeof(int));
						iSum+=iTemp;
						if(iTemp<iMin || firstRec){
							iMin = iTemp;
						}
						if(iTemp>iMax || firstRec){
							iMax = iTemp;
						}
//						cout<<" itemp "<<iTemp<<" isum "<<iSum<<" imax "<<iMax<<" imin "<<iMin<<endl;
						break;
					case TypeReal:
						memcpy(&fTemp, (char*)readData+offset, sizeof(float));
						sum+=fTemp;
						if(fTemp<min || firstRec){
							min = fTemp;
						}
						if(fTemp>max || firstRec){
							max = fTemp;
						}
//						cout<<"ftemp "<<fTemp<<"sum "<<sum<<" max "<<max<<" min "<<min<<endl;
						break;
					case TypeVarChar:
						free(readData);
						return -1;
					}
					count++;
					firstRec = false;
				}
				break;
			} else {
				if (!RecordBasedFileManager::instance()->isFieldNull(readData, index)){
					switch(inputAttributes[index].type){
					case TypeInt:
					case TypeReal:
						offset+=sizeof(int);
						break;
					case TypeVarChar:
						memcpy(&length, (char*)readData+offset, sizeof(int));
						offset+=length+sizeof(int);
						break;
					}
				}
			}
		}
	}

//	cout<<"Count: "<<count<<endl;
	if(!validAttribute){
		free(readData);
		return -1;
	}

	if(aggAttr.type == TypeInt){
		min = (float)iMin;
		max = (float)iMax;
		sum = (float)iSum;
	}

	switch(op){
	case MIN:
//		cout<<"returning min :"<<min<<endl;
		memcpy((char*)data+1, &min, sizeof(float));
		break;
	case MAX:
//		cout<<"returning max :"<<max<<endl;
		memcpy((char*)data+1, &max, sizeof(float));
		break;
	case COUNT:
//		cout<<"returning count :"<<count<<endl;
		memcpy((char*)data+1, &count, sizeof(float));
		break;
	case SUM:
//		cout<<"returning sum :"<<sum<<endl;
		memcpy((char*)data+1, &sum, sizeof(float));
		break;
	case AVG:
		avg = sum/count;
//		cout<<"returning avg :"<<avg<<endl;
		memcpy((char*)data+1, &avg, sizeof(float));
		break;
	}

	free(readData);
	opPerformed = true;
	return 0;
}
// Please name the output attribute as aggregateOp(aggAttr)
// E.g. Relation=rel, attribute=attr, aggregateOp=MAX
// output attrname = "MAX(rel.attr)"
void Aggregate::getAttributes(vector<Attribute> &attrs) const{
	Attribute temp;
	temp.length = aggAttr.length;
	temp.type = aggAttr.type;
	string tempName;
	switch(op){
	case MIN: tempName = "MIN("+aggAttr.name+")";
		break;
	case MAX: tempName = "MAX("+aggAttr.name+")";
		break;
	case COUNT:tempName = "COUNT("+aggAttr.name+")";
		break;
	case SUM:tempName = "SUM("+aggAttr.name+")";
		break;
	case AVG:tempName = "AVG("+aggAttr.name+")";
		break;
	}
	temp.name = tempName;
	attrs.push_back(temp);
}


RC BNLJoin::getNextTuple(void *data) {
	RID rid;
	return outputIter.getNextRecord(rid, data);
}

void BNLJoin::getAttributes(vector<Attribute> &attrs) const {
	attrs.clear();
	attrs.reserve(leftTableAttribs.size() + rightTableAttribs.size());
	attrs.insert(attrs.end(), leftTableAttribs.begin(), leftTableAttribs.end());
	attrs.insert(attrs.end(), rightTableAttribs.begin(), rightTableAttribs.end());
}

INLJoin::INLJoin(Iterator *leftIn, IndexScan *rightIn, const Condition &condition ){
	this->leftIn = leftIn;
	this->rightIn = rightIn;
	leftIn->getAttributes(this->leftInAttributes);
	rightIn->getAttributes(this->rightInAttributes);
	this->condition = condition;
	initializeCombinedNames();
	initialize();
}

INLJoin::~INLJoin(){
//	cout<<"closing iterator "<<endl;
	rbfmIter.close();
//	cout<<"destroying file "<<joinFileName<<endl;
	RecordBasedFileManager::instance()->closeFile(fileHandle);
	RecordBasedFileManager::instance()->destroyFile(joinFileName);
}

void INLJoin::initializeCombinedNames(){
	for(unsigned i=0; i<leftInAttributes.size(); i++){
		combinedNames.push_back(leftInAttributes[i].name);
	}
	for(unsigned i=0; i<rightInAttributes.size(); i++){
		combinedNames.push_back(rightInAttributes[i].name);
	}
}

void INLJoin::initialize(){
	vector<Attribute> attrs;
	getAttributes(attrs);
	createAndOpenJoinFile();
	populateJoinFile();
//	cout<<"Join file created, start scan now "<<endl;
	RecordBasedFileManager::instance()->scan(fileHandle, attrs, "", NO_OP, NULL, combinedNames , rbfmIter);
//	cout<<"Scanned entries INLJ "<<rbfmIter.ridVector.size()<<endl;

}

void INLJoin::createAndOpenJoinFile(){
	string leftTable, leftAttr, rightTable, rightAttr;
	splitIteratorAttribName(condition.lhsAttr, leftTable, leftAttr);
	splitIteratorAttribName(condition.rhsAttr, rightTable, rightAttr);

	time_t t = time(0);
	std::stringstream ss;
	ss << t;

	joinFileName = "INLJ"+leftTable+leftAttr+"_"+rightTable+rightAttr+"_"+ss.str();
//	cout<<"Created join file "<<joinFileName<<endl;
	RecordBasedFileManager::instance()->createFile(joinFileName);
	RecordBasedFileManager::instance()->openFile(joinFileName, fileHandle);
}

void INLJoin::populateJoinFile(){

	void *leftData, *rightData;
	leftData = malloc(BUFFER_SIZE);
	memset(leftData,0,BUFFER_SIZE);
	rightData = malloc(BUFFER_SIZE);
	memset(rightData,0,BUFFER_SIZE);
	unsigned int i=0, j=0;

	int leftNullSize = RecordBasedFileManager::instance()->getNullBytesLength(leftInAttributes.size());
	int rightNullSize = RecordBasedFileManager::instance()->getNullBytesLength(rightInAttributes.size());
	int length;
	bool lhsFound = false, rhsFound = false, leftRecValueNull = false, rightRecValueNull = false;
	void *lhsRecordValue, *rhsRecordValue;

	short leftOffset = leftNullSize;
	short rightOffset = rightNullSize;

	while(leftIn->getNextTuple(leftData)!=QE_EOF){
		lhsFound = false;
		leftOffset = leftNullSize;
		leftRecValueNull = false;
		for (i=0; i<leftInAttributes.size(); i++) {
			if (condition.lhsAttr.compare(leftInAttributes[i].name) == 0) {
//				cout<<"found lhs "<<leftInAttributes[i].name;
				lhsFound=true;
				fillVoidContainer(i, leftRecValueNull, leftOffset, length, leftData, lhsRecordValue, leftInAttributes[i].type);
				break;
			} else {
				if (!RecordBasedFileManager::instance()->isFieldNull(leftData, i)){
					switch(leftInAttributes[i].type){
					case TypeInt:
						leftOffset+=sizeof(int);
						break;
					case TypeReal:
						leftOffset+=sizeof(float);
						break;
					case TypeVarChar:
						memcpy(&length, (char*)leftData+leftOffset, sizeof(int));
						leftOffset+=length+sizeof(int);
						break;
					}
				}
			}
		}
		if(!lhsFound){
//			cout<<"Incorrect lhs attribute "<<endl;
			free(leftData);
			return;
		}
		if(leftRecValueNull == true){
//			cout<<"lhs value null"<<endl;
			memset(leftData,0,BUFFER_SIZE);
			continue;
		}
		rightIn->setIterator(lhsRecordValue, lhsRecordValue, true,true);
		//We have found lhs value, now lets loop through the rightIn and find all matching record
		while(rightIn->getNextTuple(rightData)!=QE_EOF){
			rhsFound = false;
			rightOffset = rightNullSize;
			rightRecValueNull = false;
			for (j=0; j<rightInAttributes.size(); j++) {
				if (condition.rhsAttr.compare(rightInAttributes[j].name) == 0) {
//					cout<<" found rhs "<<rightInAttributes[j].name;
					rhsFound=true;
					fillVoidContainer(j, rightRecValueNull, rightOffset, length, rightData, rhsRecordValue, rightInAttributes[j].type);
					break;
				} else {
					if (!RecordBasedFileManager::instance()->isFieldNull(rightData, j)){
						switch(rightInAttributes[j].type){
						case TypeInt:
							rightOffset+=sizeof(int);
							break;
						case TypeReal:
							rightOffset+=sizeof(float);
							break;
						case TypeVarChar:
							memcpy(&length, (char*)rightData+rightOffset, sizeof(int));
							rightOffset+=length+sizeof(int);
							break;
						}
					}
				}
			}
			if(!rhsFound){
//				cout<<"Incorrect rhs attribute "<<endl;
				free(leftData);
				free(rightData);
				return;
			}
			if(rightRecValueNull == true){
//				cout<<"rhs value null"<<endl;
				memset(rightData,0,BUFFER_SIZE);
				continue;
			}
			int compareValue = IndexManager::instance()->compareValues(leftInAttributes[i], lhsRecordValue, rhsRecordValue);
			bool conditionSatisfied = foundValidNextTuple(compareValue, condition.op);
			if(conditionSatisfied){
				concatenateAndInsertEntryToTempFile(leftData, rightData);
			}
			free(rhsRecordValue);
			memset(rightData,0,BUFFER_SIZE);
		}
		free(lhsRecordValue);
		memset(leftData,0,BUFFER_SIZE);
//		cout<<endl;
	}
	free(leftData);
	free(rightData);
}

RC INLJoin::getNextTuple(void *data){
	RID rid;
	RC rc = rbfmIter.getNextRecord(rid, data);
//	cout<<"Returning rid "<<rid.pageNum<<" "<<rid.slotNum<<endl;
	return rc;
}

void INLJoin::concatenateAndInsertEntryToTempFile(void *leftData, void *rightData){
	void *joinData = malloc(2*BUFFER_SIZE);
	memset(joinData,0,400);

	concatenateEntry(leftInAttributes, rightInAttributes, leftData, rightData, joinData);
	vector<Attribute> joinAttrs;
	getAttributes(joinAttrs);

//	RecordBasedFileManager::instance()->printRecord(joinAttrs, joinData);

	RID newRid;
	RecordBasedFileManager::instance()->insertRecord(fileHandle, joinAttrs, joinData, newRid);
//	cout<<"Inserted new rid "<<newRid.pageNum<<" "<<newRid.slotNum<<endl;
	free(joinData);
}

// For attribute in vector<Attribute>, name it as rel.attr
void INLJoin::getAttributes(vector<Attribute> &attrs) const{
	attrs.clear();
	attrs.reserve( leftInAttributes.size() + rightInAttributes.size() );
	attrs.insert( attrs.end(), leftInAttributes.begin(), leftInAttributes.end() );
	attrs.insert( attrs.end(), rightInAttributes.begin(), rightInAttributes.end() );
};

BNLJoin::BNLJoin(Iterator *leftIn, TableScan *rightIn, const Condition &condition, const unsigned numPages) {
	initializeBuffer(numPages);
	void *data = malloc(PAGE_SIZE);
	memset(data, 0, PAGE_SIZE);
	bool outerEnd = false;
	leftIn->getAttributes(leftTableAttribs);
	getAttributeFromDescriptor(leftTableAttribs, leftKeyAttribute, condition.lhsAttr, leftKeyAttrPosition);
	rightIn->getAttributes(rightTableAttribs);
	getAttributeFromDescriptor(rightTableAttribs, rightKeyAttribute, condition.rhsAttr, rightKeyAttrPosition);
	this->leftIn = leftIn;
	this->rightIn = rightIn;
	this->condition = condition;
	this->numPages = numPages;
	this->getAttributes(combinedAttributes);
	time_t t = time(NULL);
	std::stringstream ss;
	ss << t;
	tempFileName = ss.str();
	rbfManager = RecordBasedFileManager::instance();
	rbfManager->createFile(tempFileName);
	rbfManager->openFile(tempFileName, tempFileHandle);
	rbfManager->prepareNewRecordBasedPage(tempFileHandle, outputPage);

	//LHS/RHS not found. atrributes are of different types.

	int totalRecordsInBuffer = 0;
	while (fillPages(leftIn, condition, totalRecordsInBuffer) == 0) {
		join(totalRecordsInBuffer);
		totalRecordsInBuffer = 0;
	}
	join(totalRecordsInBuffer);
	tempFileHandle.appendPage(outputPage);
	//rbfManager->closeFile(tempFileHandle);
	vector<string> combinedAttribNames;

	for (vector<Attribute>::iterator it = combinedAttributes.begin(); it != combinedAttributes.end(); ++it) {
		combinedAttribNames.push_back(it->name);
	}
	/*RBFM_ScanIterator tempIter;
	rbfManager->scan(tempFileHandle, combinedAttributes, "", NO_OP, NULL, combinedAttribNames, tempIter);

	cout << " --- join results --- " << endl;
	RID rid;
	while(tempIter.getNextRecord(rid, data) != -1) {
		rbfManager->printRecord(combinedAttributes, data);
	}
	tempIter.close();
*/
	rbfManager->scan(tempFileHandle, combinedAttributes, "", NO_OP, NULL, combinedAttribNames, outputIter);


	free(data);



}

RC BNLJoin::join(int numRecords) {
	int size, keyOffset;
	void *data = malloc(PAGE_SIZE);
	memset(data, 0, PAGE_SIZE);
	string rightValue;
	map<string, vector<int>>::iterator buckets;
	while(rightIn->getNextTuple(data) != RM_EOF) {
		if (RecordBasedFileManager::instance()->isFieldNull((char *) data, rightKeyAttrPosition)) {
			continue;
		}
		keyOffset = getAttributeOffset(data, rightTableAttribs, rightKeyAttribute);
		rightValue = extractValue(data, keyOffset);
		buckets = hashTable.find(rightValue);

		if (buckets == hashTable.end()) {
			continue;
		}
		else {
			writeToOutput(buckets->second, data);
		}
	}
	rightIn->setIterator();
	free(data);
	return 0;
}

void BNLJoin::writeToOutput(vector<int> buckets, void *data) {
	void *combinedData = malloc(PAGE_SIZE);
	void *convertedData = malloc(PAGE_SIZE);
	int currentFreeSize, newSize;
	for (vector<int>::iterator it = buckets.begin(); it != buckets.end(); ++it) {
		memset(combinedData, 0, PAGE_SIZE);
		memset(convertedData, 0, PAGE_SIZE);
		concatenateEntry(leftTableAttribs, rightTableAttribs, (char *) buffer + (*it), data, combinedData);
		//rbfManager->printRecord(combinedAttributes, combinedData);
		currentFreeSize = rbfManager->getFreeSpaceSize(outputPage);
		newSize = rbfManager->getSizeOnPage(combinedAttributes, combinedData);
		rbfManager->convertToDiskFormat(combinedAttributes, combinedData, convertedData, false);
		if (currentFreeSize - newSize - 2 > 0) {
			rbfManager->writeToPage(tempFileHandle, outputPage, convertedData, newSize);
		}
		else {
			tempFileHandle.appendPage(outputPage);
			memset(outputPage, 0, PAGE_SIZE);
			rbfManager->prepareNewRecordBasedPage(tempFileHandle, outputPage);
			rbfManager->writeToPage(tempFileHandle, outputPage, convertedData, newSize);
		}
	}
	free(combinedData);
	free(convertedData);
}

string BNLJoin::extractValue(void *data, int attribOffset) {
	string strKey;
	int strLen, iVal;
	float fVal;

	switch(rightKeyAttribute.type) {
		case TypeVarChar:
			memcpy(&strLen, (char *) data + attribOffset, sizeof(int));
			strKey = string(static_cast<const char *> ((char *) data + attribOffset + sizeof(int)), strLen);
			break;
		case TypeInt:
			memcpy(&iVal, (char *) data + attribOffset, sizeof(int));
			strKey = std::to_string(iVal);
			break;
		case TypeReal:
			memcpy(&fVal, (char *) data + attribOffset, sizeof(int));
			strKey = std::to_string(fVal);
			break;
	}
	return strKey;
}

void BNLJoin::getAttributeFromDescriptor(vector<Attribute> &recDescriptor, Attribute &attr, string attrName, int &attrPosition) {
	int position = -1;
	for (vector<Attribute>::iterator it = recDescriptor.begin(); it != recDescriptor.end(); ++it, ++position) {
		if (it->name.compare(attrName) == 0) {
			attr = *it;
			attrPosition = ++position;
			break;
		}
	}
}

RC BNLJoin::initializeBuffer(const unsigned numPages) {
	this->numPages = numPages;
	this->buffer = malloc(PAGE_SIZE * numPages);
	this->overFlowRecord = malloc(MAX_RECORD_SIZE);
	this->overFlowRecordSize = 0;
	this->outputPage = malloc(PAGE_SIZE);

	return 0;
}

RC BNLJoin::destroyBuffer() {
	free(buffer);
	free(outputPage);
	free(overFlowRecord);
	return 0;
}

RC BNLJoin::fillPages(Iterator *leftIn, const Condition &condition, int &totalRecords) {
	hashTable.clear();
	memset(buffer, 0, PAGE_SIZE * numPages);
	int rc;
	void *data = malloc(MAX_RECORD_SIZE);
	memset(data, 0, MAX_RECORD_SIZE);
	int currentSize = 0;
	int maxSize = numPages * PAGE_SIZE;
	int recordSize;
	vector<Attribute> tableAttrs;
	leftIn->getAttributes(tableAttrs);

	if (overFlowRecordSize != 0) {
		memcpy(buffer, overFlowRecord, overFlowRecordSize);
		addToHash(buffer, overFlowRecordSize, condition, 0);
		currentSize = overFlowRecordSize;
		overFlowRecordSize = 0;
		memset(overFlowRecord, 0, MAX_RECORD_SIZE);
		totalRecords++;
	}

	while (currentSize < maxSize) {
		rc = leftIn->getNextTuple(data);
		if (rc != 0) {
			free(data);
			return -1;
		}

		int attribOffset = getAttributeOffset(data, leftTableAttribs, leftKeyAttribute);
		if (attribOffset < 0) {
			continue;
		}
		recordSize = computeSize(data, tableAttrs);
		if (currentSize + recordSize >= maxSize) {
			overFlowRecordSize = recordSize;
			memcpy(overFlowRecord, data, recordSize);
			return 0;
		}
		memcpy((char *) buffer + currentSize, data, recordSize);
		addToHash(data, recordSize, condition, currentSize);
		currentSize += recordSize;
		totalRecords++;
	}
	free(data);
	return 0;
}

void BNLJoin::addToHash(void *data, int recordSize, Condition condition, int offset) {
	Attribute keyType;
	int attribOffset = getAttributeOffset(data, leftTableAttribs, leftKeyAttribute);
	if (RecordBasedFileManager::instance()->isFieldNull(data, leftKeyAttrPosition)) {
		return;
	}
	int strLen, iVal;
	float fVal;
	string keyString, iStr, fStr;
	map<string, vector<int>>::iterator search;

	keyString = extractValue(data, attribOffset);
	search = hashTable.find(keyString);
	if (search == hashTable.end()) {
		vector<int> offsetList;
		offsetList.push_back(offset);
		hashTable[keyString] = offsetList;
	}
	else {
		hashTable[keyString].push_back(offset);
	}
}

int BNLJoin::getAttributeOffset(void *data, vector<Attribute> &recDescriptor, Attribute &attribute) {
	int offset = RecordBasedFileManager::instance()->getNullBytesLength(recDescriptor.size());
	int strLen, recordIndex = 0;
	bool found = false;
	for (vector<Attribute>::iterator it = recDescriptor.begin(); it != recDescriptor.end(); ++it, ++recordIndex) {
		if (RecordBasedFileManager::instance()->isFieldNull((char *) data, recordIndex)) {
			continue;
		}
		if (it->name.compare(attribute.name) == 0) {
			found = true;
			break;
		}
		else {
			if (it->type == TypeVarChar) {
				memcpy(&strLen, data, offset);
				offset += strLen;
			}
			offset += sizeof(int);
		}
	}
	if (found) {
		return offset;
	}
	else {
		return -1;
	}
}


int BNLJoin::computeSize(void *data, vector<Attribute> recordDescriptor) {
	int currentOffset = RecordBasedFileManager::instance()->getNullBytesLength(recordDescriptor.size());
	int strLen;
	for (vector<Attribute>::iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it) {
		if (it->type == TypeVarChar) {
			memcpy(&strLen, (char *) data + currentOffset, sizeof(int));
			currentOffset += strLen;
		}
		currentOffset += sizeof(int);
	}
	return currentOffset;
}

void BNLJoin::addToBufferPage(void *data, int size, int currentOffset, int pageNum) {
	memcpy((char *) buffer + (pageNum * PAGE_SIZE) + currentOffset, data, size);
}
