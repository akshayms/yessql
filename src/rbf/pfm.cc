#include "pfm.h"
#include "utils.h"
#include <fstream>


using namespace std;

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
    if(!_pf_manager)
        _pf_manager = new PagedFileManager();

    return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
}


PagedFileManager::~PagedFileManager()
{
}

RC PagedFileManager::createFile(const string &fileName)
{
    bool exists = utils::isFilePresent(fileName);
    if (exists) {
        //cerr << "File " + fileName + " already exists!";
        return -1;
    }
    else {
        ofstream pagedFile(fileName.c_str());
        if (pagedFile.fail()) {
            cerr << "Failed to create the file!";
            return -1;
        }
        // TODO: pagedFile.close();
    }
    return 0;
}


RC PagedFileManager::destroyFile(const string &fileName)
{
    bool exists = utils::isFilePresent(fileName);
    if (!exists) {
        //cerr << "File " + fileName + "does not exist!";
        return -1;
    }
    else {
        if (remove (fileName.c_str()) != 0) {
            cerr << " Error deleting file! ";
            return -1;
        }
    }
    return 0;
}


RC PagedFileManager::openFile(const string &fileName, FileHandle &fileHandle)
{

    if (!utils::isFilePresent(fileName)) {
        //cerr << "File does not exist!";
        return -1;
    }

    if (fileHandle.fileName.empty()) {
        fileHandle.fileName = fileName;

        fileHandle.fileStream.open(fileName.c_str(), ios::in | ios::out | ios::binary);

        if (fileHandle.fileStream.fail()) {
            //cerr << "Error opening file streams!";
            return -1;
        }

    }
    else {
        //cerr << "The fileHandle is being used for a different file!";
        return -1;
    }
    return 0;
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
    if (fileHandle.fileName.empty()) {
        cerr << "Trying to close a file handle that has no open files!";
        return -1;
    }
    else {
        fileHandle.fileName = string();
        fileHandle.fileStream.flush();
        fileHandle.fileStream.close();
    }
    return 0;
}


FileHandle::FileHandle()
{
	readPageCounter = 0;
	writePageCounter = 0;
	appendPageCounter = 0;
    fileName = string();
}


FileHandle::~FileHandle()
{
    //cout << "Closing strem " << fileName.c_str() << endl;
    this->fileStream.flush();
    this->fileStream.close();
}


RC FileHandle::readPage(PageNum pageNum, void *data)
{
    int size = this->getFileSize();
    if (size < PAGE_SIZE * (pageNum + 1)) {
        cerr << "The requested page does not exist!";
        return -1;
    }
    else {
        readPageCounter++;
        fileStream.seekg(PAGE_SIZE * pageNum);
        fileStream.read((char *) data, PAGE_SIZE);
    }

    return 0;
}


RC FileHandle::writePage(PageNum pageNum, const void *data)
{
    int size = getFileSize();
    if (size < PAGE_SIZE * (pageNum + 1)) {
        cerr << "The requested page does not exist!";
        return -1;
    }
    else {
        writePageCounter++;
        fileStream.seekp(PAGE_SIZE * pageNum);
        fileStream.write((char *) data, PAGE_SIZE);
        fileStream.flush();
    }
    return 0;
}


RC FileHandle::appendPage(const void *data)
{
    int numPages = getNumberOfPages();
    appendPageCounter++;
    fileStream.seekp(numPages * PAGE_SIZE);

    fileStream.write((char *) data, PAGE_SIZE);
    fileStream.flush();

    return 0;
}


unsigned FileHandle::getNumberOfPages()
{
    return (getFileSize() / PAGE_SIZE);
}

RC FileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    readPageCount = readPageCounter;
    writePageCount = writePageCounter;
    appendPageCount = appendPageCounter;
	return 0;
}

int FileHandle::getFileSize() {
    if (this->fileName.empty()) {
        cerr << "File Handle does not contain any open files!";
        return -1;
    }
    // Get the current position to reset later.
    //std::streampos current = fileStream.tellg();
    fileStream.seekg(0, ios::beg);
    std::streampos begin = fileStream.tellg();
    fileStream.seekg(0, ios::end);
    std::streampos end = fileStream.tellg();
    // Reset the position to the original place.
    //fileStream.seekg(current);

    return end - begin;
}
