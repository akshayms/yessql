#include "rbfm.h"
#include <bitset>
#include <stdlib.h>

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

RBFM_ScanIterator::RBFM_ScanIterator(){
	currentRIDPosition = 0;
	cachedPageNum = -1;
	pageData = malloc(PAGE_SIZE);
	rbfmManager = RecordBasedFileManager::instance();
}

void RBFM_ScanIterator::addRIDToVector(RID rid){
	ridVector.push_back(rid);
}

void RBFM_ScanIterator::initialize(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const vector<string> &attributeNames){
	this->fileHandle = &fileHandle;
	this->recordDescriptor = recordDescriptor;
	int j = 0;
	for(unsigned int i = 0; i<attributeNames.size(); i++){
		string attributeName = attributeNames[i];
		int attributePosition = rbfmManager->getAttributePosition(recordDescriptor, attributeName);
		if(attributePosition != -1){
			ProjectedAttribute projectedAttribute = { recordDescriptor[attributePosition].name, recordDescriptor[attributePosition].type,
													  recordDescriptor[attributePosition].length, attributePosition };
			projectedAttributes.push_back(projectedAttribute);
		}
	}
}


RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data) {
	//cout<<endl<<"Current RID Position : "<<currentRIDPosition<<endl;
	if(ridVector.size() == 0){
		return RBFM_EOF;
	}
	if(currentRIDPosition >= ridVector.size()){
		return RBFM_EOF;
	}
	RID currentRID = ridVector[currentRIDPosition];
	//cout<<currentRID.pageNum<<"  "<<currentRID.slotNum<<endl;

	if(cachedPageNum == -1 || cachedPageNum != currentRID.pageNum){
		cachedPageNum = currentRID.pageNum;
		//free(pageData);
		//pageData = malloc(PAGE_SIZE);
        memset(pageData, 0, PAGE_SIZE);
		fileHandle->readPage(currentRID.pageNum, pageData);
	}

	void *completeRecordData;

	int readReturn = rbfmManager->readRecordInDiskFormat(*fileHandle, currentRID, pageData, completeRecordData);
	convertToProjectedRecordFormatInOrder(completeRecordData, data);

	//printConvertedData(data);

	rid = currentRID;
	currentRIDPosition++;

	if(readReturn!=-1){
		free(completeRecordData);
	}
	return 0;
};


RC RBFM_ScanIterator::convertToProjectedRecordFormatInOrder(void *completeRecordData, void *projectedAttribData){

    unsigned short numProjectedNullBytes = rbfmManager->getNullBytesLength(projectedAttributes.size());
    void *nullsIndicator = malloc(numProjectedNullBytes);
    memset(nullsIndicator, 0, numProjectedNullBytes);
    memcpy(projectedAttribData, nullsIndicator, numProjectedNullBytes);
    unsigned short projectedRecordDataOffset = numProjectedNullBytes;
	unsigned short projectedDataAttributeIndex = 0;
	short endByte, startByte;


//	cout<<"projectedRecordDataOffset : "<<projectedRecordDataOffset<<endl;
	for(unsigned int i = 0; i<projectedAttributes.size(); i++){
		int attribPositionInCompleteRecord = projectedAttributes[i].offsetInRecordDescriptor;
//		cout<<"For attribute "<<projectedAttributes[i].name<<" attribPositionInCompleteRecord = "<<attribPositionInCompleteRecord<<endl;

		unsigned short offset = (attribPositionInCompleteRecord + 1) * FIELD_OFFSET_SIZE;
	    memcpy(&endByte, (char *) completeRecordData + offset, sizeof(short));
	    memcpy(&startByte, (char *) completeRecordData + offset - FIELD_OFFSET_SIZE, sizeof(short));
//	    cout<<"Start byte = "<<startByte<<" end byte = "<<endByte<<endl;
	    if(startByte == endByte){
	    	rbfmManager->updateNullBytes(projectedAttribData, projectedDataAttributeIndex);
	    	projectedDataAttributeIndex++;
	    	continue;
	    } else {
	    	int length = endByte - startByte;
//	    	cout<<"Length = "<<length<<endl;
	    	if (projectedAttributes[i].type == TypeInt || projectedAttributes[i].type == TypeReal) {
				if (startByte < 0) {
					startByte *= -1;
				}
				startByte++;
				if(projectedAttributes[i].type == TypeInt){
					int temp;
					memcpy(&temp, (char *)completeRecordData + startByte, projectedAttributes[i].length);
//					cout<<"Int value = "<<temp<<endl;
				} else {
					float temp;
					memcpy(&temp, (char *)completeRecordData + startByte, projectedAttributes[i].length);
//					cout<<"Float value = "<<temp<<endl;
				}
				memcpy((char *) projectedAttribData + projectedRecordDataOffset, (char *)completeRecordData + startByte, projectedAttributes[i].length);
				projectedRecordDataOffset+=projectedAttributes[i].length;
//				cout<<"projectedRecordDataOffset : "<<projectedRecordDataOffset<<endl;
			}
			else {
				if (endByte < 0) {
					endByte *= -1;
				}
				if (startByte < 0) {
					startByte *= -1;
				}
				length = endByte - startByte;
				startByte++;
				memcpy((char *) projectedAttribData + projectedRecordDataOffset, &length, sizeof(int));
				projectedRecordDataOffset+=sizeof(int);

				char *temp = (char *)malloc(length);
				memcpy(temp, (char*)completeRecordData+startByte, length);
				string stemp(temp, length);
//				cout<<"Varchar value "<<stemp<<endl;

				memcpy((char *) projectedAttribData + projectedRecordDataOffset, (char *) completeRecordData + startByte, length);
				projectedRecordDataOffset+=length;
//				cout<<"projectedRecordDataOffset : "<<projectedRecordDataOffset<<endl;
				free(temp);
			}
	    }
	}

    free(nullsIndicator);
    return 0;
}

void RBFM_ScanIterator::printConvertedData(void* data){
	int recordIndex = 0;
	int nullBytes = rbfmManager->getNullBytesLength(projectedAttributes.size());
	int offset = nullBytes;
	int ivalue;
	float fvalue;
	int stringLength;
	for (vector<ProjectedAttribute>::const_iterator it = projectedAttributes.begin(); it != projectedAttributes.end(); ++it, ++recordIndex) {
		if (rbfmManager->isFieldNull(data, recordIndex)) {
			cout << it->name << " : NULL " << "\t";
			continue;
		}
		if (it->type == TypeInt) {
			memcpy(&ivalue, (char *)data+offset, sizeof(int));
			cout << it->name << " : " << ivalue << "\t";
			offset += sizeof(int);
		}
		else if (it->type == TypeReal) {
			memcpy(&fvalue, (char *)data +offset, sizeof(float));
			cout << it->name << " : " << fvalue << "\t";
			offset += sizeof(float);
		}
		else {
			memcpy(&stringLength, (char *)data + offset, sizeof(int));
			offset += sizeof(int);
			void *str = malloc(stringLength);
			memcpy(str, (char *)data + offset, stringLength);
			cout << it->name << " : "; //<< (char *) str  << "\t";
			for (int i  = 0; i < stringLength; i++)
				cout << *((char *) str+i);
			cout << "\t";
			offset += stringLength;
			free(str);
		}
	}
	cout << endl;
}

RC RBFM_ScanIterator::close(){
	currentRIDPosition = 0;
	cachedPageNum = -1;
	free(pageData);
	ridVector.clear();
	return 0;
}

RC RecordBasedFileManager::createFile(const string &fileName) {
    return PagedFileManager::instance()->createFile(fileName);
}

RC RecordBasedFileManager::destroyFile(const string &fileName) {
    return PagedFileManager::instance()->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) {
    return PagedFileManager::instance()->openFile(fileName, fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
    return PagedFileManager::instance()->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid, bool isRedirected) {
    int newSize = getSizeOnPage(recordDescriptor, data);
    void *newData = malloc(newSize);
    convertToDiskFormat(recordDescriptor, data, newData, isRedirected);


    // Check if last page has sufficient storage
    unsigned int lastPage = fileHandle.getNumberOfPages();

    // Check if last page is sufficient data
    void *lastPageData = malloc(PAGE_SIZE);
    int i;
    if (lastPage == 0) {
        prepareNewRecordBasedPage(fileHandle, lastPageData);
        insertToPage(fileHandle, -1, lastPageData, newData, newSize, rid);
    }
    else {
        fileHandle.readPage(lastPage - 1, lastPageData);
        unsigned short int free_size = getFreeSpaceSize(lastPageData);
        if (free_size - SLOT_OFFSET_SIZE - newSize > 0) {
            insertToPage(fileHandle, lastPage - 1, lastPageData, newData, newSize, rid);
        }
        else {
            bool pageFound = false;
            for (i = 0; i < lastPage; i++) {
                fileHandle.readPage(i, lastPageData);
                unsigned short int freeSize = getFreeSpaceSize(lastPageData);
                if (freeSize - SLOT_OFFSET_SIZE  - newSize > 0) {
                    insertToPage(fileHandle, i, lastPageData, newData, newSize,  rid);
                    pageFound = true;
                    break;
                }
            }
            if (!pageFound) {
                prepareNewRecordBasedPage(fileHandle, lastPageData);
                insertToPage(fileHandle, -1, lastPageData, newData, newSize, rid);
            }
        }

    }

    free(lastPageData);
    free(newData);

    return 0;
}

short int RecordBasedFileManager::getFreePointer(void *pageData) {
    int numSlots = getNumberOfSlots(pageData);
	short int freePointer = 0;
    if (numSlots != 0) {
        freePointer = *((short int *) pageData + (PAGE_SIZE / sizeof(unsigned short int) - 2 - numSlots));
    }
	if (freePointer < 0) {
		freePointer *= -1;
	}
    return freePointer;
}

/*
 * Always compare if the endByte is less than 0. If it is less than 0, then  *the record is actually present at a different location and unavailable here.
 *
 */
RC RecordBasedFileManager::getRecordBoundary(FileHandle &fileHandle, const RID &rid, void *pageData, short &startByte,
                                             short &endByte) {
    unsigned short int slotNum = rid.slotNum;
    unsigned short int numSlots = getNumberOfSlots(pageData);
    RID newRid;

    /*if (numSlots == slotNum) {
        endByte = getFreePointer(pageData);
    }*/
    if (slotNum == 1) {
        startByte = 0;
    }
    else {
        memcpy(&startByte, (char *) pageData + PAGE_SIZE - 4 - slotNum * 2 + 2, sizeof(short));
    }
    memcpy( &endByte, (char *) pageData + PAGE_SIZE - 4 - slotNum*2, sizeof(short));
    /*if (endByte < 1) {
        endByte *= -1;
        startByte++;
        // read the page num and the slot num of the new record.
        memcpy(&newRid.pageNum, (char *) pageData + startByte, sizeof(short));
        memcpy(&newRid.slotNum, (char *) pageData + startByte, sizeof(short));
        void *redirectedPage = malloc(PAGE_SIZE);
        fileHandle.readPage(newRid.pageNum, redirectedPage);
        getRecordBoundary(fileHandle, newRid, redirectedPage, startByte, endByte);
        free(redirectedPage);
    }*/
    return 0;
}

RC RecordBasedFileManager::readTombstoneValue(const RID &tombstone, RID &newLocation, void *pageData, short startByte) {
    short pageNum, slotNum;
	startByte < 0 ? startByte *= -1 : 0;
    memcpy(&pageNum, (char *) pageData + startByte, sizeof(short));
    memcpy(&slotNum, (char *) pageData + startByte + 2, sizeof(short));
    newLocation.pageNum = pageNum;
    newLocation.slotNum = slotNum;
    return 0;
}

RC RecordBasedFileManager::insertNewSlotOffset(void *pageData, unsigned short int slotNumber, unsigned short int dataSize) {
    int numSlots = getNumberOfSlots(pageData);
    int prevSlot = getFreePointer(pageData);
	if (prevSlot < 1) {
		prevSlot *= -1;
	}
    int nextFreeLocation = prevSlot + dataSize;
    memcpy((char *)pageData + (PAGE_SIZE - 4 - numSlots*2 - 2), &nextFreeLocation, sizeof(unsigned short int));
    return 0;
}

RC RecordBasedFileManager::insertToPage(FileHandle &fileHandle, int pageNum, void *pageData, void *recordData,
                                        unsigned short int dataSize, RID &rid) {
    int numSlots = getNumberOfSlots(pageData);
    int freePointer = getFreePointer(pageData);
    insertNewSlotOffset(pageData, ++numSlots, dataSize);
    updateNumberOfSlots(numSlots, pageData);

    rid.slotNum = numSlots;

    int currentFreeSize = getFreeSpaceSize(pageData);
//    cout << " P: " << pageNum << " S : " << rid.slotNum <<  " C: " << currentFreeSize;
    currentFreeSize -= dataSize;
    currentFreeSize -= FIELD_OFFSET_SIZE;
    updateFreeSpaceSize(currentFreeSize, pageData);

//    cout << " F: " << currentFreeSize << " Z: " << dataSize+2 << endl;
    memcpy((char *)pageData + freePointer, recordData, dataSize);
    if (pageNum == -1) {
        fileHandle.appendPage(pageData);
        rid.pageNum = fileHandle.getNumberOfPages() - 1;
    } else {
        fileHandle.writePage(pageNum, pageData);
        rid.pageNum = pageNum;
    }

    return 0;
}

int RecordBasedFileManager::writeToPage(FileHandle &fileHandle, void *pageData, void *recordData, unsigned dataSize) {
    int numSlots = getNumberOfSlots(pageData);
    int freePointer = getFreePointer(pageData);
    insertNewSlotOffset(pageData, ++numSlots, dataSize);
    updateNumberOfSlots(numSlots, pageData);

    int currentFreeSize = getFreeSpaceSize(pageData);
//    cout << " P: " << pageNum << " S : " << rid.slotNum <<  " C: " << currentFreeSize;
    currentFreeSize -= dataSize;
    currentFreeSize -= FIELD_OFFSET_SIZE;
    updateFreeSpaceSize(currentFreeSize, pageData);

//    cout << " F: " << currentFreeSize << " Z: " << dataSize+2 << endl;
    memcpy((char *)pageData + freePointer, recordData, dataSize);
    return 0;
}

int RecordBasedFileManager::getNullBytesLength(int numRecords)
{
    return (int)ceil(numRecords / 8.0);
}

RC RecordBasedFileManager::convertToDiskFormat(const vector<Attribute> &recordDescriptor, const void *data, void *transformedData, bool isRedirected) {

    unsigned short nullSize = getNullBytesLength(recordDescriptor.size());
    unsigned short numRecords = recordDescriptor.size();

    /*
     * The first byte in the new data contains the pointer to the end of where
     * the offset pointers are located. This value/2 will give the number of records
     * that are present in the new data model.
     */


    short offsetEnd = ((numRecords+1) * 2) - 1;
    if (isRedirected) {
        offsetEnd *= -1;
    }
    memcpy((char *)transformedData , &offsetEnd, sizeof(short));
    if (isRedirected) {
        offsetEnd  *= -1;
    }


    int recordIndex = 0;
    // offset to where the next data offset will be stored.
    unsigned short nextOffsetInfoPointer = FIELD_OFFSET_SIZE;

    // Pointer to the start of where the data will be written out
    unsigned short newDataOffset = offsetEnd + 1;
    // pointer to the start of the original data block
    unsigned short originalDataPointer = nullSize;
    int stringLength;
    short int temp;

    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        if (isFieldNull(data, recordIndex)) {
            temp = newDataOffset - 1;
            memcpy((char *) transformedData + nextOffsetInfoPointer, &temp, sizeof(unsigned short int));
            nextOffsetInfoPointer += sizeof(unsigned short);
        }
        else if (it->type == TypeInt || it->type == TypeReal) {

            memcpy((char *)transformedData + newDataOffset, (char *)data + originalDataPointer, it->length);
            originalDataPointer += it->length;
            newDataOffset += it->length;
            temp = newDataOffset - 1;
            memcpy((char *) transformedData + nextOffsetInfoPointer, &temp, sizeof(unsigned short int));
            // Write offset to the field slot.
            nextOffsetInfoPointer += sizeof(unsigned short);
        }
        else {
            // Get the length of the string and increment the original data pointer.
            memcpy(&stringLength, (char *)data + originalDataPointer, sizeof(int));
            //cout << "String length: " << stringLength << endl;
            originalDataPointer += sizeof(int);

            // copy the string to the new representation
            memcpy((char *) transformedData + newDataOffset, (char *) data + originalDataPointer, stringLength);
            newDataOffset += stringLength;
            originalDataPointer += stringLength;

            // Copy the new offset value to the offset field.
            temp = newDataOffset - 1;
            if (stringLength == 0) {
                temp *= -1;
            }
            memcpy((char *) transformedData + nextOffsetInfoPointer, &temp, sizeof(unsigned short int));
            nextOffsetInfoPointer += sizeof(unsigned short int);
        }
    }
    return 0;
}

unsigned short RecordBasedFileManager::getOriginalRepresentationSize(const vector<Attribute> &recordDescriptor, const void *data) {
    unsigned short int numFields = recordDescriptor.size();
    unsigned short int nullBytes = getNullBytesLength(numFields);
    unsigned short int newSize = 0;
    newSize += nullBytes;

    unsigned short int recordIndex = 0;
    // TODO: update this to short ints
    unsigned short int begin;
    unsigned short end;
    unsigned short int stringLength;
    // extra int for every string and others will have the size described by the record descriptor itself.
    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        if (it->type == TypeReal || it->type == TypeInt) {
            newSize += it->length;
        }
        else {
            // If it is a string, get the length of the string
            memcpy(&begin, (char *)data + (FIELD_OFFSET_SIZE * recordIndex), sizeof(unsigned short int));
            memcpy(&end, (char *)data + (FIELD_OFFSET_SIZE * recordIndex) + FIELD_OFFSET_SIZE, sizeof(unsigned short int));
            stringLength = (end - begin);
            newSize += sizeof(int);
            newSize += stringLength;
        }
    }
    return newSize;

}

RC RecordBasedFileManager::convertFromDiskFormat(const vector<Attribute> &recordDescriptor, const void *data, void *transformedData) {
    unsigned short int numRecords = recordDescriptor.size();
    unsigned short int numNullBytes = getNullBytesLength(numRecords);

    void *nullsIndicator = malloc(numNullBytes);
    memset(nullsIndicator, 0, numNullBytes);
    unsigned short int newDataPointer = numNullBytes;
    memcpy(transformedData, nullsIndicator, numNullBytes);
    unsigned short int recordIndex = 0;
    // TODO: Change these to short ints
    short int begin;
    short int end;
    int stringLength;
    int ivalue;
    float fvalue;
    bool emptyString = false;
    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        memcpy(&begin, (char *)data + 2*recordIndex, sizeof(unsigned short int));
        memcpy(&end, (char *)data + (2*recordIndex + FIELD_OFFSET_SIZE), sizeof(unsigned short int));
        //begin < 0 ? begin *= -1 : 0;
        //end < 0 ? end *= -1 : 0;
        if (begin == end)/**/
        {
            updateNullBytes(transformedData, recordIndex);
            continue;
        }

        if (it->type == TypeInt || it->type == TypeReal) {
            if (begin < 0) {
                begin *= -1;
            }
            begin++;
            if (it->type == TypeInt) {
                memcpy(&ivalue, (char *)data + begin, sizeof(int));
                // Copy to the new value
//                cout<<ivalue<<"  ";
                memcpy((char*) transformedData + newDataPointer, &ivalue, sizeof(int));
            }
            else {
                memcpy(&fvalue, (char *)data + begin, sizeof(int));
//                cout<<fvalue<<"  ";
                memcpy((char *) transformedData + newDataPointer, &fvalue, sizeof(int));
            }
            newDataPointer+=it->length;
        }
        else {
            //memcpy(&end, (char *)data + (2*recordIndex + FIELD_OFFSET_SIZE), sizeof(unsigned short int));
            // indicates an empty string
            if (end < 0) {
                end *= -1;
            }
            if (begin < 0) {
                begin *= -1;
            }
            stringLength = end - begin;
//            cout<<stringLength<<endl;
            begin++;
            memcpy((char *) transformedData + newDataPointer, &stringLength, sizeof(int));
            newDataPointer += sizeof(int);
            char temp[stringLength];
            memcpy(&temp, (char *) data + begin, stringLength);
//            for(int i=0; i<stringLength; i++){
//            	cout<<temp[i];
//            }
//            cout<<"  ";
            memcpy((char *) transformedData + newDataPointer, (char *) data + begin, stringLength);
            newDataPointer += stringLength;
        }
    }
//    cout<<endl;
    //memcpy((char *)transformedData, nullsIndicator, numNullBytes);
    free(nullsIndicator);

    return 0;
}


int RecordBasedFileManager::getSizeOnPage(const vector<Attribute> &recordDescriptor, const void *data) {

    int currentDataOffset = 0;
    int newSize = 0;
    //newSize += getNullBytesLength(recordDescriptor.size());
    currentDataOffset += getNullBytesLength(recordDescriptor.size()); // Adding the size of the null indicator bytes.

    newSize += NUM_FIELD_OFFSETS_SIZE;
    int varcharLen;
    int recordIndex = 0;
    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        //cout << "CO: " << currentDataOffset << " ND: " << newSize << endl;

        if (isFieldNull(data, recordIndex)) {
            newSize += FIELD_OFFSET_SIZE;
            continue;
        }
        /* If its not null, calculate the size of the new data block.
         * New block will contain 2 bytes for the offset and the it->length
         * bytes for the data. */


        if (it->type == TypeInt || it->type == TypeReal) {
                currentDataOffset += it->length;
                newSize += it->length;
                newSize += FIELD_OFFSET_SIZE;
        }
        else {
            memcpy(&varcharLen, (char *)data + currentDataOffset, sizeof(int));
            currentDataOffset += varcharLen;
            currentDataOffset += sizeof(int);
            newSize += FIELD_OFFSET_SIZE;
            newSize += varcharLen;
        }

    }
    //cout << "CO: " << currentDataOffset << " ND: " << newSize << endl;


    return newSize;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
    void *pageData;
    void *recordDataOnDisk;
    pageData= malloc(PAGE_SIZE);

    int rc = readRecordInDiskFormat(fileHandle, rid, pageData, recordDataOnDisk);
    if (rc == -1) {
        free(pageData);
        return -1;
    }
    convertFromDiskFormat(recordDescriptor, recordDataOnDisk, data);
    free(recordDataOnDisk);
    free(pageData);
    return 0;
}

/* readRecordInDiskFormat returns the following values in corresponding cases
 * 0  - record either has no redirections, or has a redirection out to another RID, ie its the original RID
 * 1  - record has redirection in from another RID
 * -1 - record has been deleted
 */
int RecordBasedFileManager::readRecordInDiskFormat(FileHandle &fileHandle, const RID &rid, void *&pageData,
                                                  void *&recordDataOnDisk) {
    short int startByte, endByte, dataSize;
    fileHandle.readPage(rid.pageNum, pageData);
    // TODO: Change to handle the case where the record data indicates a tombstone
    getRecordBoundary(fileHandle, rid, pageData, startByte, endByte);
    RID realLocation;
    int returnVal = 0;
    bool isRedirected = false;

    if (endByte < 0) {
        isRedirected = true;
        // TODO: Add code to update the actual record location
        readTombstoneValue(rid, realLocation, pageData, startByte);
        if (realLocation.pageNum != rid.pageNum) {
            fileHandle.readPage(realLocation.pageNum, pageData);
        }
        getRecordBoundary(fileHandle, realLocation, pageData, startByte, endByte);
        //cout << "Record redirected to: p,s: " << realLocation.pageNum << " , " << realLocation.slotNum << endl;
    }
    else {
        realLocation.pageNum = rid.pageNum;
        realLocation.slotNum = rid.slotNum;
    }
	if (startByte < 0) {
		startByte *= -1;
	}
	if (endByte < 0) {
		endByte *= -1;
	}
    //cout << "slot " << rid.slotNum << " start: " << startByte << " end: " << endByte << endl;
    dataSize = endByte - startByte;
    if (startByte == endByte) {
       // cerr << "This record has been deleted!" << endl;
        return -1;
    }
    recordDataOnDisk = malloc(dataSize);

    memcpy(recordDataOnDisk, (char *)pageData + startByte, dataSize);

    short firstOffset;
    memcpy(&firstOffset, (char*)recordDataOnDisk, sizeof(short));
    if(firstOffset<0 && !isRedirected){
        //cout << "Value of first offset: " << firstOffset << endl;
    	returnVal = 1;
    }

    return returnVal;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) {
    int recordIndex = 0;
    int nullBytes = getNullBytesLength(recordDescriptor.size());
    int offset = nullBytes;
    int ivalue;
    float fvalue;
    int stringLength;
    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        if (isFieldNull(data, recordIndex)) {
            cout << it->name << " : NULL " << "\t";
            continue;
        }

        if (it->type == TypeInt) {
            memcpy(&ivalue, (char *)data+offset, sizeof(int));
            cout << it->name << " : " << ivalue << "\t";
            offset += sizeof(int);
        }
        else if (it->type == TypeReal) {
            memcpy(&fvalue, (char *)data +offset, sizeof(float));
            cout << it->name << " : " << fvalue << "\t";
            offset += sizeof(float);
        }
        else {
            memcpy(&stringLength, (char *)data + offset, sizeof(int));
            offset += sizeof(int);

            void *str = malloc(stringLength);
            memcpy(str, (char *)data + offset, stringLength);
            cout << it->name << " : "; //<< (char *) str  << "\t";
            for (int i  = 0; i < stringLength; i++)
                cout << *((char *) str+i);
            cout << "\t";

            offset += stringLength;

            free(str);
        }
    }

    cout << endl;

    return 0;
}

RC RecordBasedFileManager::prepareNewRecordBasedPage(FileHandle &fileHandle, void *newPageData) {

    /* 2 bytes allocated at the end of the page for free size in the page
     * 2 bytes allocated to indicate the number of records.
     */


    unsigned short int free_size = NEW_PAGE_FREE_SIZE;
    unsigned short int num_slots = 0;
    *((unsigned short int *) newPageData + (PAGE_SIZE / sizeof(unsigned short int) - FREE_OFFSET)) = free_size;
    *((unsigned short int *) newPageData + (PAGE_SIZE / sizeof(unsigned short int) - SLOT_OFFSET_SIZE)) = num_slots;


    return 0;
}

unsigned short int RecordBasedFileManager::getNumberOfSlots(void *data) {
    unsigned short int num_slots = *((unsigned short int *) data + (PAGE_SIZE / sizeof(unsigned short int) -
                                                                    SLOT_OFFSET_SIZE));
    return num_slots;
}

unsigned short int RecordBasedFileManager::getNumberOfSlots(PageNum pageNum, FileHandle &fileHandle) {
    void *data = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNum, data);

    unsigned short int num_slots = getNumberOfSlots(data);

    free(data);
    return num_slots;
}

RC RecordBasedFileManager::updateNumberOfSlots(unsigned short int slots, void *data) {
    *((unsigned short int *) data + (PAGE_SIZE / sizeof(unsigned short int) - SLOT_OFFSET_SIZE)) = slots;
    return 0;
}

RC RecordBasedFileManager::updateNumberOfSlots(unsigned short int slots, PageNum pageNum, FileHandle &fileHandle) {
    void *data = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNum, data);

    updateNumberOfSlots(slots, data);
    fileHandle.writePage(pageNum, data);

    free(data);
    return 0;
}

unsigned short int RecordBasedFileManager::getFreeSpaceSize(void *data) {
    unsigned short int free_size = *((unsigned short int *) data + (PAGE_SIZE / sizeof(unsigned short int) - FREE_OFFSET));
    return free_size;
}

unsigned short int RecordBasedFileManager::getFreeSpaceSize(PageNum pageNum, FileHandle &fileHandle) {
    void *data = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNum, data);

    unsigned short int free_size = getFreeSpaceSize(data);

    free(data);
    return free_size;
}

RC RecordBasedFileManager::updateFreeSpaceSize(unsigned short int freeSize, void *data) {
    *((unsigned short int *)data+(PAGE_SIZE / sizeof(unsigned short int)) - FREE_OFFSET) = freeSize;
    return 0;
}

RC RecordBasedFileManager::updateFreeSpaceSize(unsigned short int freeSize, PageNum pageNum, FileHandle &fileHandle) {
    void *data = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNum, data);

    updateFreeSpaceSize(freeSize, data);
    fileHandle.writePage(pageNum, data);

    free(data);
    return 0;
}

/*
 * Turn the bit corresponding to recordIndex in data to 1 to indicate that it is a null byte
    */

RC RecordBasedFileManager::updateNullBytes(void *data, unsigned short recordIndex) {
    int byteOffset = (int) floor(recordIndex / 8.0);
    int position = (8 * (byteOffset+1)) - recordIndex;
    char nullByte = *((char *) data + byteOffset);
    nullByte = (nullByte | (1 << (position - 1)));
    memcpy((char *) data + byteOffset, &nullByte, sizeof(char));
    return 0;
}

/*
 * Check if a given filed at position index in the reccord attribute is null.
 *
 * data: the data that is passed from the upper layer
 * index: the index of the attribute that needs to be checked if null.
 *
 * byteOffset = floor ( index / 8 ) -> This gives the nth byte where the null bit
 * will be preset for a field at position index in the record attribute. Example:
 * If 11th field needs to be checked, the byte offset will be 2.
 *
 * The position indicates the bit index which will contain the information.
 * We extract the bit by shifting the byte by (7-position) places so that the bit
 * is LSB. ANDing with 1 will give us 1 if it is null and 0 otherwise.
    */

bool RecordBasedFileManager::isFieldNull(const void *data, int index) {
    int byteOffset = (int) floor(index / 8.0);
    int position = index - (8 * byteOffset);
    char nullByte = *((char *) data + byteOffset);
    return ((nullByte >> (7 - position)) & 1);
}


RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid) {
    void *data = malloc(PAGE_SIZE);
    fileHandle.readPage(rid.pageNum, data);
    RID recordMoveStart, newLocation;
    short slotNum = rid.slotNum;
    short begin, end, size;
    bool tombstone = false;

    // TODO: delete tombstone record
    getRecordBoundary(fileHandle, rid, data, begin, end);
    if (end < -1) {
        tombstone = true;
        readTombstoneValue(rid, newLocation, data, begin);
        deleteRecord(fileHandle, recordDescriptor, newLocation);
    }
    begin < 0 ? begin *= -1 : 0;
    end < 0 ? end *= -1 : 0;
    size = end - begin;

    if (tombstone) {
        size = TOMBSTONE_SIZE;
    }

    recordMoveStart.pageNum = rid.pageNum;
    recordMoveStart.slotNum = rid.slotNum + 1;

    // move records that appear after the current record

    moveRecords(fileHandle, data, size, recordMoveStart);
    // update the slot offset of the deleted record.
    short offset;
    memcpy(&offset, (char *) data + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * rid.slotNum), sizeof(short));
    if (offset < 1) {
        offset *= -1;
        offset -= size;
        offset *= -1;
    }
    else {
        offset -= size;
    }
    memcpy((char *) data + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * rid.slotNum), &offset, sizeof(short));

    // update the free size;
    short freeSpace = getFreeSpaceSize(data);
    freeSpace -= size;
    updateFreeSpaceSize(freeSpace, data);
    fileHandle.writePage(rid.pageNum, data);
    free(data);
    return 0;
}

RC RecordBasedFileManager::moveRecords(FileHandle &fileHandle, void *data, const short &numBytes, RID &fromRecord) {
    short begin;
    short end;
    unsigned short numSlots = getNumberOfSlots(data);
    RID currentRecord;
    currentRecord.pageNum = fromRecord.pageNum;
    currentRecord.slotNum = fromRecord.slotNum;
    short recordSize;
    void *recordData;
    short recordOffset;
    short metaOffset;

    // Move the records
    while(currentRecord.slotNum <= numSlots) // DONE: Verify if < or <=
    {
        getRecordBoundary(fileHandle, currentRecord, data, begin, end);
        if (begin < 0) {
            begin *= -1;
        }
        if (end < 0) {
            end *= -1;
        }
        recordSize = end - begin;
        recordData = malloc(recordSize);
        memcpy(recordData, (char *) data + begin, recordSize);
        memcpy((char *)data + begin - numBytes, recordData, recordSize);
        free(recordData);
        currentRecord.slotNum++;
    }

    // Move the offsets
    currentRecord.slotNum = fromRecord.slotNum;
    while(currentRecord.slotNum <= numSlots) {
        metaOffset = NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * currentRecord.slotNum);
        memcpy(&recordOffset, (char *) data + metaOffset, sizeof(short));
        if (recordOffset < 1) {
            // in case the offset is used to indicate a tombstone at the record
            recordOffset *= -1;
            recordOffset -= numBytes;
            recordOffset *= -1;
        }
        else {
            recordOffset -= numBytes;
        }
        memcpy((char *) data + metaOffset , &recordOffset, sizeof(short));
        currentRecord.slotNum++;
    }
    return 0;
}

RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data) {
    void *pageData = malloc(PAGE_SIZE);
    fileHandle.readPage(rid.pageNum, pageData);
    short startByte, endByte, recordSize, offset;
    int length;

    getRecordBoundary(fileHandle, rid, pageData, startByte, endByte);


    // DONE: Handle tombstone case
	if (endByte < 1) {
		RID redirected;
		readTombstoneValue(rid, redirected, pageData, startByte);
		free(pageData);
		return readAttribute(fileHandle, recordDescriptor, redirected, attributeName, data);
	}
	startByte < 0 ? startByte *= -1 : 0;
	endByte < 0 ? endByte *= -1 : 0;
	recordSize = endByte - startByte;
    if (recordSize == 0) {
        //cerr << "This record does not exist!";
        return -1;
    }

    void *recordData = malloc(recordSize);
    memcpy(recordData, (char *)pageData + startByte, recordSize);

    short attributePosition = getAttributePosition(recordDescriptor, attributeName);
    Attribute attribute = recordDescriptor.at(attributePosition);

    // position of the offset where the end byte will be located
    offset = (attributePosition + 1) * FIELD_OFFSET_SIZE;
    memcpy(&endByte, (char *) recordData + offset, sizeof(short));
    memcpy(&startByte, (char *) recordData + offset - FIELD_OFFSET_SIZE, sizeof(short));

    if (startByte == endByte) {
        // Null byte
        updateNullBytes(data, 0);
        // Since the value of the attribute is null, there is nothing to write back to the pointer.
        return 0;
    }
    else {
        if (attribute.type == TypeInt || attribute.type == TypeReal) {
            if (startByte < 1) {
                startByte *= -1;
            }
            startByte++;
            memcpy((char *) data + 1, (char *)recordData + startByte, attribute.length);
        }
        else {
            if (endByte < -1) {
                endByte *= -1;
            }
            if (startByte < -1) {
                startByte *= -1;
            }
            length = endByte - startByte;
            startByte++;
            memcpy((char *) data + 1, &length, sizeof(int));
            memcpy((char *) data + 5, (char *) recordData + startByte, length);

        }
    }

    free(recordData);
    free(pageData);
    return 0;
}


short RecordBasedFileManager::getAttributePosition(const vector<Attribute> &recordDescriptor, const string &attributeName) {
    int index = 0;
    for (vector<Attribute>::const_iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++index) {
        if (attributeName.compare(it->name) == 0) {
            return index;
        }
    }
    return -1;
}

RC RecordBasedFileManager::scan(FileHandle &fileHandle,
        const vector<Attribute> &recordDescriptor, const string &conditionAttribute, const CompOp compOp,
        const void *value, const vector<string> &attributeNames, RBFM_ScanIterator &rbfm_ScanIterator){

	short int attribPosition = getAttributePosition(recordDescriptor, conditionAttribute);
	unsigned int numOfPages = fileHandle.getNumberOfPages();
	Attribute attribute;


	if(attribPosition == -1 && compOp!=NO_OP){
		return -1;
	}
	if(attribPosition > -1){
		attribute = recordDescriptor.at(attribPosition);
	}

	rbfm_ScanIterator.initialize(fileHandle, recordDescriptor, attributeNames);
	void *pageData = malloc(PAGE_SIZE);
	void *attribData = malloc(PAGE_SIZE);

	for(unsigned int i = 0; i<numOfPages; i++){
		memset(pageData, 0, PAGE_SIZE);
		fileHandle.readPage(i, pageData);
		unsigned short numOfSlotsForPage = getNumberOfSlots(pageData);
		for(unsigned int j=1; j<=numOfSlotsForPage; j++){
			RID currentRID = {i,j};
			if(shouldRecordBeIgnoredForScan(fileHandle, pageData, currentRID)){
				//RIDs should not be considered for two cases:
				// 1) RID belongs to a deleted record: RID will be present in the page slots but should not be considered for compare since there is no data
				// 2) RID is a re-direction from some other RID. Since this RID is not the one visible to the upper layer, we should not make this RID accessible
				//cout << i << j << " IGNORED " << endl;
				continue;
			}
			if(compOp == NO_OP){
                //cout << i << j << " ADDED" << endl;
				rbfm_ScanIterator.addRIDToVector(currentRID);
				continue;
			}
			int totalSize;
			if(attribute.type == TypeVarChar){
				totalSize = attribute.length + 1 + sizeof(int);
			} else {
				totalSize = attribute.length + 1;
			}
			memset(attribData, 0, totalSize);

			//cout<<endl<<"Reading RID "<<currentRID.pageNum<<"  "<<currentRID.slotNum<<endl;
			if (readAttribute(fileHandle, recordDescriptor, currentRID, conditionAttribute, attribData) != 0 ){
				//cout<<"Reading RID "<<currentRID.pageNum<<"  "<<currentRID.slotNum<<" failed"<<endl;
				continue;
			}
			bool isCompareSuccessful = compareAttribValues(conditionAttribute, compOp, value, attribData, attribute.type);
			if(isCompareSuccessful){
				//cout<<"Adding current RID "<<i<<"  "<<j<<endl;
				rbfm_ScanIterator.addRIDToVector(currentRID);
			}
		}
	}
	free(attribData);
	free(pageData);
	return 0;
}

bool RecordBasedFileManager::compareAttribValues(const string &conditionAttribute, const CompOp compOp,
        							const void *referenceValue, void *attribValue, AttrType type ){
	if(isFieldNull(attribValue, 0)){
		return false;
	}

	switch(type){
	case TypeInt:
		int intAttribData, intRefData;
		memcpy(&intAttribData, (char*)attribValue+1, sizeof(int));
		memcpy(&intRefData, (char*)referenceValue, sizeof(int));
//		cout<<"Comparing "<<intAttribData<<" with "<<intRefData<<endl;
		return compare(intAttribData, compOp, intRefData);
	case TypeReal:
		float floatAttribData, floatReferenceData;
		memcpy(&floatAttribData, (char*)attribValue+1, sizeof(float));
		memcpy(&floatReferenceData, (char*)referenceValue, sizeof(float));
//		cout<<"Comparing "<<floatAttribData<<" with "<<floatReferenceData<<endl;
		return compare(floatAttribData, compOp, floatReferenceData);
	case TypeVarChar:

		//convert record attribute value to string - 1 byte nullbyte map, 4 bytes length, remaining actual string
		int attribValueLength;
		memcpy(&attribValueLength, (char*)attribValue+1, sizeof(int));
		void *attribValueString = malloc(attribValueLength);
		memcpy((char*)attribValueString, (char*)attribValue+5, attribValueLength);
		string attribString(static_cast<const char*>(attribValueString), attribValueLength);

		//convert reference value to string - 4 bytes length, remaining actual string
		int referenceLength;
		memcpy(&referenceLength, (char*)referenceValue, sizeof(int));
		void*referenceValueString = malloc(referenceLength);
		memcpy((char*)referenceValueString, (char*)referenceValue+4, referenceLength);
		string referenceString(static_cast<const char*>(referenceValueString), referenceLength);

		bool b = compare(attribString, compOp, referenceString);

		free(attribValueString);
		free(referenceValueString);
		return b;
		break;
	}
	return false;
}

template<typename Type>
bool RecordBasedFileManager::compare(Type recordAttribData, const CompOp compOp, Type value){

	switch(compOp){
	case EQ_OP: return (recordAttribData == value);
	case LT_OP: return (recordAttribData < value);
	case LE_OP: return (recordAttribData <= value);
	case GT_OP: return (recordAttribData > value);
	case GE_OP: return (recordAttribData >= value);
	case NE_OP: return (recordAttribData != value);
	case NO_OP: return true;
	}

	return false;
}

bool RecordBasedFileManager::shouldRecordBeIgnoredForScan(FileHandle &fileHandle, void* pageData, RID &rid){

	void *recordDataOnDisk;
	bool shouldBeIgnored = false;
	int readReturnValue = readRecordInDiskFormat(fileHandle, rid, pageData, recordDataOnDisk);
	if(readReturnValue == -1){
		// readRecordInDiskFormat returns -1 only in case the record has been deleted. If it has been deleted, it should be ignored for the scan
		//cout<<rid.pageNum<<" "<<rid.slotNum<<" has been deleted. it should be ignored for scan"<<endl;
		shouldBeIgnored = true;
	} else if(readReturnValue == 1){
		// readRecordInDiskFormat returns 1 only in case the record has an incoming redirection, it should be ignored for the scan
		//cout<<rid.pageNum<<" "<<rid.slotNum<<" is an incoming redirection. it should be ignored for scan"<<endl;
		shouldBeIgnored = true;
	}

	if(readReturnValue!=-1){
		free(recordDataOnDisk);
	}
	return shouldBeIgnored;
}


RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid) {
    void *pageData = malloc(PAGE_SIZE);
    short begin, end, size, freeSpace, offset, difference, originalPageBegin, originalPageEnd;
    RID realLocation, prevLocation;
    void *realPage;
    bool tombstone = false;

    int newSize = getSizeOnPage(recordDescriptor, data);
    fileHandle.readPage(rid.pageNum, pageData);
    getRecordBoundary(fileHandle, rid, pageData, begin, end);

    // Check if the record is a tombstone. Find the actual page
    // that contains the data and assign the actual page data to realPage.
    if (end < 0) {
        tombstone = true;
        readTombstoneValue(rid, realLocation, pageData, begin);
        if (realLocation.pageNum != rid.pageNum) {
            realPage = malloc(PAGE_SIZE);
            fileHandle.readPage(realLocation.pageNum, realPage);
        }
        else {
            realPage = pageData;
        }
        // Recalculate the record boundary.
        getRecordBoundary(fileHandle, realLocation, pageData, begin, end);
    }
    else {
        realLocation.pageNum = rid.pageNum;
        realLocation.slotNum = rid.slotNum;
        realPage = pageData;
    }
    begin < 0 ? begin *= -1 : 0;
    end < 0 ? end *= -1 : 0;

    // TODO: if tombstone needs to be added to each of the comparisons below.
    size = end - begin;
    void *newData = malloc(newSize);
    RID newLocation, recordMoveStart;

    if (newSize == size)
    {
        // TODO: what if newSize is 4 bytes and current record points to a tombstone?
        convertToDiskFormat(recordDescriptor, data, newData);
        memcpy((char *) realPage + begin, newData, newSize);
        fileHandle.writePage(realLocation.pageNum, realPage);
        //cout << "Same size"<< endl;
    }

    else if (newSize < size) {
        // TODO: this breaks if the updated record size is just 4 bytes, and is a tombstone
        // in addition to this check, we should check if the record is a
        // tombstone
        difference = size - newSize;
        convertToDiskFormat(recordDescriptor, data, newData);
        memcpy((char *) realPage + begin, newData, newSize);

        recordMoveStart.pageNum = realLocation.pageNum;
        recordMoveStart.slotNum = realLocation.slotNum + 1;

        // move records that appear after the current record
        moveRecords(fileHandle, realPage, difference, recordMoveStart);
        // update offsets for the updated record
        memcpy(&offset, (char *) realPage + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * realLocation.slotNum), sizeof(short));
        if (offset < 1) {
            offset *= -1;
            offset -= difference;
            offset *= -1;
        }
        else {
            offset -= difference;
        }
        memcpy((char *) realPage + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * realLocation.slotNum), &offset, sizeof(short));

        // update the free space;
        freeSpace = getFreeSpaceSize(realPage);
        updateFreeSpaceSize(freeSpace + difference, realPage);
        fileHandle.writePage(realLocation.pageNum, realPage);
    }

    else if (newSize > size) {

        //insertRecord(fileHandle, recordDescriptor, data, newLocation);

        // retain only 4 bytes for the tombstone and shift all the other records by the difference.
        // if !tombstone
        /*
         * if it is a tombstone, then the previous page's offsets should change to the new location.
         * further, the current page allocation for the page should be deleted.
         */
        /*
         * DONE: the insert should happen after the page is written with the intermediate changes.
         * this is because insert will read the page with old contents, which will have smaller free space on the current page.
         */
        if (!tombstone) {
            difference = size - TOMBSTONE_SIZE;
            //begin++;
            //memcpy((char *) realPage + begin, &newLocation.pageNum, sizeof(short));
            //memcpy((char *) realPage + begin + sizeof(short), &newLocation.slotNum, sizeof(short));

            recordMoveStart.pageNum = realLocation.pageNum;
            recordMoveStart.slotNum = realLocation.slotNum + 1;
            moveRecords(fileHandle, realPage, difference, recordMoveStart);

            memcpy(&offset, (char *) realPage + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * realLocation.slotNum),
                   sizeof(short));

            if (offset < 1) {
                offset *= -1;
                offset += difference;
            }
            else {
                offset -= difference;
            }
            offset *= -1;
            memcpy((char *) realPage + NEW_PAGE_FREE_SIZE - (SLOT_OFFSET_SIZE * realLocation.slotNum), &offset,
                   sizeof(short));
            // update free space
            freeSpace = getFreeSpaceSize(realPage);
            updateFreeSpaceSize(freeSpace + difference, realPage);
            // persist changes of free space before attempting to insert record.
            fileHandle.writePage(realLocation.pageNum, realPage);

            insertRecord(fileHandle, recordDescriptor, data, newLocation, true);
            //cout << "New location : " << newLocation.pageNum << " " << newLocation.slotNum << endl;
            if (newLocation.pageNum == realLocation.pageNum) {
                // re-read the page because the contents have changed since inserting.
                fileHandle.readPage(realLocation.pageNum, realPage);
            }
            memcpy((char *) realPage + begin, &newLocation.pageNum, sizeof(short));
            memcpy((char *) realPage + begin + sizeof(short), &newLocation.slotNum, sizeof(short));
            // persit changes to the tombstone after insert has been completed either in the same
            // page or a different page.
            fileHandle.writePage(realLocation.pageNum, realPage);

        }
        else {
            // call delete record on the previous page and rid.
            // insert to new location.
            // update the old pointer
            // delete the rid that was present earlier in the old pointer
            insertRecord(fileHandle, recordDescriptor, data, newLocation, true);
            //cout << "New Location: " << newLocation.pageNum << " " << newLocation.slotNum << endl;
            // the previous location of the record is no longer useful, so delete the record
            deleteRecord(fileHandle, recordDescriptor, realLocation);

            getRecordBoundary(fileHandle, rid, pageData, originalPageBegin, originalPageEnd);
            originalPageBegin < 0 ? originalPageBegin *= -1 : 0; // TODO: unseenm untested case
            originalPageEnd < 0 ? originalPageEnd *= -1 : 0;    // TODO: unseen, untested case
            memcpy((char *) pageData + originalPageBegin, &(newLocation.pageNum), sizeof(short));
            memcpy((char *) pageData + originalPageBegin + sizeof(short), &(newLocation.slotNum), sizeof(short));
            fileHandle.writePage(rid.pageNum, pageData);
        }
    }

    if (rid.pageNum != realLocation.pageNum) {
        free(realPage);
    }
    free(pageData);
    free(newData);
    return 0;
}

