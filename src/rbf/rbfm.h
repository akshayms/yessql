#ifndef _rbfm_h_
#define _rbfm_h_

#include <string>
#include <vector>
#include <climits>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>

#include "../rbf/pfm.h"

using namespace std;

// Record ID
typedef struct
{
    unsigned pageNum;	// page number
    unsigned slotNum; // slot number in the page
} RID;


// Attribute
typedef enum { TypeInt = 0, TypeReal, TypeVarChar } AttrType;

typedef unsigned AttrLength;

struct Attribute {
    string   name;     // attribute name
    AttrType type;     // attribute type
    AttrLength length; // attribute length
};

// Comparison Operator (NOT needed for part 1 of the project)
typedef enum { EQ_OP = 0, // no condition// = 
    LT_OP,      // <
    LE_OP,      // <=
    GT_OP,      // >
    GE_OP,      // >=
    NE_OP,      // !=
    NO_OP	   // no condition
} CompOp;

struct ProjectedAttribute {
    string   name;     // attribute name
    AttrType type;     // attribute type
    AttrLength length; // attribute length
    int offsetInRecordDescriptor; // attribute index in original record descriptor
};
/********************************************************************************
The scan iterator is NOT required to be implemented for the part 1 of the project 
********************************************************************************/

# define RBFM_EOF (-1)  // end of a scan operator

# define NEW_PAGE_FREE_SIZE 4092
# define FREE_OFFSET 1
# define SLOT_OFFSET_SIZE 2
# define NUM_FIELD_OFFSETS_SIZE 2
# define FIELD_OFFSET_SIZE 2
# define TOMBSTONE_SIZE 4

// RBFM_ScanIterator is an iterator to go through records
// The way to use it is like the following:
//  RBFM_ScanIterator rbfmScanIterator;
//  rbfm.open(..., rbfmScanIterator);
//  while (rbfmScanIterator(rid, data) != RBFM_EOF) {
//    process the data;
//  }
//  rbfmScanIterator.close();

class RecordBasedFileManager;

class RBFM_ScanIterator {
private:

    vector<ProjectedAttribute> projectedAttributes;
    vector<string> projectedAttributeNames;
    int currentRIDPosition;
    FileHandle *fileHandle;
    vector<Attribute> recordDescriptor;
    RecordBasedFileManager *rbfmManager;
    void* pageData;
    int cachedPageNum;
public:
    RBFM_ScanIterator();
    ~RBFM_ScanIterator(){  }
    vector<RID> ridVector;

    // Never keep the results in the memory. When getNextRecord() is called,
    // a satisfying record needs to be fetched from the file.
    // "data" follows the same format as RecordBasedFileManager::insertRecord().
    RC getNextRecord(RID &rid, void *data);
    RC close();

    void addRIDToVector(RID rid);
    void initialize(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const vector<string> &attributeNames);
    RC convertToProjectedRecordFormat(void *completeRecordData, void *data);
    RC convertToProjectedRecordFormatInOrder(void *completeRecordData, void *data);
    void printConvertedData(void* data);
};


class RecordBasedFileManager
{
public:
    static RecordBasedFileManager* instance();

    RC createFile(const string &fileName);

    RC destroyFile(const string &fileName);

    RC openFile(const string &fileName, FileHandle &fileHandle);

    RC closeFile(FileHandle &fileHandle);

    //  Format of the data passed into the function is the following:
    //  [n byte-null-indicators for y fields] [actual value for the first field] [actual value for the second field] ...
    //  1) For y fields, there is n-byte-null-indicators in the beginning of each record.
    //     The value n can be calculated as: ceil(y / 8). (e.g., 5 fields => ceil(5 / 8) = 1. 12 fields => ceil(12 / 8) = 2.)
    //     Each bit represents whether each field value is null or not.
    //     If k-th bit from the left is set to 1, k-th field value is null. We do not include anything in the actual data part.
    //     If k-th bit from the left is set to 0, k-th field contains non-null values.
    //     If there are more than 8 fields, then you need to find the corresponding byte first,
    //     then find a corresponding bit inside that byte.
    //  2) Actual data is a concatenation of values of the attributes.
    //  3) For Int and Real: use 4 bytes to store the value;
    //     For Varchar: use 4 bytes to store the length of characters, then store the actual characters.
    //  !!! The same format is used for updateRecord(), the returned data of readRecord(), and readAttribute().
    // For example, refer to the Q8 of Project 1 wiki page.
    RC insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid, bool isRedirected = false);

    RC readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data);

    // This method will be mainly used for debugging/test ing.
    // The format is as follows:
    // field1-name: field1-value  field2-name: field2-value ... \n
    // (e.g., age: 24  height: 6.1  salary: 9000
    //        age: NULL  height: 7.5  salary: 7500)
    RC printRecord(const vector<Attribute> &recordDescriptor, const void *data);

/******************************************************************************************************************************************************************
IMPORTANT, PLEASE READ: All methods below this comment (other than the constructor and destructor) are NOT required to be implemented for the part 1 of the project
******************************************************************************************************************************************************************/
    RC deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid);

    // Assume the RID does not change after an update
    RC updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid);

    RC readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data);

    // Scan returns an iterator to allow the caller to go through the results one by one.
    RC scan(FileHandle &fileHandle,
            const vector<Attribute> &recordDescriptor,
            const string &conditionAttribute,
            const CompOp compOp,                  // comparision type such as "<" and "="
            const void *value,                    // used in the comparison
            const vector<string> &attributeNames, // a list of projected attributes
            RBFM_ScanIterator &rbfm_ScanIterator);

    short getAttributePosition(const vector<Attribute> &recordDescriptor, const string &attributeName);

    /**
     *  Get the start and end byte of the record represented by RID.
     */
    RC getRecordBoundary(FileHandle &fileHandle, const RID &rid, void *pageData, short &startByte, short &endByte);

    /**
     * Check if the field at index is null
     */
    bool isFieldNull(const void *data, int index);

    /**
     * Update the null bytes with a 1 at the appropriate bit represented by recordIndex
     */
    RC updateNullBytes(void *data, unsigned short recordIndex);

    /**
     * Get the number of bytes required to represent nulls for "numRecords" records.
     */
    int getNullBytesLength(int numRecords);

    /**
	 * Gets the record pointed to by rid in the disk format
	 */
    int readRecordInDiskFormat(FileHandle &fileHandle, const RID &rid, void *&pageData, void *&recordDataOnDisk);

    /**
     * Update the number of slots in the page pointed to by "data" to "slots".
     */
    RC updateNumberOfSlots(unsigned short int slots, void *data);

    /**
     * Get the current number of slots in the data. Data contains 4096 bytes of a page.
     */
    unsigned short int getNumberOfSlots(void *data);

    /**
     * Update free space size to freeSize in the page data present in "data"
     */
    RC updateFreeSpaceSize(unsigned short int freeSize, void *data);

    /**
     * Gets the free space in the page pointed to by data.
     */
    unsigned short int getFreeSpaceSize(void *data);

    /**
     * Prepare an empty page with free space and number of slots (0) initialized
     */
    RC prepareNewRecordBasedPage(FileHandle &fileHandle, void *newPageData);

    /*
     * Get the size of the transformed record that will be written to the page
     */
    int getSizeOnPage(const vector<Attribute> &recordDescriptor, const void *data);

    /**
     * Transform the data coming from the caller in insert calls to the format that
     * will be used to store on disk.
     */
    RC convertToDiskFormat(const vector<Attribute> &recordDescriptor, const void *data, void *transformedData, bool isRedirected = false);

    int writeToPage(FileHandle &fileHandle, void *pageData, void *recordData, unsigned recordSize);

protected:
    RecordBasedFileManager();
    ~RecordBasedFileManager();

private:
    static RecordBasedFileManager *_rbf_manager;

    /**
     * Get the number of slots in the pageNum of fileHande. This invoke a fileHandle read
     * operation and is expensive. Used mostly for debugging. Use overloaded method
     * with the pageData to avoid multiple accesses to the same page from disk.
     */
    unsigned short int getNumberOfSlots(PageNum pageNum, FileHandle &fileHandle);

    /**
     * Update the number of slots in the page pageNum to numSlots.
     * Warning: This reads a page from disk and writes back after updating.
     */
    RC updateNumberOfSlots(unsigned short int numSlots, PageNum pageNum, FileHandle &fileHandle);

    /**
     * Gets the free space pointed to by pageNum by reading pageNum from disk.
     * This reads from the file and the data is discarded after reading the free
     * space. Call the overloaded method if data will be used for anything after
     * getting the free space.
     */
    unsigned short int getFreeSpaceSize(PageNum pageNum, FileHandle &fileHandle);

    /**
     * Overloaded method in case the data is not available.
     * Warning: Use the other method if pageData will be used after getting the
     * free space. This reads and frees the data that is read from disk.
     */
    RC updateFreeSpaceSize(unsigned short int freeSize, PageNum pageNum, FileHandle &fileHandle);

    /**
     * Convert the record format to the one expected from the caller.
     */
    RC convertFromDiskFormat(const vector<Attribute> &recordDescriptor, const void *data, void *transformedData);

    /**
     * Get the size of record format that the caller passes from disk-format data
     */
    unsigned short getOriginalRepresentationSize(const vector<Attribute> &recordDescriptor, const void *data);

    /**
     * Get the starting byte of the free space in page.
     */
    short int getFreePointer(void *pageData);

    /**
     * Update the offset value stored in slotNumber to freeSize + dataSize
     */
    RC insertNewSlotOffset(void *pageData, unsigned short int slotNumber, unsigned short int dataSize);

    /**
     * Finds the page to append/insert the record to and updates the respective counters in pageData.
     */
    RC insertToPage(FileHandle &fileHandle, int pageNum, void *pageData, void *recordData, unsigned short int dataSize,
                    RID &rid);

    RC moveRecords(FileHandle &fileHandle, void *data, const short &numBytes, RID &fromRecord);



    /**
     * does the actual comparision of attribute value from record to given value and returns true if comparision is successful
     */
    bool compareAttribValues(const string &conditionAttribute, const CompOp compOp, const void *value, void *attribData, AttrType type );
    template<typename Type>
    bool compare(Type recordAttribData, const CompOp compOp, Type value);

    RC readTombstoneValue(const RID &tombstone, RID &newLocation, void *pageData, short startByte);

    bool shouldRecordBeIgnoredForScan(FileHandle &fileHandle, void* pageData, RID &rid);

};

#endif
