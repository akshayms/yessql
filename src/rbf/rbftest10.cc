//#include <fstream>
//#include <iostream>
//#include <string>
//#include <cassert>
//#include <sys/stat.h>
//#include <stdlib.h>
//#include <string.h>
//#include <stdexcept>
//#include <stdio.h>
//
//#include "pfm.h"
//#include "rbfm.h"
//#include "test_util.h"
//
//using namespace std;
//
//int RBFTest_10(RecordBasedFileManager *rbfm) {
//    // Functions tested
//    // 1. Open Record-Based File
//    // 2. Read Multiple Records
//    // 3. Close Record-Based File
//    // 4. Destroy Record-Based File
//    cout << endl << "***** In RBF Test Case 10 *****" << endl;
//
//    RC rc;
//    string fileName = "test9";
//
//    // Open the file "test9"
//    FileHandle fileHandle;
//    rc = rbfm->openFile(fileName, fileHandle);
//    assert(rc == success && "Opening the file should not fail.");
//
//    int numRecords = 20;
//    void *record = malloc(1000);
//    void *returnedData = malloc(1000);
//
//    vector<Attribute> recordDescriptor;
//    createLargeRecordDescriptor(recordDescriptor);
//
//    vector<RID> rids;
//    vector<int> sizes;
//    RID tempRID;
//
//	// Read rids from the disk - do not use this code in your codebase. This is not a PAGE-BASED operation - for the test purpose only.
//	ifstream ridsFileRead("test9rids", ios::in | ios::binary);
//
//	unsigned pageNum;
//	unsigned slotNum;
//
//	if (ridsFileRead.is_open()) {
//		ridsFileRead.seekg(0,ios::beg);
//		for (int i = 0; i < numRecords; i++) {
//			ridsFileRead.read(reinterpret_cast<char*>(&pageNum), sizeof(unsigned));
//			ridsFileRead.read(reinterpret_cast<char*>(&slotNum), sizeof(unsigned));
//			if (i % 1000 == 0) {
//				cout << "loaded RID #" << i << ": " << pageNum << ", " << slotNum << endl;
//			}
//			tempRID.pageNum = pageNum;
//			tempRID.slotNum = slotNum;
//			rids.push_back(tempRID);
//		}
//		ridsFileRead.close();
//	}
//
//	assert(rids.size() == (unsigned) numRecords && "Reading records should not fail.");
//
//	// Read sizes vector from the disk - do not use this code in your codebase. This is not a PAGE-BASED operation - for the test purpose only.
//	ifstream sizesFileRead("test9sizes", ios::in | ios::binary);
//
//	int tempSize;
//
//	if (sizesFileRead.is_open()) {
//		sizesFileRead.seekg(0,ios::beg);
//		for (int i = 0; i < numRecords; i++) {
//			sizesFileRead.read(reinterpret_cast<char*>(&tempSize), sizeof(int));
//			if (i % 1000 == 0) {
//				cout << "loaded Sizes #" << i << ": " << tempSize << endl;
//			}
//			sizes.push_back(tempSize);
//		}
//		sizesFileRead.close();
//	}
//
//	assert(sizes.size() == (unsigned) numRecords && "Reading records should not fail.");
//
//    // NULL field indicator
//    int nullFieldsIndicatorActualSize = getActualByteForNullsIndicator(recordDescriptor.size());
//    unsigned char *nullsIndicator = (unsigned char *) malloc(nullFieldsIndicatorActualSize);
//	memset(nullsIndicator, 0, nullFieldsIndicatorActualSize);
//
//    for(int i = 0; i < numRecords; i++)
//    {
//        memset(record, 0, 1000);
//        memset(returnedData, 0, 1000);
//
//
//        rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], returnedData);
//        assert(rc == success && "Reading a record should not fail.");
//
//        if (i % 1000 == 0) {
//            cout << endl << "Returned Data: for record " << i<< endl;
//            rbfm->printRecord(recordDescriptor, returnedData);
//        }
//
//        int size = 0;
//        prepareLargeRecord(recordDescriptor.size(), nullsIndicator, i, record, &size);
//        if(memcmp(returnedData, record, sizes[i]) != 0)
//        {
//            cout << "[FAIL] Test Case 10 Failed!" << endl << endl;
//            free(record);
//            free(returnedData);
//            return -1;
//        }
//    }
//
//    cout << endl;
//
////    RID recordToDelete = {0,2};
////    rbfm->deleteRecord(fileHandle,recordDescriptor,recordToDelete );
//
//    vector<string> projectedAttribs;
//
//	projectedAttribs.push_back("attr2");
//	projectedAttribs.push_back("attr1");
//	projectedAttribs.push_back("attr0");
//
//    RBFM_ScanIterator scanIter, scanIter2, scanIter3;
//
//
////    int intCompareVal = 5;
////    rc = rbfm->scan(fileHandle,recordDescriptor, "attr1", GT_OP, &intCompareVal, projectedAttribs, scanIter);
////    cout<<"Matched RIDs number = "<<scanIter.ridVector.size()<<endl;
////    for(unsigned int i=0;i<scanIter.ridVector.size();i++){
////    	cout<<scanIter.ridVector[i].pageNum<<"  "<<scanIter.ridVector[i].slotNum<<endl;
////    }
//
//    float floatCompareVal = 5.0f;
//    rc = rbfm->scan(fileHandle,recordDescriptor, "attr2", LE_OP, &floatCompareVal, projectedAttribs, scanIter2);
//    cout<<"Matched RIDs number = "<<scanIter2.ridVector.size()<<endl;
//	for(unsigned int i=0;i<scanIter2.ridVector.size();i++){
//		cout<<scanIter2.ridVector[i].pageNum<<"  "<<scanIter2.ridVector[i].slotNum<<endl;
//	}
//
//
////    char testVarchar[] = {"eee"};
////    int testVarcharSize = 3;
////    void* varcharCompareVal = malloc(sizeof(int)+testVarcharSize);
////    memset(varcharCompareVal ,0 ,sizeof(int)+testVarcharSize);
////    memcpy((char*)varcharCompareVal, &testVarcharSize, sizeof(int));
////    memcpy((char*)varcharCompareVal + sizeof(int), (char*)testVarchar, testVarcharSize);
////    rc = rbfm->scan(fileHandle,recordDescriptor, "attr0", NO_OP, varcharCompareVal, projectedAttribs, scanIter3);
////	cout<<"Matched RIDs number = "<<scanIter3.ridVector.size()<<endl;
////	for(unsigned int i=0;i<scanIter3.ridVector.size();i++){
////		cout<<scanIter3.ridVector[i].pageNum<<"  "<<scanIter3 .ridVector[i].slotNum<<endl;
////	}
////	free(varcharCompareVal);
//
//
//    RID rid;
//    void *projectedData = malloc(1000);
//    memset(projectedData,0,1000);
//    while(scanIter2.getNextRecord(rid, projectedData) == 0){
//    	memset(projectedData,0,1000);
//    	cout<<"processed rid :"<<rid.pageNum<<"  "<<rid.slotNum<<endl<<endl;
//    }
//    free(projectedData);
//
////    scanIter.close();
//    scanIter2.close();
////    scanIter3.close();
//
//    // Close the file "test9"
//    rc = rbfm->closeFile(fileHandle);
//    assert(rc == success && "Closing the file should not fail.");
//
////    rc = rbfm->destroyFile(fileName);
////    assert(rc == success && "Destroying the file should not fail.");
////
////	rc = destroyFileShouldSucceed(fileName);
////    assert(rc == success  && "Destroying the file should not fail.");
//
//	free(record);
//	free(returnedData);
//
//	cout << "RBF Test Case 10 Finished! The result will be examined." << endl << endl;
//
////	remove("test9sizes");
////	remove("test9rids");
//
//	return 0;
//}
//
//int main()
//{
//    // To test the functionality of the record-based file manager
//    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
//
//    RC rcmain = RBFTest_10(rbfm);
//    return rcmain;
//}
