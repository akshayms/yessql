#ifndef _CS222_UTILS_H_
#define _CS222_UTILS_H_
#include <sys/stat.h>
#include <iostream>


/*
namespace utils {
    void debug(const std::string &statement);

    bool isFilePresent(const std::string &fileName);
}*/

//CS222_UTILS_H

// Change to false to prevent non-essential debug statements from getting printed

#define DEBUG_MODE true

namespace utils {

    void debug (const string &statement)
    {
        if (DEBUG_MODE) {
            cout << statement << endl;
        }
    }

    bool isFilePresent(const string &fileName) {

        struct stat fileInfo;

        if (stat(fileName.c_str(), &fileInfo) == 0) {
            return true;
        }
        return false;

    }
}
#endif
