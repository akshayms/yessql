#include <string>
#include <stdlib.h>
#include "rm.h"
#include "../rbf/rbfm.h"
#include <stdlib.h>
#include <map>

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager()
{
    //if (utils::isFilePresent("Tables") && utils::isFilePresent("Columns")) {
        int rc = rbfManager->openFile("Tables", tablesHandle);
        int rc2 = rbfManager->openFile("Columns", colHandle);
    if (rc == 0) {
        currentTableIndex = getNextTableId();
    }
    //}
    else {
        currentTableIndex = 1;
    }
    rbfManager = RecordBasedFileManager::instance();
    ixManager = IndexManager::instance();
}

RelationManager::~RelationManager()
{
    if (tablesHandle.fileName.length() > 0) {
        rbfManager->closeFile(tablesHandle);
    }
    if (colHandle.fileName.length() > 0) {
        rbfManager->closeFile(colHandle);
    }
}

RC RelationManager::createCatalog()
{
    //if (utils::isFilePresent("Tables") && utils::isFilePresent("Columns")) {
      //  return -1;
    //}

    int rc = rbfManager->createFile("Tables");
    if (rc != 0) {
        return -1;
    }
    rc = rbfManager->createFile("Columns");
    if (rc != 0) {
        return -1;
    }
    rbfManager->openFile("Tables", tablesHandle);
    rbfManager->openFile("Columns", colHandle);
    createTablesMetadata();
    createColumnsMetadata();
    return 0;
}

RC RelationManager::deleteCatalog()
{
    rbfManager->closeFile(tablesHandle);
    rbfManager->closeFile(colHandle);
    rbfManager->destroyFile("Tables");
    rbfManager->destroyFile("Columns");
    return 0;
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    string fileName = tableName;
    RID location;
    void *data = malloc(1000);
    memset(data, 0, 1000);
    int rc = rbfManager->createFile(fileName);
    if (rc == -1) {
        free(data);
        cerr << "Table already exists!";
        return -1;
    }
    createTableRecord(currentTableIndex, tableName, fileName, data);
    rbfManager->insertRecord(tablesHandle, getTablesDescriptor(), data, location);
    vector<Attribute> colDescriptor = getColumnsDescriptor();
    int index = 1;
    for (vector<Attribute>::const_iterator it = attrs.begin(); it != attrs.end(); ++it, ++index) {
        memset(data, 0, 1000);
        createColumnRecord(currentTableIndex, it->name, it->type, it->length, index, data);
        rbfManager->insertRecord(colHandle, colDescriptor, data, location);
    }
    currentTableIndex++;
    free(data);
    return 0;
}

RC RelationManager::deleteTable(const string &tableName)
{
    TableDetails table;
    RID rid;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        //cerr << "Table does not exist!";
        return -1;
    }

    int tableId = table.tableId;
    if (tableId == 1 || tableId == 2) {
        return -1;
    }
    vector<Attribute> tableDescriptor = getTablesDescriptor();
    // remove all indices
    std::map<string, string> colIndexMap;
    getColumnIndexMap(colIndexMap, table.tableId);
    for (map<string, string>::iterator colIndexIt = colIndexMap.begin(); colIndexIt != colIndexMap.end(); ++colIndexIt)
    {
        if (colIndexIt->second.length() != 0) {
            remove(colIndexIt->second.c_str());
        }
    }
    rbfManager->deleteRecord(tablesHandle, tableDescriptor, table.rid);
    vector<Attribute> colDescriptor = getColumnsDescriptor();
    vector<string> projectedNames;
    projectedNames.push_back("table-id");
    RBFM_ScanIterator colIter;
    rbfManager->scan(colHandle, colDescriptor, "table-id", EQ_OP, &tableId, projectedNames, colIter);

    for (vector<RID>::iterator it = colIter.ridVector.begin(); it != colIter.ridVector.end(); ++it) {
        rid.pageNum = it->pageNum;
        rid.slotNum = it->slotNum;
        rbfManager->deleteRecord(colHandle, colDescriptor, rid);
    }
    // TODO: What if table name contains invalid OS characters?
    rbfManager->destroyFile(tableName);
    colIter.close();
    return 0;
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    TableDetails table;
    RID rid;
    int size, rc, tableId, nullBytesLength;

    RBFM_ScanIterator colIter;
    rc = findTableRecord(tableName, table);
    if (rc != 0) {
        cerr << "Table does not exist!";
        return -1;
    }

    tableId = table.tableId;
    vector<Attribute> colDescriptor = getColumnsDescriptor();
    vector<string> projected = getColumnTableColumns();
    rbfManager->scan(colHandle, colDescriptor, "table-id", EQ_OP, &tableId, projected, colIter);
    void *data = malloc(PAGE_SIZE);
    memset(data, 0, PAGE_SIZE);
    //rid.pageNum = 0;
    //rid.slotNum = 9;
    //rbfManager->readRecord(colHandle, colDescriptor, rid, data);
    //rbfManager->printRecord(colDescriptor, data);
    nullBytesLength = rbfManager->getNullBytesLength(colDescriptor.size());
    while(colIter.getNextRecord(rid, data) != RBFM_EOF) {

        //rbfManager->printRecord(colDescriptor, data);
        Attribute attr;
        // read name
        memcpy(&size, (char *) data + 4 + nullBytesLength, sizeof(int));
        void *str = malloc(size);
        AttrType type;
        AttrLength length;
        memcpy(str, (char *)data + 8 + nullBytesLength, size);
        memcpy(&type, (char *)data + 8 + size + nullBytesLength, sizeof(int));
        memcpy(&length, (char *)data + 8 + size + 4 + nullBytesLength, sizeof(int));
        attr.name.assign(static_cast<const char *>(str), size);
        attr.type = type;
        attr.length = length;
        attrs.push_back(attr);
        // TODO: copy the position to sort by position later?
        memset(data, 0, PAGE_SIZE);
        free(str);
    }
    free(data);
    colIter.close();
    return 0;
}

vector<string> RelationManager::getColumnTableColumns() {
    vector<string> colTableColumns;
    colTableColumns.push_back("table-id");
    colTableColumns.push_back("column-name");
    colTableColumns.push_back("column-type");
    colTableColumns.push_back("column-length");
    colTableColumns.push_back("column-position");
    colTableColumns.push_back("index-file");
    return colTableColumns;
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    if (table.tableId == 1 || table.tableId == 2) {
        return -1;
    }
    FileHandle fileHandle;
    rbfManager->openFile(table.fileName, fileHandle);
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    rbfManager->insertRecord(fileHandle, recordDescriptor, data, rid);
    rbfManager->closeFile(fileHandle);

    std::map<string, string> colIndexMap;

    getColumnIndexMap(colIndexMap, table.tableId);
    insertToIndex(table, recordDescriptor, colIndexMap, data, rid);

    return 0;
}

RC RelationManager::removeFromIndex(TableDetails &table, vector<Attribute> &recordDescriptor, map<string, string> &colIndexMap, const void *data, const RID &rid) {
    string ixfile;
    IXFileHandle ixfileHandle;
    int offset, fieldSize, recordIndex = 0;
    int nullBytes = rbfManager->getNullBytesLength(recordDescriptor.size());

    offset = nullBytes;
    for (vector<Attribute>::iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        if (rbfManager->isFieldNull(data, recordIndex)) {
            continue;
        }
        ixfile = colIndexMap[it->name];
        fieldSize = 0;
        if (it->type == TypeVarChar) {
            memcpy(&fieldSize, (char *) data + offset, sizeof(int));
        }
        fieldSize += sizeof(int);
        if (ixfile.length() != 0) {
            ixManager->openFile(ixfile, ixfileHandle);
            ixManager->deleteEntry(ixfileHandle, *it, (char *) data + offset, rid);
            ixManager->closeFile(ixfileHandle);
        }
        offset += fieldSize;
    }
    return 0;
}

RC RelationManager::insertToIndex(TableDetails &table, vector<Attribute> &recordDescriptor, map<string, string> &colIndexMap, const void *data, const RID &rid) {
    string ixfile;
    IXFileHandle ixfileHandle;
    int offset, fieldSize, recordIndex = 0;

    int numNullBytes = rbfManager->getNullBytesLength(recordDescriptor.size());

    offset = numNullBytes;

    for (vector<Attribute>::iterator it = recordDescriptor.begin(); it != recordDescriptor.end(); ++it, ++recordIndex) {
        if (rbfManager->isFieldNull(data, recordIndex)) {
            continue;
        }
        // check for null .
    	ixfile = colIndexMap[it->name];
        fieldSize = 0;
        if (it->type == TypeVarChar) {
            memcpy(&fieldSize, (char *) data + offset, sizeof(int));
        }
        fieldSize += sizeof(int);
        if (ixfile.length() != 0) {
            ixManager->openFile(ixfile, ixfileHandle);
            ixManager->insertEntry(ixfileHandle, *it, (char *) data + offset, rid);
            ixManager->closeFile(ixfileHandle);
        }
        offset += fieldSize;
    }
    return 0;
}

void RelationManager::getColumnIndexMap(std::map <string, string> &colIndexMap, int &tableId) {
    //map<const char *,const char *> tempMap;
    void *colData= malloc(PAGE_SIZE);
    vector<string> colNames = getColumnTableColumns();
    RBFM_ScanIterator sIter;
    int numNullBytes = rbfManager->getNullBytesLength(colNames.size());
    rbfManager->scan(colHandle, getColumnsDescriptor(), "table-id", EQ_OP, &tableId, colNames, sIter);
    RID colRid;
    std::string colIndexName, attrName;
    int strLen, ixOffset;
    while(sIter.getNextRecord(colRid, colData) != RBFM_EOF)
    {
        ixOffset = getIndexOffset(colData);
        memcpy(&strLen, (char *) colData + ixOffset, sizeof(int));
        colIndexName = std::string(static_cast<const char *>((char *) colData + ixOffset + sizeof(int)), strLen);

        memcpy(&strLen, (char *) colData + sizeof(int) + numNullBytes, sizeof(int));
        attrName = std::string(static_cast<const char *>((char *) colData + (2*sizeof(int)) + numNullBytes), strLen);
        //colIndexMap.insert(attrName, colIndexName);
        //colIndexMap[attrName.c_str()] = colIndexName.c_str();
        //colIndexMap.insert(make_pair(attrName, colIndexName));

        colIndexMap[attrName] = colIndexName;

    }




    free(colData);
    sIter.close();
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    FileHandle fileHandle;
    rbfManager->openFile(table.fileName, fileHandle);
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    void *oldData = malloc(PAGE_SIZE);
    readTuple(tableName, rid, oldData);
    rbfManager->deleteRecord(fileHandle, recordDescriptor, rid);
    rbfManager->closeFile(fileHandle);

    std::map<string, string> colIndexMap;
    getColumnIndexMap(colIndexMap, table.tableId);
    RID rid2;
    rid2.pageNum = rid.pageNum;
    rid2.slotNum = rid.slotNum;
    removeFromIndex(table, recordDescriptor, colIndexMap, oldData, rid2);
    free(oldData);
    return 0;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    void *oldData = malloc(PAGE_SIZE);
    FileHandle fileHandle;
    rbfManager->openFile(table.fileName, fileHandle);
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    rbfManager->readRecord(fileHandle, recordDescriptor, rid, oldData);
    rc = rbfManager->updateRecord(fileHandle, recordDescriptor, data, rid);
    rbfManager->closeFile(fileHandle);

    std::map<string, string> colIndexMap;
    getColumnIndexMap(colIndexMap, table.tableId);
    removeFromIndex(table, recordDescriptor, colIndexMap, oldData, rid);
    insertToIndex(table, recordDescriptor, colIndexMap, data, rid);
    return rc;
}

RC RelationManager::updateSystemTuple(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid) {
    return rbfManager->updateRecord(fileHandle, recordDescriptor, data, rid);
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    FileHandle fileHandle;
    rbfManager->openFile(table.fileName, fileHandle);
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    rc = rbfManager->readRecord(fileHandle, recordDescriptor, rid, data);
    rbfManager->closeFile(fileHandle);
    return rc;
}

RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
	return rbfManager->printRecord(attrs, data);
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    FileHandle fileHandle;
    rbfManager->openFile(table.fileName, fileHandle);
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    rbfManager->readAttribute(fileHandle, recordDescriptor, rid, attributeName, data);
    rbfManager->closeFile(fileHandle);
    return rc;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }

    rbfManager->openFile(table.fileName, rm_ScanIterator.fileHandle);
    //rm_ScanIterator.fileHandle = &fileHandle;
    vector<Attribute> recordDescriptor;
    getAttributes(tableName, recordDescriptor);
    rbfManager->scan(rm_ScanIterator.fileHandle, recordDescriptor, conditionAttribute, compOp, value, attributeNames, rm_ScanIterator.sIter);
    return rc;

}

// Extra credit work
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName)
{
    return -1;
}

// Extra credit work
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr)
{
    return -1;
}

Attribute RelationManager::getAttributeDescriptor(const string &name, const AttrType &type, const AttrLength length) {
    Attribute attr;
    attr.name = name;
    attr.type = type;
    attr.length = length;
    return attr;
}

Attribute RelationManager::getTableIdAttribute() {
    return getAttributeDescriptor("table-id", TypeInt, sizeof(int));
}

Attribute RelationManager::getTableNameAttribute() {
    return getAttributeDescriptor("table-name", TypeVarChar, 50);
}

Attribute RelationManager::getTableFileNameAttribute() {
    return getAttributeDescriptor("file-name", TypeVarChar, 50);
}

Attribute RelationManager::getColumnNameAttribute() {
    return getAttributeDescriptor("column-name", TypeVarChar, 50);
}

Attribute RelationManager::getColumnTypeAttribute() {
    return getAttributeDescriptor("column-type", TypeInt, sizeof(int));
}

Attribute RelationManager::getColumnLengthAttribute() {
    return getAttributeDescriptor("column-length", TypeInt, sizeof(int));
}

Attribute RelationManager::getColumnPositionAttribute() {
    return getAttributeDescriptor("column-position", TypeInt, sizeof(int));
}

Attribute RelationManager::getColumnIndexAttribute() {
    return getAttributeDescriptor("index-file", TypeVarChar, 50);
}

vector<Attribute> RelationManager::getColumnsDescriptor() {
    vector<Attribute> colDescriptor;
    colDescriptor.push_back(getTableIdAttribute());
    colDescriptor.push_back(getColumnNameAttribute());
    colDescriptor.push_back(getColumnTypeAttribute());
    colDescriptor.push_back(getColumnLengthAttribute());
    colDescriptor.push_back(getColumnPositionAttribute());
    colDescriptor.push_back(getColumnIndexAttribute());
    return colDescriptor;
}

vector<Attribute> RelationManager::getTablesDescriptor() {
    vector<Attribute> recDescriptor;
    recDescriptor.push_back(getTableIdAttribute());
    recDescriptor.push_back(getTableNameAttribute());
    recDescriptor.push_back(getTableFileNameAttribute());
    return recDescriptor;
}

RC RelationManager::createColumnsMetadata() {
    vector<Attribute> recordDescriptor = getColumnsDescriptor();
    RID location;
    void *data = malloc(1000);
        /*  (1, "table-id", TypeInt, 4 , 1)
            (1, "table-name", TypeVarChar, 50, 2)
            (1, "file-name", TypeVarChar, 50, 3)
            (2, "table-id", TypeInt, 4, 1)
            (2, "column-name",  TypeVarChar, 50, 2)
            (2, "column-type", TypeInt, 4, 3)
            (2, "column-length", TypeInt, 4, 4)
            (2, "column-position", TypeInt, 4, 5)
        */
    createColumnRecord(1, "table-id", TypeInt, 4, 1, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(1, "table-name", TypeVarChar, 50, 2, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(1, "file-name", TypeVarChar, 50, 3, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);

    createColumnRecord(2, "table-id", TypeInt, 4, 1, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(2, "column-name", TypeVarChar, 50, 2, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(2, "column-type", TypeInt, 4, 3, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(2, "column-length", TypeInt, 4, 4, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(2, "column-position", TypeInt, 4, 5, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    createColumnRecord(2, "index-file", TypeVarChar, 100, 6, data);
    rbfManager->insertRecord(colHandle, recordDescriptor, data, location);
    free(data);
    return 0;
}

RC RelationManager::createColumnRecord(const int &tableId, const string &name, const AttrType &type, const int &colSize, const int &colPosition, void *data, string indexFileName) {
    vector<Attribute> recordDescriptor = getColumnsDescriptor();
    int position = 1;
    memset(data, 0, 1000);

    addField(tableId, data, position);
    position += sizeof(int);
    addField(name.length(), name, data, position);
    position += name.length();
    position += sizeof(int);
    addField(type, data, position);
    position += sizeof(int);
    addField(colSize, data, position);
    position += sizeof(int);
    addField(colPosition, data, position);
    position += sizeof(int);

    int ixfileLength = indexFileName.length();
    memcpy((char *) data + position, &ixfileLength, sizeof(int));
    position += sizeof(int);
    memcpy((char *)data + position, indexFileName.c_str(), ixfileLength);

    return 0;
}

RC RelationManager::createTableRecord(const int &tableId, const string &tableName, const string &fileName, void *data) {
    int position = 1;
    memset(data, 0, 1000);

    addField(tableId, data, position);
    position += sizeof(int);
    addField(tableName.length(), tableName, data, position);
    position += sizeof(int) + tableName.length();
    addField(fileName.length(), fileName, data, position);

    return 0;
}

RC RelationManager::createTablesMetadata() {
    vector<Attribute> recordDescriptor = getTablesDescriptor();
    RID location;
    void *data = malloc(1000);
    memset(data, 0, 1000);  // initialize with 0s
    currentTableIndex = 1;
    createTableRecord(currentTableIndex++, "Tables", "Tables", data);
    rbfManager->insertRecord(tablesHandle, recordDescriptor, data, location);
    createTableRecord(currentTableIndex++, "Columns", "Columns", data);
    rbfManager->insertRecord(tablesHandle, recordDescriptor, data, location);

    free(data);
    return 0;
}


RC RelationManager::addField(const int iValue, void *record, short position) {
    memcpy((char *)record + position, &iValue, sizeof(int));
    return 0;
}

RC RelationManager::addField(const float fValue, void *record, short position) {
    memcpy((char *)record + position, &fValue, sizeof(int));
    return 0;
}

RC RelationManager::addField(const int stringLength, const string &sValue, void *record, short position) {
    memcpy((char *)record + position, &stringLength, sizeof(int));
    memcpy((char *)record + position + sizeof(int), sValue.c_str(), stringLength);
    return 0;
}


RC RelationManager::getTablesCursor(RBFM_ScanIterator &sIter) {
    vector<Attribute> tablesDescriptor = getTablesDescriptor();
    vector<string> projectedAttributes;
    projectedAttributes.push_back("table-id");
    projectedAttributes.push_back("table-name");
    projectedAttributes.push_back("file-name");
    rbfManager->scan(tablesHandle, tablesDescriptor, "", NO_OP, NULL, projectedAttributes, sIter);
    return 0;
}

int RelationManager::getNextTableId() {
    RBFM_ScanIterator sIter;
    getTablesCursor(sIter);
    int maxId = 0, tableId;
    RID rid;
    void *data = malloc(PAGE_SIZE);
    memset(data, 0, PAGE_SIZE);
    while(sIter.getNextRecord(rid, data) != RBFM_EOF) {
        memcpy(&tableId, (char *)data + 1, sizeof(int));
        tableId > maxId ? maxId = tableId : 0;
        memset(data, 0, PAGE_SIZE);
    }
    free(data);
    return ++maxId;
}

RC RelationManager::findTableRecord(string tableName, TableDetails &table) {
    vector<Attribute> tablesDescriptor = getTablesDescriptor();
    string conditionAttribute = "table-name";
    int length = tableName.length();
    void *data = malloc(length + 4);
    memset(data, 0, length + 4);
    memcpy((char *) data, &length , sizeof(int));
    memcpy((char *)data + sizeof(int), tableName.c_str(), tableName.length());
    vector<string> projectedAttributes;
    projectedAttributes.push_back("table-id");
    projectedAttributes.push_back("table-name");
    projectedAttributes.push_back("file-name");
    RBFM_ScanIterator sIter;

    rbfManager->scan(tablesHandle, tablesDescriptor, conditionAttribute, EQ_OP, data, projectedAttributes, sIter);
    RID rid; void *rec;
    rec = malloc(PAGE_SIZE);
    memset(rec, 0, PAGE_SIZE);
    int rc = sIter.getNextRecord(rid, rec);
    if (rc == RBFM_EOF) {
        sIter.close();
        //cerr << "Table not found!";
        return -1;
    }

    int tableId;
    memcpy(&tableId, (char *) rec + 1, sizeof(int));
    table.tableId = tableId;
    table.tableName = tableName;
    table.fileName = tableName;
    table.rid.pageNum = rid.pageNum;
    table.rid.slotNum = rid.slotNum;
    sIter.close();
    free(rec);
    free(data);
    return 0;
}

RC RelationManager::getAttributeNames(const vector<Attribute> recDescriptor) {
    return 0;
}

RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
    RBFM_ScanIterator colIter;
    vector<Attribute> cols = getColumnsDescriptor();
    vector<string> colNames = getColumnTableColumns();
    TableDetails table;
    int rc = 0;
    rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return rc;
    }
    int tableId = table.tableId;
    vector<Attribute> tableCols;
    getAttributes(table.fileName, tableCols);

    rbfManager->scan(colHandle, cols, "table-id", EQ_OP, &tableId, colNames, colIter);
    RID rid;
    void *data = malloc(PAGE_SIZE);
    memset(data, 0, PAGE_SIZE);
    bool found = false;


    while (colIter.getNextRecord(rid, data) != RBFM_EOF) {
        int compVal = IndexManager::instance()->compareValues(cols.at(COL_NAME_POSITION), attributeName.c_str(), attributeName.length(), (char *) data + COL_TABLE_NULL_BYTES + sizeof(int));
        if (compVal == 0) {
            //cout << " FOUND ATTR " << attributeName.c_str() << " at " << rid.pageNum << " , " << rid.slotNum << endl;
            found = true;
            colIter.close();
            break;
        }
    }
    if (found) {
        rc = updateIndexName(data, attributeName, tableName);
        if (rc != -1) {
            rc = updateSystemTuple(colHandle, cols, data, rid);
        }
        string ixfile = getIndexName(attributeName, tableName);
        rc = IndexManager::instance()->createFile(ixfile);
        if (rc == 0) {
            // fetch updated attribute details
            for (vector<Attribute>::iterator it =  tableCols.begin(); it != tableCols.end(); ++it)
            {
                if ((it->name).compare(attributeName) == 0) {
                    addAllEntriesToIndex(table, *it, ixfile);
                    break;
                }
            }
        } else {
            colIter.close();
            free(data);
            return -1;
        }
    }
    else
    {
        colIter.close();
        free(data);
        return -1;
    }
    free(data);
    return rc;
}

RC RelationManager::addAllEntriesToIndex(TableDetails &table, Attribute &attribute, const string &ixfileName) {
    RBFM_ScanIterator sIter;
    RID rid;
    void *data = malloc(PAGE_SIZE);
    map<string, string> colIndexMap;
    vector<Attribute> recordDescriptor;
    getAttributes(table.tableName, recordDescriptor);
    getColumnIndexMap(colIndexMap, table.tableId);
    FileHandle tableHandle;
    rbfManager->openFile(table.tableName, tableHandle);
    IXFileHandle ixfileHandle;
    ixManager->openFile(colIndexMap[attribute.name], ixfileHandle);
    vector<string> projected;
    projected.push_back(attribute.name);
    rbfManager->scan(tableHandle, recordDescriptor, "", NO_OP, NULL, projected, sIter);

    while(sIter.getNextRecord(rid, data) != RBFM_EOF) {
        ixManager->insertEntry(ixfileHandle, attribute, (char *) data + 1, rid);
    }
    sIter.close();
    ixManager->closeFile(ixfileHandle);
    rbfManager->closeFile(tableHandle);
    free(data);
    return 0;
}

RC RelationManager::testMethod() {
    cout << "------- TEST METHOD ------ " << endl;
    RID rid;
    vector<Attribute> cols = getColumnsDescriptor();
    vector<string> colNames = getColumnTableColumns();
    rid.pageNum = 0;
    rid.slotNum = 11;
    void *newData = malloc(PAGE_SIZE);
    memset(newData, 0, PAGE_SIZE);
    rbfManager->readRecord(colHandle, cols, rid, newData);
    rbfManager->printRecord(cols, newData);

    rid.slotNum = 12;
    rbfManager->readRecord(colHandle, cols, rid, newData);
    rbfManager->printRecord(cols, newData);


    printAll();

    free(newData);
    return 0;
}

int RelationManager::printAll() {
    RID rid;
    vector<Attribute> cols = getColumnsDescriptor();
    vector<string> colNames = getColumnTableColumns();
    RBFM_ScanIterator sIter;
    rbfManager->scan(colHandle, cols, "", NO_OP, NULL, colNames, sIter);
    void *newData = malloc(PAGE_SIZE);
    while(sIter.getNextRecord(rid, newData) != RBFM_EOF) {
        rbfManager->printRecord(cols, newData);
    }
    free(newData);
    sIter.close();
    return 0;
}

string RelationManager::getIndexName(const string &attribute, const string &tableName) {
    return (attribute + tableName + "idx");
}

RC RelationManager::updateIndexName(void *data, const string &attributeName, const string &tableName) {
    int startOffset = (5 * sizeof(int)) + COL_TABLE_NULL_BYTES;
    int colNameLength;
    memcpy(&colNameLength, (char *) data + sizeof(int) + COL_TABLE_NULL_BYTES, sizeof(int));
    startOffset += colNameLength;
    string ixfileName = getIndexName(attributeName, tableName);
    int ixfileNameLen = ixfileName.length();
    memcpy((char *) data + startOffset, &ixfileNameLen, sizeof(int));
    memcpy((char *) data + startOffset + sizeof(int), ixfileName.c_str(), ixfileNameLen);
    return 0;
}

int RelationManager::getIndexOffset(void *data) {
    int startOffset;
    memcpy(&startOffset, (char *) data + sizeof(int) + COL_TABLE_NULL_BYTES, sizeof(int));
    startOffset += (5 * sizeof(int));
    startOffset += COL_TABLE_NULL_BYTES;
    return startOffset;
}

RC RelationManager::getIndexName(void *data, string &fileName) {
    int ixStart = getIndexOffset(data);
    int ixfileLen;
    memcpy(&ixfileLen, (char *) data + ixStart, sizeof(int));
    fileName = string (static_cast<const char *>((char *) data + ixStart + sizeof(int)), ixfileLen);
    return 0;
}

RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
    TableDetails table;
    findTableRecord(tableName, table);
    RID rid;
    RBFM_ScanIterator colIter;
    vector<Attribute> cols = getColumnsDescriptor();
    vector<string> colNames = getColumnTableColumns();
    rbfManager->scan(colHandle, cols, "table-id", EQ_OP, &(table.tableId), colNames, colIter);
    void *data = malloc(PAGE_SIZE);
    memset(data, 0, PAGE_SIZE);
    int strLen, ixOffset;
    int colNameOffset = COL_TABLE_NULL_BYTES + sizeof(int);
    string colName;
    while(colIter.getNextRecord(rid, data) != RBFM_EOF) {
        memcpy(&strLen, (char *) data + colNameOffset, sizeof(int));
        colName = string(static_cast<const char *>((char *) data + colNameOffset + sizeof(int)), strLen);
        if (colName.compare(attributeName) == 0) {
            ixOffset = getIndexOffset(data);
            memcpy(&strLen, (char *) data + ixOffset, sizeof(int));
            colName = string (static_cast<const char *>((char *) data + ixOffset + sizeof(int)), strLen);
            if (colName.length() == 0) {
                colIter.close();
                free(data);
                return -1;
            }
            else {
                remove(getIndexName(attributeName, tableName).c_str());
                strLen = 0;
                memcpy((char *) data + ixOffset, &strLen, sizeof(int));
                updateTuple("Columns", data, rid);
                break;
            }
        }
    }
    colIter.close();
    free(data);
    return 0;
}

RC RelationManager::indexScan(const string &tableName,
                      const string &attributeName,
                      const void *lowKey,
                      const void *highKey,
                      bool lowKeyInclusive,
                      bool highKeyInclusive,
                      RM_IndexScanIterator &rm_IndexScanIterator)
{
    TableDetails table;
    int rc = findTableRecord(tableName, table);
    if (rc != 0) {
        return -1;
    }
    vector<Attribute> attributes;
    Attribute attr;
    getAttributes(tableName, attributes);
    for (vector<Attribute>::iterator it = attributes.begin(); it != attributes.end(); ++it) {
        if ((it->name).compare(attributeName) == 0) {
            attr = *it;
            break;
        }
    }
    string ixfileName = getIndexName(attributeName, tableName);
    rc = ixManager->openFile(ixfileName, rm_IndexScanIterator.fileHandle);
    if (rc != 0) { return -1; }
    ixManager->scan(rm_IndexScanIterator.fileHandle, attr, lowKey, highKey, lowKeyInclusive, highKeyInclusive, rm_IndexScanIterator.ixIter);

	return 0;
}
