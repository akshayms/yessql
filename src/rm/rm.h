#ifndef _rm_h_
#define _rm_h_

#include <string>
#include <vector>
#include <map>

#include "../rbf/rbfm.h"
#include "../ix/ix.h"

using namespace std;

# define RM_EOF (-1)  // end of a scan operator

struct TableDetails {
    int tableId;
    string tableName;
    string fileName;
    RID rid;
};

struct Column {
    int tableId;
    Attribute details;
    int position;
    string ixfileName;

    bool operator <(const Column &col) {
        return (position < col.position);
    }
};

#define COL_NAME_POSITION 1
#define COL_TABLE_NULL_BYTES 1
#define TABLES "Tables"
#define COLUMNS "Columns"


// RM_ScanIterator is an iteratr to go through tuples
class RM_ScanIterator {
public:
    RBFM_ScanIterator sIter;
    FileHandle fileHandle;

    RM_ScanIterator() { };

    ~RM_ScanIterator() { };

    // "data" follows the same format as RelationManager::insertTuple()
    RC getNextTuple(RID &rid, void *data) {
        return sIter.getNextRecord(rid, data);
    };

    RC close() {
        sIter.close();
        return 0;
    };

};

// RM_IndexScanIterator is an iterator to go through index entries
class RM_IndexScanIterator {
 public:
    IX_ScanIterator ixIter;
    IXFileHandle fileHandle;

  RM_IndexScanIterator() {};  	// Constructor
  ~RM_IndexScanIterator() {}; 	// Destructor

  // "key" follows the same format as in IndexManager::insertEntry()
  RC getNextEntry(RID &rid, void *key) {
      return ixIter.getNextEntry(rid, key);
  };  	// Get next matching entry
  RC close() {
      IndexManager::instance()->closeFile(fileHandle);
      ixIter.close();
      return 0;
  };             			// Terminate index scan
};


// Relation Manager
class RelationManager {
public:
    static RelationManager *instance();

    RC createCatalog();

    RC deleteCatalog();

    RC createTable(const string &tableName, const vector<Attribute> &attrs);

    RC deleteTable(const string &tableName);

    RC getAttributes(const string &tableName, vector<Attribute> &attrs);

    RC insertTuple(const string &tableName, const void *data, RID &rid);

    RC deleteTuple(const string &tableName, const RID &rid);

    RC updateTuple(const string &tableName, const void *data, const RID &rid);

    RC readTuple(const string &tableName, const RID &rid, void *data);

    // Print a tuple that is passed to this utility method.
    // The format is the same as printRecord().
    RC printTuple(const vector<Attribute> &attrs, const void *data);

    RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

    // Scan returns an iterator to allow the caller to go through the results one by one.
    // Do not store entire results in the scan iterator.
    RC scan(const string &tableName,
            const string &conditionAttribute,
            const CompOp compOp,                  // comparison type such as "<" and "="
            const void *value,                    // used in the comparison
            const vector<string> &attributeNames, // a list of projected attributes
            RM_ScanIterator &rm_ScanIterator);

    RC createIndex(const string &tableName, const string &attributeName);

    RC destroyIndex(const string &tableName, const string &attributeName);

    // indexScan returns an iterator to allow the caller to go through qualified entries in index
    RC indexScan(const string &tableName,
                          const string &attributeName,
                          const void *lowKey,
                          const void *highKey,
                          bool lowKeyInclusive,
                          bool highKeyInclusive,
                          RM_IndexScanIterator &rm_IndexScanIterator);

    RC testMethod();
    int printAll();

// Extra credit work (10 points)
public:
    RC addAttribute(const string &tableName, const Attribute &attr);

    RC dropAttribute(const string &tableName, const string &attributeName);


protected:
    RelationManager();

    ~RelationManager();

private:
    static RelationManager *_rm;
    FileHandle tablesHandle;
    FileHandle colHandle;
    RecordBasedFileManager *rbfManager;
    IndexManager *ixManager;
    int currentTableIndex;

    RC createTablesMetadata();
    RC createTableRecord(const int &tableId, const string &tableName, const string &fileName, void *data);
    RC createColumnRecord(const int &tableId, const string &name, const AttrType &type, const int &colSize, const int &colPosition, void *data, string ixfileName = string());
    RC createColumnsMetadata();
    vector<Attribute> getTablesDescriptor();
    vector<Attribute> getColumnsDescriptor();
    Attribute getColumnPositionAttribute();
    Attribute getColumnLengthAttribute();
    Attribute getColumnTypeAttribute();
    Attribute getColumnNameAttribute();
    Attribute getTableFileNameAttribute();
    Attribute getTableNameAttribute();
    Attribute getTableIdAttribute();
    Attribute getColumnIndexAttribute();
    Attribute getAttributeDescriptor(const string &name, const AttrType &type, const AttrLength length);
    RC addField(const int iValue, void *record, short position);
    RC addField(const float fValue, void *record, short position);
    RC addField(const int stringLength, const string &sValue, void *record, short position);
    int getNextTableId();
    RC findTableRecord(string tableName, TableDetails &table);
    RC getAttributeNames(const vector<Attribute> recDescriptor);
    RC getTablesCursor(RBFM_ScanIterator &sIter);
    vector<string> getColumnTableColumns();
    RC findIndexFileName(const string tableName, const string colName, string &fileName);

    string getIndexName(const string &attribute, const string &tableName);
    RC updateIndexName(void *data, const string &attributeName, const string &tableName);
    int getIndexOffset(void *data);
    RC getIndexName(void *data, string &fileName);

    RC updateSystemTuple(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid);

    RC addAllEntriesToIndex(TableDetails &table, Attribute &attribute, const string &ixfileName);
    void getColumnIndexMap(std::map <string, string> &colIndexMap, int &tableId);
    RC insertToIndex(TableDetails &table, vector<Attribute> &recordDescriptor, map<string, string> &colIndexMap, const void *data, const RID &rid);
    RC removeFromIndex(TableDetails &table, vector<Attribute> &recordDescriptor, map<string, string> &colIndexMap, const void *data, const RID &rid);

};

#endif
